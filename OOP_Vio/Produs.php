<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/9/2018
 * Time: 7:27 PM
 */
class Produs
{
    public $productName;
    public $price;
    public $stoc;

    /**
     * Produs constructor.
     * @param $productName
     * @param $price
     * @param $totalQuantity
     */
    public function __construct($productName, $price, $stoc)
    {
        $this->productName = $productName;
        $this->price = $price;
        $this->stoc = $stoc;
    }

    public function DisplayStock()
    {
        return $this->stoc;
    }
}