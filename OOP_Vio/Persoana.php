<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/3/2018
 * Time: 8:40 PM
 */

class Persoana
{
    public $name;
    public $wallet;

    public function __construct($name, Portofel $wallet)
    {
        $this->name = $name;
        $this->wallet = $wallet;
    }

    public function Give(Persoana $receiver, $amount)
    {
      $this->wallet->Out($amount);
      $receiver->Receive($amount);
    }

    public function Receive($amount)
    {
        $this->wallet->In($amount);
    }

    public function DisplayWallet()
    {
        return $this->name." wallet contains: " .$this->wallet->amount;
    }

    public function Buy(Casier $casier, $shoppingPrice, $paymentMethod = "cash")
    {
        if($paymentMethod == "card") {
            $casier->pos->ReceiveAmount($shoppingPrice);
        }
        else {
            if($this->wallet->Out($shoppingPrice))
                $casier->ReceiveMoney($shoppingPrice);
        }
    }
}