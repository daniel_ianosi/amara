<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/12/2018
 * Time: 9:16 PM
 */
include "def.php";

/** @var Cart $cart */
$cart = unserialize($_SESSION["cart"]);
$product = $products[$_GET['prodId']];
$quantity = $_POST["quantity"];

$cart->update($product, $quantity);
$_SESSION["cart"] = serialize($cart);
header('Location: index.php');