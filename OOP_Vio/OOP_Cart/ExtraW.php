<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/10/2018
 * Time: 8:32 PM
 */
class ExtraW extends BaseProduct
{
    public $percent;
    public $parentProduct;


    /**
     * ExtraW constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param bool $inStock
     * @param float $percent
     * @param BaseProduct $product
     */
    public function __construct($id, $name, $description, $percent, BaseProduct $product)
    {
        $this->percent = $percent;
        $this->parentProduct = $product;
        $ewPrice = $product->price * $percent/100 + $product->price;
        parent::__construct($id, $name, $ewPrice, $description, true);
    }

    /**
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return BaseProduct
     */
    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    /**
     * @param BaseProduct $parentProduct
     */
    public function setParentProduct($parentProduct)
    {
        $this->parentProduct = $parentProduct;
    }


}