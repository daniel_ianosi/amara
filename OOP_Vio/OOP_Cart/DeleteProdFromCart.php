<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/17/2018
 * Time: 4:39 PM
 */

include "def.php";

/** @var Cart $cart */
$cart = unserialize($_SESSION["cart"]);
$product = $products[$_GET['deleteId']];
$cart->delete($product);
$_SESSION["cart"] = serialize($cart);
header('Location: index.php');