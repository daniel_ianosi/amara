<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 7:05 PM
 */
class BaseProduct
{
    /** @var  int */
    public $id;
    /** @var  string */
    public $name;
    /** @var  float */
    public $price;
    /** @var  string */
    public $description;
    /** @var  boolean */
    public $inStock;

    /**
     * Product constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param bool $inStock
     */
    public function __construct($id, $name, $price, $description, $inStock)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->inStock = $inStock;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isInStock()
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
    }

}