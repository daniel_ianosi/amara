<?php
session_start();
include "Cart.php";
include "CartItem.php";
include "BaseProduct.php";
include "FashionProduct.php";
include "ExtraW.php";
include "SpecialProduct.php";

$products[1] = new BaseProduct(1, 'Aspirator', 23.25, 'sdasdasd', false);
$products[2] = new FashionProduct(2, 'Camasa', 13.99, 'sdasdasd', true,['S','L'], "S");
//$products[] = new ExtraW(1, "ewCamasa", "ExtraW", 5, $product1);
$products[3] = new SpecialProduct(3, "SpecialProd", 10, "Special Prod");

if(!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = serialize(new Cart());
}
$cart = unserialize($_SESSION['cart']);