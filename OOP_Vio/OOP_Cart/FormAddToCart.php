<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/12/2018
 * Time: 8:52 PM
 */
include "def.php";


/** @var Cart $cart */
$cart = unserialize($_SESSION["cart"]);
$product = $products[$_POST['products']];
$quantity = $_POST["quantity"];
$cart->add($product, $quantity);
$_SESSION["cart"] = serialize($cart);
header('Location: index.php');