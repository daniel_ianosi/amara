<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/11/2018
 * Time: 8:09 PM
 */
class SpecialProduct extends BaseProduct
{

    /**
     * SpecialProduct constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param bool $inStock
     * @param int $quantity
     */
    public function __construct($id, $name, $price, $description, $inStock = true)
    {
        parent::__construct($id, $name, $price, $description, $inStock);
    }
}