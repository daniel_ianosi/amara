<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/5/2018
 * Time: 12:52 PM
 */
class Card
{
    public $pin;
    public $amount;
    public $cardName;
    public $wallet;

    /**
     * Card constructor.
     * @param $pin
     * @param $amount
     * @param $cardName
     */
    public function __construct($cardName, $pin = "1234", $amount)
    {
        $this->cardName = $cardName;
        $this->pin = $pin;
        $this->amount = $amount;
    }

    public function In($receivedMoney)
    {
        //$this->wallet->Out($receivedMoney);
        return $this->amount += $receivedMoney;
    }

    public function Out($sentMoney)
    {
        //$this->wallet->In($sentMoney);
        return $this->amount -= $sentMoney;
    }

    public function DisplayCardAmount()
    {
        return "Card contains ".$this->amount;
    }
}