<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/5/2018
 * Time: 7:07 PM
 */
class Casier
{
    public $checkout;
    public $pos;
    /**
     * Casier constructor.
     * @param $amount
     */
    public function __construct(CasaDeMarcat $checkout, POS $pos)
    {
        $this->checkout = $checkout;
        $this->pos = $pos;
    }

    public function ReceiveMoney($amount)
    {
        $this->checkout->ReceiveAmount($amount);
        echo "The amount is successfully received!<br>";
    }

}