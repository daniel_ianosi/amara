<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/20/2019
 * Time: 8:07 PM
 */

include "../dbconnect.php";
include "../classes/BaseTable.php";
include "../classes/BaseProduct.php";
include "../classes/Comment.php";
include "../classes/Author.php";
include "../classes/Category.php";
include "../classes/User.php";
include "../classes/Product.php";
include "../classes/ProductImage.php";
include "../classes/Blogpost.php";
include "../classes/ProductCategory.php";
include "../classes/ProductManufacturer.php";
include "../classes/Order.php";
include "../classes/Supplier.php";