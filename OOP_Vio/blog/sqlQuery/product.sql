-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2018 at 09:56 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `stock` int(10) NOT NULL,
  `id_category` int(10) NOT NULL,
  `id_description` int(10) NOT NULL,
  `id_manufacturer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `id_category`, `id_description`, `id_manufacturer`) VALUES
(1, 'Televizor 81cm, 4K-8K', 652, 21, 1, 1, '1'),
(14, 'Televizor 101cm LED TV', 1021, 5, 1, 2, '1'),
(15, 'Monitor 21 inch', 236, 12, 2, 3, '2'),
(16, 'Monitor 19 inch', 156, 6, 2, 4, '3'),
(17, 'Jaketa ski', 526, 8, 3, 5, '4'),
(18, 'Pantaloni ski', 623, 9, 3, 6, '5'),
(19, 'Masina de spalat', 896, 2, 4, 7, '6'),
(20, 'Aspirator', 325, 6, 4, 8, '1'),
(21, 'Cuptor cu microunde', 198, 16, 4, 9, '2'),
(22, 'Bormasina', 465, 3, 5, 10, '7'),
(23, 'Scaun', 26, 9, 6, 11, '8'),
(24, 'masa 6 persoane', 266, 3, 6, 12, '8'),
(25, 'ceas barbatesc', 466, 15, 7, 13, '9'),
(26, 'ceas femeiesc', 366, 17, 7, 14, '10'),
(27, 'Parfum 100ml', 266, 4, 8, 15, '11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
