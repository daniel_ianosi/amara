<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/13/2019
 * Time: 12:34 PM
 */
class Author extends BaseTable
{
    public $id;
    public $name;

//    public function __construct($id = null)
//    {
//        parent::__construct($id);
//    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public static function getTableName()
    {
        return "author";
    }
}