<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/19/2019
 * Time: 12:29 PM
 */
class User extends BaseTable
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $user_name;
    /**
     * @var
     */
    public $type;
    /**
     * @var
     */
    public $password;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser_name()
    {
        return $this->user_name;
    }

    /**
     * @param mixed $user_name
     */
    public function setUser_name($user_name)
    {
        $this->user_name = $user_name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @return string
     */
    static function getTableName()
    {
        return "user";
    }
}