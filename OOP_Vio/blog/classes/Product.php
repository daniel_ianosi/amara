<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/19/2019
 * Time: 12:49 PM
 */
class Product extends BaseProduct
{
    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $stock;
    /**
     * @var
     */
    public $id_category;
    /**
     * @var
     */
    public $id_manufacturer;
    /**
     * @var
     */
    public $best_sellers;
    /**
     * @var
     */
    public $new_arrivals;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getId_category()
    {
        return $this->id_category;
    }

    /**
     * @param mixed $id_category
     */
    public function setId_category($id_category)
    {
        $this->id_category = $id_category;
    }

    /**
     * @return mixed
     */
    public function getId_manufacturer()
    {
        return $this->id_manufacturer;
    }

    /**
     * @param mixed $id_manufacturer
     */
    public function setId_manufacturer($id_manufacturer)
    {
        $this->id_manufacturer = $id_manufacturer;
    }

    /**
     * @return mixed
     */
    public function getBest_sellers()
    {
        return $this->best_sellers;
    }

    /**
     * @param mixed $best_sellers
     */
    public function setBest_sellers($best_sellers)
    {
        $this->best_sellers = $best_sellers;
    }

    /**
     * @return mixed
     */
    public function getNew_arrivals()
    {
        return $this->new_arrivals;
    }

    /**
     * @param mixed $new_arrivals
     */
    public function setNew_arrivals($new_arrivals)
    {
        $this->new_arrivals = $new_arrivals;
    }

    /**
     * @return string
     */
    static function getTableName()
    {
        return "product";
    }

    /**
     * @return boolean
     */
    public function isStockable()
    {
        return true;
    }
}