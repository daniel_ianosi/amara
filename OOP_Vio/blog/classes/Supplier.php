<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/20/2019
 * Time: 7:13 PM
 */
class Supplier extends BaseTable
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $id_product;
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $stock;
    /**
     * @var
     */
    public $delivery;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId_product()
    {
        return $this->id_product;
    }

    /**
     * @param mixed $id_product
     */
    public function setId_product($id_product)
    {
        $this->id_product = $id_product;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * @return string
     */
    static function getTableName()
    {
        return "supplier";
    }
}