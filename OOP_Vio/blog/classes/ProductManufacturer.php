<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/20/2019
 * Time: 3:49 PM
 */
class ProductManufacturer extends BaseTable
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    static function getTableName()
    {
        return "product_manufacturer";
    }
}