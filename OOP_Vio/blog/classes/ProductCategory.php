<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/20/2019
 * Time: 2:38 PM
 */
class ProductCategory extends BaseTable
{

    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $category;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


    /**
     * @return string
     */
    static function getTableName()
    {
        return "product_category";
    }
}