<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/13/2019
 * Time: 2:27 PM
 */
class Comment extends BaseTable
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $id_post;
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $comment;
    /**
     * @var
     */
    public $review;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId_post()
    {
        return $this->id_post;
    }

    /**
     * @param mixed $id_post
     */
    public function setId_post($id_post)
    {
        $this->id_post = $id_post;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param mixed $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }


    /**
     * @return string
     */
    public static function getTableName()
    {
        return "comment";
    }
}