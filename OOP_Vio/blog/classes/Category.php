<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/13/2019
 * Time: 2:16 PM
 */
class Category extends BaseTable
{
    public $id;
    public $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public static function getTableName()
    {
        return "category";
    }
}