<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/13/2019
 * Time: 12:35 PM
 */
abstract class BaseTable
{
    public function __construct($id = null)
    {
        if (isset($id)) {
            $data = $this->getFromDb(static::getTableName(),['id'=>$id]);
            foreach (get_object_vars($this) as $columnName=>$value) {
                $setter = 'set' . ucfirst($columnName);
                $this->$setter($data[$columnName]);
            }
        }
    }

    abstract static function getTableName();

    /**
     * @param $table
     * @param array $filters
     * @return array
     */
    public function getFromDb($table, $filters = [])
    {
        global $mysql;
        $list = [];
        $filter = '1=1';
        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }

        $result = $mysql->query("SELECT * FROM `$table` WHERE $filter LIMIT 1;");
        return $result->fetch_assoc();
    }

    public static function getAll($filters = [], $groupBy = [], $filterLike = [], $filterOrder = [], $offset = NULL, $limit = NULL, $filtersColumn = [],$condition='AND')
    {
        global $mysql;
        $table = static::getTableName();
        $list = [];
        $condition0='AND';
        $filter = '1=1';
        $like = 'AND 1=1';
        $order = '';


        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }
        foreach ($filtersColumn as $column => $value) {
            $filter .= ' AND ' . "$column=$value";
        }
        if (!empty($groupBy)) {
            $groupByCondition = ' GROUP BY ' . implode(',', $groupBy);
        } else {
            $groupByCondition = '';
        }
        foreach ($filterLike as $col => $val) {
            if ($col == 'table') {
            } else {
                if (isset($val) && !is_int($val)) {
                    $like .= ' '.$condition0.' ' . "$col" . " LIKE '%" . "$val" . "%'";
                    $condition0=$condition;
                } else {
                    $like .= ' AND ' . "$col='$val'";
                }
            }
        }
        if (!empty($filterOrder)) {

            $orderList = [];
            foreach ($filterOrder as $key => $value) {
                $orderList[] = " $key $value ";
            }
            $order = ' ORDER BY ' . implode(',', $orderList);
        }
        if (!is_null($offset) || !is_null($limit)) {
            $pages = ' LIMIT ' . "$offset" . "," . "$limit";
        } else {
            $pages = "";
        }
        //die("SELECT * FROM `$table` WHERE $filter $like $groupByCondition  $order $pages;");
        $result = $mysql->query("SELECT * FROM `$table` WHERE $filter $like $groupByCondition  $order $pages;");

        $tableName = ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $table))));
        while ($row = $result->fetch_assoc()) {
            $list[] = new $tableName($row['id']);
        }

        return $list;
    }

    public static function getOne($filters = [])
    {
        global $mysql;
        $table = static::getTableName();
        $list = [];
        $filter = '1=1';
        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }
        $result = $mysql->query("SELECT * FROM $table WHERE $filter LIMIT 1;");
        $tableName = ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $table))));
        while($row = $result->fetch_assoc()) {
           $list = new $tableName($row['id']);
        }
        return $list;
    }

    /**
     *
     */
    public function Delete()
    {
        global $mysql;
        $query = "DELETE FROM " . $this->getTableName() . " WHERE id = " . $this->getId();
        $mysql->query($query);
    }


    /**
     *
     */
    public function update()
    {
        global $mysql;
        $sql = "UPDATE " . $this->getTableName() . " SET ";
        foreach ($this as $key => $item) {
            if (is_array($item)) {
                $sql .= $key . " = '" . implode(",", $item) . "',";
            } else {
                $sql .= $key . " = '" . $item . "',";
            }
        }
        $sql = rtrim($sql, ',');
        $sql .= "WHERE id = " . $this->getId();
        $mysql->query($sql);
    }

    public function insert()
    {
        global $mysql;
        $keys = null;
        $values = null;
        $sql = "INSERT INTO " . $this->getTableName();
        foreach ($this as $key => $item) {

            $keys .= $key . ", ";
            if (is_array($item)) {
                $values .= '"' . implode(",", $item) . '", ';
            } else {
                $values .= '"' . $item . '", ';
            }
        }
        $keys = rtrim($keys, ', ');
        $values = rtrim($values, ', ');
        $sql .= "($keys) VALUES ($values)";
        $mysql->query($sql);
    }

    /**
     *
     */
    public function save()
    {
        if(!is_null($this->getId())) {
            $this->update();
        }
        else {
            $this->insert();
        }
    }

}