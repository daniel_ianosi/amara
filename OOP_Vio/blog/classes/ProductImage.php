<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/19/2019
 * Time: 4:02 PM
 */
class ProductImage extends BaseTable
{

    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $id_produs;
    /**
     * @var
     */
    public $file_name;
    /**
     * @var
     */
    public $main_image;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId_produs()
    {
        return $this->id_produs;
    }

    /**
     * @param mixed $id_produs
     */
    public function setId_produs($id_produs)
    {
        $this->id_produs = $id_produs;
    }

    /**
     * @return mixed
     */
    public function getFile_name()
    {
        return $this->file_name;
    }

    /**
     * @param mixed $file_name
     */
    public function setFile_name($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return mixed
     */
    public function getMain_image()
    {
        return $this->main_image;
    }

    /**
     * @param mixed $main_image
     */
    public function setMain_image($main_image)
    {
        $this->main_image = $main_image;
    }

    /**
     * @return Product
     */
    public function getProduct($idProd = null)
    {
        if(!isset($idProd))
            return new Product($this->getId_produs());
        else
            return new Product($idProd);
    }

    /**
     * @return string
     */
    static function getTableName()
    {
        return "product_image";
    }
}