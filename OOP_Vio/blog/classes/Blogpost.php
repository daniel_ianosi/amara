<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/13/2019
 * Time: 2:02 PM
 */
class Blogpost extends BaseTable
{

    public $id;
    public $title;
    public $id_category;
    public $content;
    public $id_author;
    public $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getId_category()
    {
        return $this->id_category;
    }

    /**
     * @param mixed $id_catogory
     */
    public function setId_category($id_category)
    {
        $this->id_category = $id_category;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getId_author()
    {
        return $this->id_author;
    }

    /**
     * @param mixed $id_author
     */
    public function setId_author($id_author)
    {
        $this->id_author = $id_author;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }



    public static function getTableName()
    {
        return "blogpost";
    }

    public function getAuthor()
    {
        return new Author($this->getId_author());
    }

    public function getCategory()
    {
        return new Category($this->getId_category());
    }
}