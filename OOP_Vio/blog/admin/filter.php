<?php
include "functions.php";
$_SESSION['filter']['table'] = $_POST;
checkUser();
$condition = boolval(0);
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>
<?php require 'inc/views/template_head_end.php'; ?>
<?php require 'inc/views/base_head.php'; ?>

<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 7px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 10px;
        margin: 3px 3px;
        cursor: pointer;
    }

    .button2 {
        background-color: #008CBA;
    }

    /* Blue */
    .button3 {
        background-color: #f44336;
    }

    /* Red */
    .button4 {
        background-color: #e1e1e1;
    }

    /* Blue */
</style>

<!-- Page Header -->
<div class="bg-image overflow-hidden"
     style="background-image: url('<?php echo $one->assets_folder; ?>/img/photos/photo31@2x.jpg');">
    <div class="bg-black-op">
        <div class="content content-narrow">
            <div class="block block-transparent">
                <div class="block-content block-content-full">
                    <h1 class="h1 font-w300 text-white animated fadeInDown push-50-t push-5">Dashboard A.M.A.R.A </h1>
                    <h2 class="h4 font-w300 text-white-op animated fadeInUp">Welcome Administrator</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content content-narrow">
    <!-- Stats -->
    <div class="row text-uppercase">
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Vizitatori unici</div>
                    <div><h3>402929 </h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Autori activi</div>
                    <div><h3>4562</h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Articole blog</div>
                    <div><h3>72929 </h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Total comentarii</div>
                    <div><h3>112929 </h3></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats -->

    <!-- Page Content -->
    <div class="content">

        <!-- Main Dashboard Chart -->
        <div class="block">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <button type="button" data-toggle="block-option" data-action="refresh_toggle"
                                data-action-mode="demo"><i class="si si-refresh"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Administrare tabele</h3>
            </div>
            <div class="block-content block-content-full bg-gray-lighter text-center">


                <!-- START My code -->

                <table class="main-table">
                    <tr>
                        <td class="content">
                            <?php if (!empty($_SESSION['filter']['table']['table'])) { ?>
                                <div>
                                    <img src="assets/img/various/new_post.png" width="30" height="30">
                                    <a href="insert_registration.php?table=<?php echo $_SESSION['filter']['table']['table']; ?>">
                                        <button>+ Adauga post nou</button>
                                    </a>
                                    </br>
                                    </br>
                                </div>
                            <?php } else {
                            } ?>
                            <?php if (isset($_SESSION['filter']['table']['table'])):?>
                            <form action="filter.php" method="post">
                                <table border="1">
                                    <div>
                                        <?php foreach (ShowColumns($_SESSION['filter']['table']['table']) as $column): ?>
                                        <tr>
                                            <th><?php echo $column['Field'] ?> </th>
                                            <td><input type="text" name="<?php echo $column['Field']; ?>"></td>
                                            <?php if ($column['Field'] == 'review') {
                                                $condition = 1;
                                            } else {
                                            } ?>
                                            <?php endforeach; ?>
                                        </tr>
                                    </div>
                                    <tr>
                                        <td>
                                            <button type="submit">SEARCH</button>
                                        </td>
                                    </tr>
                                </table>
                        <td><input type="hidden" name="table" value="<?php echo $_SESSION['filter']['table']['table']; ?>"/></td>
                        </form>
                        <?php endif; ?>
                            <table border="1" class="table-content">
                                </br>
                                </br>
                                <?php if (!empty($_SESSION['filter']['table']['table'])){ ?>
                                <tr>
                                    <?php foreach (ShowColumns($_SESSION['filter']['table']['table']) as $column): ?>
                                        <th valign="top">
                                            <p align="center"><?php echo $column['Field'] ?></p>
                                            <p align="center">
                                                <a href="sort.php?table=<?php echo $_SESSION['filter']['table']['table']. "&column=" . $column['Field'] . "&sorttype=ASC" ?>"
                                                   methods="post">&#9650</a>
                                                <a href="sort.php?table=<?php echo $_SESSION['filter']['table']['table']. "&column=" . $column['Field'] . "&sorttype=DESC" ?>"
                                                   methods="post">&#9660</a>
                                            </p>
                                        </th>
                                        <?php if ($column['Field'] == 'review') {
                                            $condition = 1;
                                        } else {
                                        } ?>
                                    <?php endforeach; ?>

                                    <th valign="top">Actions</th>
                                </tr>
                                <tr>
                                    <?php foreach (getUniversal($_SESSION['filter']['table']['table'], [], [], $_SESSION['filter']['table']) as $comm): ?>
                                    <?php foreach (ShowColumns($_SESSION['filter']['table']['table']) as $col): ?>
                                        <td> <?php echo extractFirstWords($comm[$col['Field']]); ?></td>
                                    <?php endforeach; ?>
                                    <td>

                                        <?php foreach (getButtons($_SESSION['filter']['table']['table'], $comm['id']) as $action => $link): ?>
                                            <a href="<?php echo $link ?>"><?php echo $action; ?></a>
                                        <?php endforeach; ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </table>
                        </td>
                    </tr>
                    <?php } else {
                    } ?>
                    <!-- END My code-->

            </div>
        </div>
        <!-- END Main Dashboard Chart -->


        <!-- END Latest Sales Widget -->

    </div>
    <!-- END Page Content -->
    <!-- END Page Content -->

    <?php require 'inc/views/base_footer.php'; ?>
    <?php require 'inc/views/template_footer_start.php'; ?>

    <!-- Page Plugins -->
    <script src="<?php echo $one->assets_folder; ?>/js/plugins/chartjs/Chart.min.js"></script>

    <!-- Page JS Code -->
    <script src="<?php echo $one->assets_folder; ?>/js/pages/base_pages_dashboard_v2.js"></script>
    <script>
        jQuery(function () {
            // Init page helpers (CountTo plugin)
            App.initHelpers('appear-countTo');
        });
    </script>

    <?php require 'inc/views/template_footer_end.php'; ?>

