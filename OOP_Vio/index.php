<?php
/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/3/2018
 * Time: 7:35 PM
 */
include "Portofel.php";
include "Card.php";
include "Persoana.php";
include "Bancomat.php";
include "POS.php";
include "Casier.php";
include "CasaDeMarcat.php";
include "Cos.php";
include "Produs.php";
//$redBrick = new Brick();
//$redBrick->cols= 20;
//$redBrick->rows= 20;
//$greyBrick = new Brick('grey');
////$greyBrick->colour='blue';
//$blueSmallBrick = new Brick('blue',1,4);
//
//
////$redBrick->addBrick($greyBrick);
//
//for ($i=0; $i<=100; $i++) {
//    $redBrick->addBrick(new Brick('blue',rand(1,20),rand(1,20)));
//}
//
////var_dump($redBrick);
//
////echo $redBrick->colour."\n";
////echo $blueSmallBrick->colour."\n";
////echo $greyBrick->colour."\n";
//
//$redBrick->draw();

$cardIng = new Card("ING", "1234", 20);
$portofel1 = new Portofel(125, $cardIng);
$portofel2 = new Portofel(200, $cardIng);
$person1 = new Persoana("Persoana1", $portofel1);
$person2 = new Persoana("Persoana2", $portofel2);

//$person1->Give($person2, 25);
//echo $person1->DisplayWallet()."<br>";
//echo $person2->DisplayWallet()."<br>";
//$person2->Receive($person1, 50);
//echo $person1->DisplayWallet()."<br>";
//echo $person2->DisplayWallet();


//$bancomatIng = new Bancomat($cardIng, "1234", "ING");
//$bancomatIng->CashOut(5);
//echo $cardIng->DisplayCardAmount();
//echo $person1->DisplayWallet();

$pos = new POS($cardIng);
$casaDeMarcat = new CasaDeMarcat();
$casier = new Casier($casaDeMarcat, $pos);

$person1->Buy($casier, 20, "card");
echo $cardIng->DisplayCardAmount()."<br>";
echo $person1->DisplayWallet()."<br>";

$shopping1 = new Produs("Produs1", 10, 5);
$shopping2 = new Produs("Produs2", 20, 7);
$basket = new Cos();
$basket->AddShoppingIntoBasket($shopping1, 3);
$basket->AddShoppingIntoBasket($shopping2, 1);
$basket->RemoveShoppingFromBasket($shopping1, 2);

echo "<br />".$basket->DisplayBasket();
echo "<br />Total basket value: ".$basket->TotalBasket();

