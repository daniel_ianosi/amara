<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/3/2018
 * Time: 7:32 PM
 */
class Brick
{
    public $colour;

    public $rows;

    public $cols;

    public $height=10;

    /** @var Brick  */
    public $topBrick = null;

    /**
     * Brick constructor.
     * @param $colour
     * @param $rows
     * @param $cols
     * @param $height
     */
    public function __construct($colour='red', $rows=2, $cols=8)
    {
        $this->colour = $colour;
        $this->rows = $rows;
        $this->cols = $cols;
    }

    public function addBrick(Brick $newBrick)
    {
        $topBrick = $this->getTopBrick();
        if ($newBrick->rows<=$topBrick->rows && $newBrick->cols <= $topBrick->cols){
            $topBrick->topBrick = $newBrick;
        }
    }

    public function getTopBrick()
    {
        $topBrick = $this;
        while ($topBrick->topBrick!=null){
            $topBrick =$topBrick->topBrick;
        }

        return $topBrick;
    }

    public function draw()
    {
        if ($this->topBrick != null){
            $this->topBrick->draw();
        }
        for ($i=0; $i<$this->cols; $i++) {
            echo '-';
        }

        echo "<br />";
    }


}