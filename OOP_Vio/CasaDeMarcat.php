<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/5/2018
 * Time: 7:08 PM
 */
class CasaDeMarcat
{
    public $amount;

    /**
     * CasaDeMarcat constructor.
     *
     */
    public function __construct()
    {
        $this->amount = 0;
    }

    public function ReceiveAmount($amount)
    {
        $this->amount += $amount;
    }
}