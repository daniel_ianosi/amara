<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/5/2018
 * Time: 7:07 PM
 */
class POS
{
    public $amount;
    public $card;
    /**
     * POS constructor.
     * @param $amount
     */
    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    public function ReceiveAmount($amount)
    {
        if($this->card->amount >= $amount) {
            $this->card->Out($amount);
            echo "Payment successfully received<br />";
        }
        else {
            echo "Not enough money in your bank account<br>";
        }
    }
}