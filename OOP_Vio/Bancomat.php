<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/5/2018
 * Time: 12:53 PM
 */
class Bancomat
{
    public $card;
    public $pin;
    public $bankName;
    /**
     * Bancomat constructor.
     * @param $card
     * @param $pin
     * @param $bankName
     */
    public function __construct(Card $card, $pin, $bankName)
    {
        $this->card = $card;
        $this->pin = $pin;
        $this->bankName = $bankName;
    }

    public function CheckPin()
    {
        if($this->card->pin != $this->pin) {
            return false;
        }
        return true;
    }

    public function CheckCard()
    {
        if($this->card->cardName == $this->bankName)
            return true;
        else
            return false;
    }

    public function CashOut($money)
    {
        if($this->CheckCard()) {
            if($this->CheckPin()) {
                if($money <= $this->card->amount) {
                    $this->card->Out($money);
                }
                else {
                    echo "Not enough founds.";
                }
            }
            else {
                echo "Wrong PIN number for ".$this->card->cardName;
            }
        }
        else
        {
            echo "Wrong Bank name for ".$this->card->cardName;
        }
    }

    public function PlaceFunds($money)
    {
        if($this->CheckCard()) {
            if($this->CheckPin()) {
                if($money <= $this->card->amount) {
                    $this->card->In($money);
                }
                else {
                    echo "Not enough founds.";
                }
            }
            else {
                echo "Wrong PIN number for ".$this->card->cardName;
            }
        }
        else
        {
            echo "Wrong Bank name for ".$this->card->cardName;
        }
    }
}