<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/19/2018
 * Time: 8:16 PM
 */
class Category extends BaseTable
{
    public $id;

    public $name;

    /**
     * Category constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getTableName()
    {
        return "category";
    }
}