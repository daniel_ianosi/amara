<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/19/2018
 * Time: 8:20 PM
 */
abstract class BaseTable
{
    /** @var  int */
    public $id;

    /**
     * @return string
     */
    abstract public function getTableName();

    /**
     * Product constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param bool $inStock
     */
    public function __construct($id = null)
    {
        if (isset($id)) {
            $data = $this->getFromDb($this->getTableName(),['id'=>$id]);
            foreach (get_object_vars($this) as $columnName=>$value) {
                $setter = 'set' . ucfirst($columnName);
                $this->$setter($data[$columnName]);
            }
        }

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getFromDb($table, $filters = [])
    {
        global $mysql;
        $list = [];
        $filter = '1=1';
        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }

        $result = $mysql->query("SELECT * FROM $table WHERE $filter LIMIT 1;");
        return $result->fetch_assoc();
    }

    /**
     *
     */
    public function Delete()
    {
        global $mysql;
        $query = "DELETE FROM " . $this->getTableName() . " WHERE id = " . $this->getId();
        $mysql->query($query);
    }


    /**
     *
     */
    public function update()
    {
        global $mysql;
        $sql = "UPDATE " . $this->getTableName() . " SET ";
        foreach ($this as $key => $item) {
            if (is_array($item)) {
                $sql .= $key . " = '" . implode(",", $item) . "',";
            } else {
                $sql .= $key . " = '" . $item . "',";
            }
        }
        $sql = rtrim($sql, ',');
        $sql .= "WHERE id = " . $this->getId();
        $mysql->query($sql);
    }

    public function insert()
    {
        global $mysql;
        $keys = null;
        $values = null;
        $sql = "INSERT INTO " . $this->getTableName();
        foreach ($this as $key => $item) {

            $keys .= $key . ", ";
            if (is_array($item)) {
                $values .= '"' . implode(",", $item) . '", ';
            } else {
                $values .= '"' . $item . '", ';
            }
        }
        $keys = rtrim($keys, ', ');
        $values = rtrim($values, ', ');
        $sql .= "($keys) VALUES ($values)";
        $mysql->query($sql);
        //echo $sql;
    }

    /**
     *
     */
    public function save()
    {
        if(is_null($this->getId())) {
            $this->update();
        }
        else {
            $this->insert();
        }
    }
}