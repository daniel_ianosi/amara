<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/9/2018
 * Time: 7:38 PM
 */
class Cos
{
    public $purchase;
    public $totalBasket;
    public $basket;

    public function AddShoppingIntoBasket(Produs $purchase, $quantity)
    {
        if($quantity <= $purchase->stoc) {
            $this->basket[] = array("quantity" => $quantity, "shopping" => $purchase);
            $purchase->stoc -= $quantity;
        }
        else{
            echo "The quantity of: ".$purchase->productName." is higher than stock";
        }
    }

    public function RemoveShoppingFromBasket(Produs $purchase, $quantity)
    {
        foreach($this->basket as $key => $item) {
            if($item["shopping"]->productName == $purchase->productName) {
                if($item["quantity"] == $quantity) {
                    unset($this->basket[$key]);

                }
                elseif($item["quantity"] > $quantity) {
                    $this->basket[$key]["quantity"] -= $quantity;
                }
                else {
                   echo "Number of items you want to delete is higher than the quantity from basket!";
                }
                break;
            }
        }
        $purchase->stoc +=  $quantity;

    }

    public function TotalPerShopping(Produs $purchase, $quantity)
    {
        $totalPerShopping = 0;
        $totalPerShopping += $quantity * $purchase->price;

        return $totalPerShopping;
    }

    public function DisplayBasket()
    {
        $summary = "The basket contains:<br /> ";
        $this->totalBasket = 0;
        foreach($this->basket as $purchase) {
            $summary .= $purchase["quantity"]." x ".$purchase["shopping"]->productName." ";
            $summary .= "Total per shopping: ".$this->TotalPerShopping($purchase["shopping"], $purchase["quantity"])."<br>";
            $this->totalBasket +=  $this->TotalPerShopping($purchase["shopping"], $purchase["quantity"]);
        }

        return $summary;
    }

    public function TotalBasket()
    {
        return $this->totalBasket;
    }

}