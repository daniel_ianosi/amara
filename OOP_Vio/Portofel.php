<?php

/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 12/3/2018
 * Time: 8:41 PM
 */
class Portofel
{
    public $amount;
    public $card;

    public function __construct($amount, Card $card)
    {
        $this->card = $card;
        $this->amount = $amount;
    }

    public function In($receivedMoney)
    {
        return $this->amount += $receivedMoney;
    }

    public function Out($sentMoney)
    {
        if($sentMoney <= $this->amount) {
            $this->amount -= $sentMoney;
            return true;
        }
        else {
            echo "Not enough money in the wallet<br>";
            return false;
        }
    }

}