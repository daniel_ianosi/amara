<?php include "functions.php";?>
<?php include "componets/header.php";?>

<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>
<?php $date=getOneUniversal('blogpost',['id'=>$_GET['date']]); ?>
<h1>Here you can see all posts from this date: <?php echo $date['date']; ?></h1>
<?php foreach (getUniversal('blogpost',['date'=>$_GET['date']],[],[],[]) as $post): ?>
<div class="blog_post">
    <h1><a href="post.php?id=<?php echo $post['id']; ?>" id="intro"><?php echo $post['title']; ?></a></h1>
    <p>
        <?php echo extractFirstWords($post['content']); ?> ...
    </p>
    <div class="article_menu">
        <?php $aut=getOneUniversal('authors',['id'=>$post['id_author']]) ?>
        <b>Posted by <a href="author.php?author=<?php echo $post['id_author']; ?>"><?php echo $aut['name']; ?></a></b>
        <?php $cat=getOneUniversal('category',['id'=>$post['id_category']]) ?>
        <a href="category.php?category=<?php echo $post['id_category']; ?>"><?php echo $cat['name']; ?></a>
    </div>
    <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $post['date']; ?>"><?php echo $post['date']; ?></a></b></p>


</div>
<?php endforeach;?>

