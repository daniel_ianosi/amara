<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/9/2018
 * Time: 8:04 PM
 */
class cartItem
{
    public $product;
    public $quantity;

    /**
     * cartItem constructor.
     * @param $product
     * @param $quantity
     */
    public function __construct(produs $product, $quantity)
    {
        if ($product->stoc>=$quantity){
        $this->product = $product;

        $this->quantity = $quantity;}
        else {
            echo "PRODUCT ".$product->nume." HAS A STOCK OF ".$product->stoc."<br/>";
        }
    }

    /**
     * @param $quantity int
     */
    public function modifyQuantity($quantity){
    if ($this->product->stoc>=$quantity){
        $this->quantity=$quantity;}
        else {
        echo "QUANTITY NOT MODIFIED"."<br/>";
        echo "PRODUCT ".$this->product->nume." HAS A STOCK OF ".$this->product->stoc."<br/>";
        }
    }

    /**
     * @return int
     */
    public function itemPrice(){
        $price=0;
        $price=$this->product->pret*$this->quantity;
        return $price;
    }



}