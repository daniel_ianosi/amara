<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/4/2018
 * Time: 7:55 PM
 */
class Card
{
  public $pin;

  public $cont;


    /**
     * Card constructor.
     * @param $pin
     */
    public function __construct($pin, Cont $cont)
    {
        $this->pin = $pin;
        $this->cont=$cont;
    }
    public function feedCard ($pinCard,$amount){
        if ($amount>0) {
            if ($this->pin == $pinCard) {
                $this->cont->in($amount);
                $condition = TRUE;
            } else {
                echo "WRONG PIN CODE !\n" . "<br/>";
                $condition = FALSE;
            }
        }
        else {
            echo "INSERT AMOUNT BIGGER THEN 0 !";
            $condition=FALSE;
        }
        return $condition;
    }
    public function takeFromCard($pinCard,$amount){
        $condition=FALSE;
        if ($amount>0){
        if ($this->pin == $pinCard){
            if ($this->cont->out($amount)){
            $condition=TRUE;}
        }
        else {
            echo "WRONG PIN CODE !\n"."<br/>";
            $condition=FALSE;
        }
        }
        else {
            echo "INSERT AMOUNT BIGGER THEN 0 !";
            $condition=FALSE;
        }
        return $condition;

    }

}
