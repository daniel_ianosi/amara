<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/5/2018
 * Time: 7:05 PM
 */
class Casier
{
    public $nume;

    public $CasaMarcat;

    public $POS;

    /**
     * Casier constructor.
     * @param $nume
     */
    public function __construct($nume,CasaMarcat $casaMarcat, POS $POS)
    {
        $this->nume=$nume;
        $this->CasaMarcat=$casaMarcat;
        $this->POS=$POS;
    }

    public function takeCash($amount){
        $this->CasaMarcat->in($amount);
        echo "BON: AI PLATIT ".$amount." lei\n"."<br/>";
    }

    public function takeCard($amount){
        $this->POS->in($amount);
    }

}