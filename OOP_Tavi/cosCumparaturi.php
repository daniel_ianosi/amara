<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/9/2018
 * Time: 11:57 AM
 */
class cosCumparaturi
{
    /** @var [cantitate=>produs] */
    public $produs=[];

    /**
     * cosCumparaturi constructor.
     * @param array $produs
     */
    public function __construct(array $produs)
    {
        $this->produs = $produs;
    }

    public function generateCheck(){
        $pret=0;
        foreach ($this->produs as $products){
            foreach ($products as $quantity=>$product){
            if ($quantity>0){
            if ($product instanceof produs){
            $pret+=$product->generatePrice($quantity);
        }
        else {
                echo "WRONG INPUT PRODUCT !"."<br/>";;
        }}
        else{
                echo "WRONG INPUT QUANTITY !"."<br/>";
        }
        }}

        return $pret;
    }

}