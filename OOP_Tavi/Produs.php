<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/9/2018
 * Time: 11:52 AM
 */
class produs
{
    public $nume;
    public $pret;
    public $stoc;
    public $descriere;

    /**
     * produs constructor.
     * @param $pret
     * @param $stoc
     * @param $descriere
     */
    public function __construct($nume,$pret, $stoc,descriereProdus $descriere)
    {
        $this->nume = $nume;
        $this->pret = $pret;
        $this->stoc = $stoc;
        $this->descriere = $descriere;
    }

    public function generatePrice($quantity){
        $price=0;
        if ($this->stoc>=$quantity){
            $price=$this->pret*$quantity;
            $this->stoc-=$quantity;
        }
        else {
            if ($this->stoc < $quantity) {
                if ($this->stoc == 0) {
                    echo "PRODUCT ".$this->nume." OUT OF STOCK !" . "<br/>";
                } else {
                    echo "QUANTITY ON PRODUCT " . $this->nume . " IS " . $this->stoc . "<br/>";
                }
            }
        }
        return $price;
    }

}
