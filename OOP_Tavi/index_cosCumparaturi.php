<?php
include "includeClase.php";
$desc=new descriereProdus('Punga plastic');
$desc1=new descriereProdus('Cutie Carton');
$prod1=new produs('plasa mica',0.5,11,$desc);
$prod2=new produs('plasa mare',1,10,$desc);
$prod3=new produs('cutie',2,20,$desc1);
$cos1=new cosCumparaturi([0=>[10=>$prod1], 1=>[4=>$prod2], 2=>[10=>$prod3]]);
$cont1=new Cont(800);
$cont2=new Cont(10000);
$bancomat=new Bancomat(99999);

$card1=new Card(1233,$cont1);
$POS1=new POS($cont2);
$casaMarcat1=new CasaMarcat(1000);
$GigelCasier=new Casier('Gigel',$casaMarcat1,$POS1);

$portofel1=new Portofel(1000,$card1);
$blackPerson=new Persoana('Dita','Octavian',$portofel1);

//CASH TRANSACTION
echo "CASH TRANSACTION: "."<br>";
echo $blackPerson->Nume." are in portofel inainte de tranzactie ".$portofel1->bani."<br/>";
echo "*********"."<br/>";
echo "STOCUL PRODUSELOR INAINTE DE TRANZACTIE: "."<br/>";
echo "PRODUSUL1 AVEA: ".$prod1->stoc." PRODUSE IN STOC"."<br/>";
echo "PRODUSUL2 AVEA: ".$prod2->stoc." PRODUSE IN STOC"."<br/>";
echo "PRODUSUL3 AVEA: ".$prod3->stoc." PRODUSE IN STOC"."<br/>";
echo "*********"."<br/>";
$blackPerson->buy($cos1,$GigelCasier,$portofel1);

echo "STOCUL PRODUSELOR DUPA TRANZACTIE STOCUL ESTE:"."<br/>";
echo "PRODUSUL1 ARE: ".$prod1->stoc." PRODUSE IN STOC"."<br/>";
echo "PRODUSUL2 ARE: ".$prod2->stoc." PRODUSE IN STOC"."<br/>";
echo "PRODUSUL3 ARE: ".$prod3->stoc." PRODUSE IN STOC"."<br/>";
echo "*********"."<br/>";
echo $blackPerson->Nume." are in portofel dupa tranzactie ".$portofel1->bani."<br/>";
echo "<hr>";

//CARD TRANSACTION
echo "CARD TRANSACTION NOW:"."<br/>";
echo $blackPerson->Nume." are pe card ".$card1->cont->sold."<br/>";
$blackPerson->buy($cos1,$GigelCasier,$card1,1233);
echo $blackPerson->Nume." are pe card dupa tranzactie ".$card1->cont->sold."<br/>";
echo "*********"."<br/>";
echo "STOCUL PRODUSELOR DUPA TRANZACTIE STOCUL ESTE:"."<br/>";
echo "PRODUSUL1 ARE: ".$prod1->stoc." PRODUSE IN STOC"."<br/>";
echo "PRODUSUL2 ARE: ".$prod2->stoc." PRODUSE IN STOC"."<br/>";
echo "PRODUSUL3 ARE: ".$prod3->stoc." PRODUSE IN STOC"."<br/>";
echo "*********"."<br/>";