<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/3/2018
 * Time: 8:44 PM
 */
class Portofel
{
    Public $bani;

    /** @var Card[] */
    public $cards=[];

    /**
     * Portofel constructor.
     * @param $bani
     */
    public function __construct($bani, $cards=[])
    {
        $this->bani = $bani;
        $this->cards = $cards;
    }

    public function in($bani){
        $this->bani+=$bani;
    }

    public function out($bani){
        $condition=FALSE;
        if ($this->bani>=$bani){
            $this->bani-=$bani;
            $condition=TRUE;
        }
        else
        { echo "TRANZACTIE DEPASITA\n"."<br/>"; }
        return $condition;
    }

}