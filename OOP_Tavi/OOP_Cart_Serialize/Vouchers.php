<?php
class Vouchers extends BaseProduct
{
/** @var  string */
    public $Code;
    /** @var  integer */
    public $Amount=0;
    /** @var  float */
    public $Percent=100;

    /**
     * Vouchers constructor.
     * @param int $id
     * @param string $name
     * @param string $description
     * @param bool $inStock
     * @param $code
     * @param $amount
     * @param $percent
     */
    public function __construct($id, $name, $description, $inStock, $code, $amount, $percent)
    {
        $this->Code=$code;
        $this->Amount=$amount;
        $this->Percent=$percent;
        parent::__construct($id, $name, $price=0, $description, $inStock);
    }


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param string $Code
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->Amount;
    }

    /**
     * @param int $Amount
     */
    public function setAmount($Amount)
    {
        $this->Amount = $Amount;
    }

    /**
     * @return float
     */
    public function getPercent()
    {
        return $this->Percent;
    }

    /**
     * @param float $Percent
     */
    public function setPercent($Percent)
    {
        $this->Percent = $Percent;
    }


}