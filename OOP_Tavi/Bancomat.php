<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/4/2018
 * Time: 9:49 PM
 */
class Bancomat
{
    public $bani;

    /**
     * bancomat constructor.
     * @param $bani
     */
    public function __construct($bani)
    {
        $this->bani = $bani;
    }

    public function withdrawATM ($amount){
        if ($amount>0) {
            if ($this->bani >= $amount) {
                $this->bani -= $amount;
                $condition = TRUE;
            } else {
                echo "INSUFICIENT FUNDS IN ATM \n" . "<br/>";
                $condition = FALSE;
            }

        }
        else {
            echo "INSERT AMOUNT BIGGER THEN 0 !";
            $condition=FALSE;
        }
        return $condition;
    }
    public function feedATM($amount){
        if ($amount>0){
        $this->bani+=$amount;}
        else {
            echo "INSERT AMOUNT BIGGER THEN 0 !";
        }
    }
}