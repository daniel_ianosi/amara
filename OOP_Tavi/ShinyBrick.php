<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 7:55 PM
 */
class ShinyBrick extends Brick
{
    public $weight;

    public function __construct($colour = 'red', $rows = 2, $cols = 8, $weight)
    {

        parent::__construct($colour, $rows, $cols);
        $this->weight = $weight;
    }

    public function addBrick(Brick $newBrick)
    {
        return;
    }

}