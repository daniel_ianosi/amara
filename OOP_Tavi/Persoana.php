<?php
class Persoana
{
    Public $Nume;

    Public $Prenume;

    Public $Portofel;

    /**
     * Persoana constructor.
     * @param $Nume
     * @param $Prenume
     * @param $Portofel
     */
    public function __construct($Nume, $Prenume, Portofel $Portofel)
    {
        $this->Nume = $Nume;
        $this->Prenume = $Prenume;
        $this->Portofel = $Portofel;
    }

    public function Give(Persoana $receiver, $amount)
    {
        if ($amount > 0 && ($this->Portofel->out($amount)) == TRUE) {
            $receiver->Portofel->in($amount);
        }

    }

    public function Take($amount)
    {
        $this->Portofel->in($amount);
    }

    public function deposit(Bancomat $bancomat, Card $card, $pin, $amount)
    {
        if ($this->Portofel->cards==$card) {
            if ($this->Portofel->out($amount)) {
                if ($card->feedCard($pin, $amount)) {
                    echo "DEPOSIT DONE\n"."<br/>";
                    $bancomat->feedATM($amount);
                }
            } else {
                echo "NOTE ENOUGH MONEY\n"."<br/>";
            }
        }
        else
        { echo "NOT YOUR CARD !\n"."<br/>";}
    }

    public function withdraw(Bancomat $bancomat, Card $card, $pin, $amount)
    {
        if ($card->takeFromCard($pin, $amount) ) {
            if ($bancomat->withdrawATM($amount)){
                echo "WITHDRAW DONE\n"."<br/>";
                $this->Portofel->bani+=$amount;
            }
            else
            {$card->feedCard($pin,$amount);}

        }
    }

    public function buy(cosCumparaturi $cos,Casier $casier, $method, $pin=NULL){
        $amount=$cos->generateCheck();
        if ($method instanceof Card){
            if ($method->takeFromCard($pin,$amount)){
                $casier->takeCard($amount);
                echo "YOU PAYED WITH CARD ".$amount." lei\n"."<br/>";
            }
            else {
                echo "WOULD YOU LIKE TO TRY AGAIN ?\n"."<br/>";
            }
        }
        if ($method instanceof Portofel){
            if ($method->out($amount)){
                $casier->takeCash($amount);
            }
            else {
                echo "WOULD YOU LIKE TO PAY WITH CARD ?\n"."<br/>";
            }
        }
    }
}