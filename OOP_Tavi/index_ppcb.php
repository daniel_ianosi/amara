<?php
include "Persoana.php";
include "Portofel.php";
include "Card.php";
include "Bancomat.php";

$black=new Bancomat(99999);

$goldCard=new Card(1233,9999);
$blackCard=new Card(1213,500);
$yellowCard=new Card(1312, 200);

$blackPortofel=new Portofel(400,[$blackCard,$goldCard]);
$blackPerson=new Persoana('Dita','Octavian',$blackPortofel);

//** EXAMPLE OF WITHDRAW MONEY FROM CARD
echo $blackPerson->Prenume." are ".$blackPerson->Portofel->bani." in portofel \n";
$blackPerson->withdraw($black,$goldCard,1233,500);
echo $blackPerson->Prenume." retrage 500 lei de pe card \n";
echo $blackPerson->Prenume." are ".$blackPerson->Portofel->bani." in portofel\n";

//** EXAMPLE OF FEEDING MONEY TO CARD
echo "Inainte de tranzactie pe YELLOWCARD sunt ".$yellowCard->bani."\n";
echo "Inainte de tranzactie in ATM-ul black sunt ".$black->bani."\n";
$blackPerson->deposit($black,$yellowCard,1312,900);
echo "Pe YELLOWCARD sunt ".$yellowCard->bani."\n";
echo "In ATM-ul black sunt".$black->bani;

//var_dump($blackPerson);


