<?php
/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/9/2018
 * Time: 8:08 PM
 */
$item=0;
class cart
{
    public $cartItems=[];

    /**
     * cart constructor.
     * @param array $cartItems=[produs=>Quantity]
     */
    public function __construct(array $cartItems=NULL)
    {
        if (is_null($cartItems)){
        $this->cartItems = $cartItems;}
        else
        {
            foreach ($cartItems as $produs=>$quantity){
                $this->addToCart($produs,$quantity);
            }
        }
    }

    /**
     * cart constructor.
     * @param $cartItems
     */

    public function addToCart(produs $produs, $quantity)
    {
        global $item;
        if (isset($this->cartItems)){
            foreach ($this->cartItems as $id => $cartItem) {
            if ($cartItem->product == $produs) {
                $this->cartItems[$id]->modifyQuantity($quantity);
                echo "CART UPDATED" . "<br/>";
                break;
            }
        }}
        else {
                $item += 1;
                $this->cartItems[$item] = new cartItem($produs, $quantity);
            }
        }

    public function updateCart(produs $produs, $quantity){
        foreach ($this->cartItems as $id=>$cartItem){
            if ($cartItem->product==$produs){
                $this->cartItems[$id]->modifyQuantity($quantity);
                echo "CART UPDATED"."<br/>";
                break;
            }
        }
    }

    public function deleteFromCart(produs $produs){
    $condition=FALSE;
        foreach ($this->cartItems as $id=>$cartItem){
            if ($cartItem->product==$produs){
                unset($this->cartItems[$id]);
                echo "PRODUCT ".$produs->nume." HAS BEEN REMOVED FROM CART"."<br/>";
                $condition=TRUE;
                break;
            }
        }
    if ($condition==FALSE){

        echo "PRODUCT NOT IN CART !"."<br/>";
    }
    }

    public function generateCheck(){
        $price=0;
        foreach ($this->cartItems as $id=>$cartItem){
            $price+=$cartItem->itemPrice();
        }

        return $price;
    }
}