<!DOCTYPE html>
<?php include_once "functions/functions.php";
if (isset($_POST['table'])){
    $_SESSION['table']=$_POST['table'];
    $table = $_SESSION['table'];
    if (!isset($_SESSION['sort'][$table])) {
        $_SESSION['sort'][$table] = [];}}
$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if ($page == "" || $page == "1") {
    $page1 = 0;
} else {
    $page1 = ($page * 5) - 5;
}
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ADMIN PANEL</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        .active, .accordion:hover {
            background-color: #ccc;
        }

        .accordion:after {
            content: '\002B';
            color: #777;
            font-weight: bold;
            float: right;
            margin-left: 5px;
        }

        .active:after {
            content: "\2212";
        }

        .panel {
            padding: 0 18px;
            background-color: white;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
        }
    </style>
</head>
<?php

$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if ($page == "" || $page == "1") {
    $page1 = 0;
} else {
    $page1 = ($page * 5) - 5;
}
?>
<body>
<div class="container">
    <div id="content" class="row">
        <?php include "new_components/admin_sidebar.php"; ?>

    <div class="col-9">
        <div class="row">
        <div class="col-4" style="border: solid">
        NUMAR POSTURI: <?php echo countRegistrations('blogpost')//['COUNT(*)']; ?>
        </div>
        <div class="col-4" style="border: solid">
        NUMAR PRODUSE: <?php echo countRegistrations('product')//['COUNT(*)']; ?>
        </div>
        <div class="col-4" style="border-bottom: double; border-bottom-color: blue; border-top: double;border-top-color: red; border-right: double; border-right-color: yellow;border-left:double;border-left-color: yellow">
        NUMAR COMMENTURI: <?php echo countRegistrations('comments')//['COUNT(*)']; ?>
        </div>
        </div>
        <br>
        <div class="row">
        <div class="col-12">
          <?php if (!empty($_SESSION['table'])) { ?>
        <div>
        <a href="edit.php?table=<?php echo $_SESSION['table']; ?>">
        <button style="font-size:16px;color:darkred"> Adauga post nou <i style="color:red" class="fa fa-plus"></i> </button>
        </a>
        </div>
        <?php } else { } ?>
        </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                <?php if (isset($_SESSION['filter']['table']['table'])):?>
                    <form action="filter.php" method="post">
                        <table border="1">
                            <div>
                                <?php foreach (ShowColumns($_SESSION['filter']['table']['table']) as $column): ?>
                                <tr>
                                    <th><?php echo $column['Field'] ?> </th>
                                    <td><input type="text" name="<?php echo $column['Field']; ?>"></td>
                                    <?php endforeach; ?>
                                </tr>
                            </div>
                            <tr>
                                <td>
                                    <button type="submit">SEARCH</button>
                                </td>
                            </tr>
                        </table>
                        <td><input type="hidden" name="table" value="<?php echo $_SESSION['filter']['table']['table']; ?>"/></td>
                    </form>
                <?php endif; ?>
                <table border="1" class="table-content">
                    </br>
                    </br>
                    <?php if (!empty($_SESSION['filter']['table']['table'])){ ?>
                    <tr>
                        <?php foreach (ShowColumns($_SESSION['filter']['table']['table']) as $column): ?>
                            <th valign="top">
                                <p align="center"><?php echo $column['Field'] ?></p>
                                <p align="center">
                                    <a href="sort.php?table=<?php echo $_SESSION['filter']['table']['table']. "&column=" . $column['Field'] . "&sorttype=ASC" ?>"
                                       methods="post">&#9650</a>
                                    <a href="sort.php?table=<?php echo $_SESSION['filter']['table']['table']. "&column=" . $column['Field'] . "&sorttype=DESC" ?>"
                                       methods="post">&#9660</a>
                                </p>
                            </th>
                            <?php if ($column['Field'] == 'review') {
                                $condition = 1;
                            } else {
                            } ?>
                        <?php endforeach; ?>

                        <th valign="top">Actions</th>
                    </tr>
                    <tr>
                        <?php foreach (getUniversal($_SESSION['filter']['table']['table'], [], [], $_SESSION['filter']['table']) as $comm): ?>
                        <?php foreach (ShowColumns($_SESSION['filter']['table']['table']) as $col): ?>
                            <td> <?php echo extractFirstWords($comm[$col['Field']]); ?></td>
                        <?php endforeach; ?>
                        <td>
                            <?php foreach (getButtons($_SESSION['table'], $object->getId(),$page1/5+1) as $action => $link): ?>
                                <a href="<?php echo $link ?>"><?php echo $action; ?></a>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

                </table>
            <?php } else {            } ?>
            </div>
        </div>