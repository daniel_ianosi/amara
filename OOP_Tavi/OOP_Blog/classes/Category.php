<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/3/2019
 * Time: 12:54 PM
 */
class Category extends BaseTable
{
    /** @var  string */
    public $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getTableName(){
        return 'category';
    }
}