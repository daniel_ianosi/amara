<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/9/2019
 * Time: 12:59 PM
 */
class Product_images extends BaseTable
{
    /** @var  integer */
    public $id_produs;
    /** @var  string */
    public $file_name;
    /** @var  boolean */
    public $main_image;

    /**
     * @return int
     */
    public function getId_produs()
    {
        return $this->id_produs;
    }

    /**
     * @param int $id_produs
     */
    public function setId_produs($id_produs)
    {
        $this->id_produs = $id_produs;
    }

    /**
     * @return string
     */
    public function getFile_name()
    {
        return $this->file_name;
    }

    /**
     * @param string $file_name
     */
    public function setFile_name($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return bool
     */
    public function getMain_image()
    {
        return $this->main_image;
    }

    /**
     * @param bool $main_image
     */
    public function setMain_image($main_image)
    {
        $this->main_image = $main_image;
    }

    protected function getTableName()
    {
        return 'product_images';
    }


}