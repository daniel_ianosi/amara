<?php
class Order_items extends BaseTable
{
    /** @var  integer */
    public $id_order;
    /** @var  integer */
    public $id_product;
    /** @var  integer */
    public $quantity;
    /** @var  string */
    public $product_name;
    /** @var  float */
    public $price;

    /**
     * @return int
     */
    public function getId_order()
    {
        return $this->id_order;
    }

    /**
     * @param int $id_order
     */
    public function setId_order($id_order)
    {
        $this->id_order = $id_order;
    }

    /**
     * @return int
     */
    public function getId_product()
    {
        return $this->id_product;
    }

    /**
     * @param int $id_product
     */
    public function setId_product($id_product)
    {
        $this->id_product = $id_product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getProduct_name()
    {
        return $this->product_name;
    }

    /**
     * @param string $product_name
     */
    public function setProduct_name($product_name)
    {
        $this->product_name = $product_name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return 'order_items';
    }
}