<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/3/2019
 * Time: 12:57 PM
 */
class Authors extends BaseTable
{
    /** @var  string */
    public $name;


    /**
     * @return string
     */
    protected function getTableName()
    {
        return 'authors';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}