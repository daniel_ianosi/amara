<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/8/2019
 * Time: 1:50 PM
 */
class Product_manufacturer extends BaseTable
{
    /** @var  string */
    public $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    protected function getTableName()
    {
     return 'product_manufacturer';
    }


}