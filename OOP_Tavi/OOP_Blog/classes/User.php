<?php

class User extends BaseTable
{
    /** @var  string */
    public $user_name;
    /** @var  boolean */
    public $type;
    /** @var  string */
    public $password;

    /**
     * @return string
     */
    public function getUser_name()
    {
        return $this->user_name;
    }

    /**
     * @param string $user_name
     */
    public function setUser_name($user_name)
    {
        $this->user_name = $user_name;
    }

    /**
     * @return bool
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param bool $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    protected function getTableName()
    {
     return 'user';
    }


}