<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/3/2019
 * Time: 12:42 PM
 */
class Blogpost extends BaseTable
{
    /** @var  string */
    public $title;

    /** @var  integer */
    public $id_category;

    /** @var  string */
    public $content;

    /** @var integer */
    public $id_author;

    /** @var string */
    public $date;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getId_category()
    {
        return $this->id_category;
    }

    /**
     * @param int $id_category
     */
    public function setId_category($id_category)
    {
        $this->id_category = $id_category;
    }

    public function getCategory(){
        return new Category($this->getId_Category());
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId_author()
    {
        return $this->id_author;
    }

    /**
     * @param int $id_author
     */
    public function setId_author($id_author)
    {
        $this->id_author = $id_author;
    }

    public function getAuthor(){
        return new Authors($this->getId_author());
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getTableName(){
        return 'blogpost';
    }

}