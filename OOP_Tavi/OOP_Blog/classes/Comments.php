<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/4/2019
 * Time: 12:06 PM
 */
class Comments extends BaseTable
{
    /** @var  integer */
    public $id_post;
    /** @var  string */
    public $name;
    /** @var string */
    public $comment;
    /** @var  string */
    public $review;

    /**
     * @return int
     */
    public function getId_post()
    {
        return $this->id_post;
    }

    /**
     * @param int $id_post
     */
    public function setId_post($id_post)
    {
        $this->id_post = $id_post;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param string $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }


    /**
     * @return string
     */
    protected function getTableName()
    {
        return "comments";
    }
}