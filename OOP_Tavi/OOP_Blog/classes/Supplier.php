<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 1/8/2019
 * Time: 1:42 PM
 */
class Supplier extends BaseTable
{
    /** @var  integer */
    public $id_product;
    /** @var string  */
    public $name;
    /** @var  integer */
    public $stock;
    /** @var  integer */
    public $delivery;

    /**
     * @return int
     */
    public function getId_product()
    {
        return $this->id_product;
    }

    /**
     * @param int $id_product
     */
    public function setId_product($id_product)
    {
        $this->id_product = $id_product;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return int
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param int $delivery
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }

    protected function getTableName()
    {
     return 'supplier';
    }


}