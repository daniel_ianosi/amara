<?php

abstract class BaseTable
{
    /** @var integer */
    public $id;

    public function __construct($id = null)
    {
        if (isset($id)) {
            $this->id = $id;
            $data = $this->getFromDb($this->getTableName(),['id'=>$id]);
            if (empty($data)){
                echo "ID NOT FOUND IN DB !";
            }
            else {
                $vals = get_object_vars($this);
                foreach ($vals as $atribute => $value) {
                    $setter = 'set' . ucfirst($atribute);
                    $this->$setter($data[$atribute]);
                }
            }
        }

    }

    private function getFromDb($table, $filters = [])
    {
        global $mysql;
        $list = [];
        $filter = '1=1';
        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }

        $result = $mysql->query("SELECT * FROM $table WHERE $filter LIMIT 1;");
        return $result->fetch_assoc();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * delete from DB
     */
    public function delete()
    {
        global $mysql;
        if ($this->getId()!=null){
        $mysql->query("DELETE FROM ".$this->getTableName()." where id=".$this->getId());}

    }
    private function update()
    {
        global $mysql;
        $table=$this->getTableName();
        $vals=get_object_vars($this);
        $elements = [];
        foreach ($vals as $column => $value) {
            if (is_array($value)) {
                $value=implode(", ", $value);
            }
            $elements[] = "$column='$value'";

        }
        $colUpdate = implode(',', $elements);
        //die("UPDATE $table SET $colUpdate WHERE id=".$this->getId()."");
        $mysql->query("UPDATE $table SET $colUpdate WHERE id=".$this->getId());

    }


    private function insert(){
        global $mysql;
        $table=$this->getTableName();
        $columnsTable = '(`';
        $values = "('";
        foreach ($this as $columns=>$value) {
            if ($columns!= 'id') {
                $columnsTable .= $columns;
                $columnsTable .= "`,`";
                if (is_array($value)){
                    $value=implode(',',$value);
                }
                $values .=$value;
                $values .= "','";
            }

        }
        $columnsTable = substr($columnsTable, 0, -2);
        $values = substr($values, 0, -2);
        $columnsTable .= ')';
        $values .= ')';
        //die("INSERT INTO $table $columnsTable VALUES $values");
        $mysql->query("INSERT INTO $table $columnsTable VALUES $values;");
        $this->setId($mysql->insert_id);
    }
    public function save(){
        if ($this->getId()!=null){
            $this->update();
        }
        else{
            $this->insert();
        }
    }

    /**
     * @return string
     */
    abstract protected function getTableName();
}