<?php
class Product_category extends BaseTable
{
    /** @var  integer */
    public $category;

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    protected function getTableName()
    {
     return 'product_category';
    }
}