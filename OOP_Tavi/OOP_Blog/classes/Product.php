<?php
class Product extends BaseTable
{
    /** @var  string */
    public $name;
    /** @var  float */
    public $price;
    /** @var  boolean */
    public $stock;
    /** @var  integer */
    public $id_category;
    /** @var  string */
    public $description;
    /** @var  integer */
    public $id_manufacturer;
    /** @var  boolean */
    public $best_sellers;
    /** @var  boolean */
    public $new_arrivals;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param bool $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return int
     */
    public function getId_category()
    {
        return $this->id_category;
    }

    /**
     * @param int $id_category
     */
    public function setId_category($id_category)
    {
        $this->id_category = $id_category;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function getBest_sellers()
    {
        return $this->best_sellers;
    }

    /**
     * @param bool $best_sellers
     */
    public function setBest_sellers($best_sellers)
    {
        $this->best_sellers = $best_sellers;
    }

    /**
     * @return bool
     */
    public function getNew_arrivals()
    {
        return $this->new_arrivals;
    }

    /**
     * @param bool $new_arrivals
     */
    public function setNew_arrivals($new_arrivals)
    {
        $this->new_arrivals = $new_arrivals;
    }

    /**
     * @return int
     */
    public function getId_manufacturer()
    {
        return $this->id_manufacturer;
    }

    /**
     * @param int $id_manufacturer
     */
    public function setId_manufacturer($id_manufacturer)
    {
        $this->id_manufacturer = $id_manufacturer;
    }

    public function getCategory()
    {
        return new Product_category($this->getId_category());
    }

    public function getManufacturer()
    {
        return new Product_manufacturer($this->getId());
    }

    public function getMainImage()
    {
        return getOneUniversal('product_images',['id_produs'=>$this->getId(),'main_image'=>1]);
    }
    public function getSupplier(){
        return getOneUniversal('supplier',['id_product'=>$this->getId()]);
    }

    public function getImages(){
        return getUniversal('product_images',['id_produs'=>$this->getId()]);
    }

    protected function getTableName()
    {
     return 'product';
    }


}