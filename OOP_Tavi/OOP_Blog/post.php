<!DOCTYPE html>
<html lang="en">
<?php include_once "functions/functions.php"; ?>
<?php $post=new Blogpost($_GET['id']) //getOneUniversal('blogpost',['id'=>$_GET['id']]); ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $post->getTitle(); ?></title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" >
</head>
<body>
<div class="container">
    <?php include "new_components/header.php"; ?>
    <?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>
    <div id="content" class="row">
        <div class="col-lg-9 col-sm-12">
<div class="blog_post">
    <h1><a href="#intro" id="intro"><?php echo $post->getTitle(); ?></a></h1>
    <p>
        <?php echo $post->getContent(); ?>
    </p>
    <div class="row">
        <div class="col-6" align="left">
            <b>Posted by <a href="author.php?author=<?php echo $post->getId_author(); ?>"><?php echo $post->getAuthor()->getName(); ?></a></b>
        </div>
        <div class="col-6" align="right">
            <a href="category.php?category=<?php echo $post->getId_category(); ?>">Category : <?php echo $post->getCategory()->getName(); ?></a>
            <br />
            <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $post->getDate(); ?>"><?php echo $post->getDate(); ?></a></b></p>
        </div>
    </div>
</div>
    <form action="new_design.php">
        <td><button type="submit" >Back to Index</button></td>
    </form>
<h2> COMMENTS: </h2>
<?php foreach (getUniversal('comments',['id_post'=>$post->getId()],[],[],[]) as $comm): ?>
<div   class="comments">
    <?php if ($comm->getReview()=='Accepted') { ?>
       <hr>
    <p>
        <?php echo $comm->getComment();
        echo ' | By: '.$comm->getName().'</br>';?>
    </p>
    <?php } ?>
</div>


<?php endforeach;?>
<?php if (isset($_SESSION['user_name'])): ?>
<form action="functions/addComment.php" method="post">
<table>
    <tr>
        <th>Comment :</th>
        <td><input type="text" name="Comment"/></td>
    </tr>
    <tr>
        <td><input type="hidden" name="CommentName" value="<?php echo $_SESSION['user_name']; ?>" /></td>
    </tr>
    <tr>
        <td><button type="submit" >SUBMIT</button></td>
    </tr>
    <td><input type="hidden" name="id" value="<?php echo $post['id']; ?>"/> </td>
    <?php if ($_SESSION['type']==1){ ?>
        <td><input type="hidden" name="review" value="Accepted"/></td>
    <?php } else { ?>
    <td><input type="hidden" name="review" value=" "/></td>
    <?php }; ?>
</table>
</form>
<?php endif;?>
    </div>
    <?php include "new_components/sidebar.php"; ?>
    </div>
<?php include "new_components/footer.php" ;?>
