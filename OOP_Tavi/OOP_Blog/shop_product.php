<?php include "functions/functions.php"; ?>
<?php $product= getOneUniversal('product',['id'=>$_GET['id']]); ?>
<?php // $productImages= getUniversal('product_images',['id_produs'=>$_GET['id']]); ?>
<?php // $mainImage= getOneUniversal('product_images',['id_produs'=>$_GET['id'], 'main_image'=>1]); ?>
<?php // $productSupplier= getOneUniversal('supplier',['id_product'=>$_GET['id']]);

$stockSupplier=0;

if ($product->getSupplier()->getStock()!=null) {
    $stockSupplier=$product->getSupplier()->getStock(); }
/*if (!is_null($productSupplier)){
    $stockSupplier=$productSupplier['stock'];
}*/
?>

<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>OneUI - Admin Dashboard Template &amp; UI Framework</title>

    <meta name="description" content="OneUI - Admin Dashboard Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/img/favicons/favicon.png">

    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-192x192.png" sizes="192x192">

    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="assets/js/plugins/magnific-popup/magnific-popup.min.css">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" id="css-main" href="assets/css/oneui.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>
<body>
<!-- Page Container -->
<!--
    Available Classes:

    'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

    'sidebar-l'                  Left Sidebar and right Side Overlay
    'sidebar-r'                  Right Sidebar and left Side Overlay
    'sidebar-mini'               Mini hoverable Sidebar (> 991px)
    'sidebar-o'                  Visible Sidebar by default (> 991px)
    'sidebar-o-xs'               Visible Sidebar by default (< 992px)

    'side-overlay-hover'         Hoverable Side Overlay (> 991px)
    'side-overlay-o'             Visible Side Overlay by default (> 991px)

    'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

    'header-navbar-fixed'        Enables fixed header
-->
<div id="page-container" class="side-scroll">
    <!-- Main Container -->
    <main id="main-container">
        <!-- Hero Content -->
        <div class="bg-image" style="background-image: url('assets/img/various/ecom_product6.png');">
            <div class="bg-primary-dark-op">
                <section class="content content-full overflow-hidden">
                    <!-- Section Content -->
                    <div class="push-30-t push-30 text-center">
                        <h1 class="h2 text-white push-10 visibility-hidden" data-toggle="appear" data-class="animated fadeInDown">A.M.A.R.A. SHOPPING CITY</h1>
                        <h2 class="h5 text-white-op visibility-hidden" data-toggle="appear" data-class="animated fadeInDown">Here you can find anything</h2>
                    </div>
                    <!-- END Section Content -->
                </section>
            </div>
        </div>
        <!-- END Hero Content -->
        <section class="content content-boxed">
            <div class="row">
                <form action="shop_products.php" method="get">
                    <input type="text" name="search" placeholder="search">
                    <button type="submit">SEARCH</button>
                </form>
            </div>
        </section>
        <!-- Side Content and Product -->
        <section class="content content-boxed">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Product -->
                    <div class="block">
                        <div class="block-content">
                            <div class="row items-push">
                                <div class="col-sm-6">
                                    <!-- Images -->
                                    <div class="row js-gallery">
                                        <div class="col-xs-12 push-10">
                                            <?php $imageURL = 'uploads/' . $product->getMainImage()->getFile_name();?>
                                            <a class="img-link" href="<?php echo $imageURL; ?>">
                                                <div>
                                                    <img src="<?php echo $imageURL; ?>" alt="" width="180" height="180"/>
                                                </div>
                                            </a>
                                        </div>
                                        <?php foreach ($product->getImages() as $image): ?>
                                            <div class="col-xs-4 push-20">
                                                <div class="col-xs-1">
                                                    <?php $imageURL = 'uploads/' . $image->getFile_name(); ; ?>
                                                    <a class="img-link" href="<?php echo $imageURL; ?>">
                                                        <img src="<?php echo $imageURL; ?>" alt="" height="80" width="80"/>
                                                    </a>
                                            </div>
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                    <!-- END Images -->
                                </div>
                                <div class="col-sm-6">
                                    <!-- Vital Info -->
                                    <div class="clearfix">
                                        <div class="pull-right">
                                            <span class="h2 font-w700 text-success"><?php echo $product->getPrice(); ?> Lei</span>
                                        </div>
                                        <p><b><?php echo $product->getName(); ?></b></p>
                                        <br />
                                        <span class="h5">
                                            <span class="font-w600 text-success"><b><?php echo $product->getStock(); ?></b> IN STOCK</span>
                                        </span>
                                        <br />

                                        <span class="h5">
                                            <span class="font-w600 text-success" style="color: red"><b><?php echo $stockSupplier; ?></b> Pending from supplier</span>
                                        </span>
                                    </div>
                                    <hr>
                                    <?php if ($product->getStock()>1){ ?>
                                    <form action="addtocart.php" method="get" >
                                        <input type="number"  name="quantity" min="1" max="100">
                                        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" >
                                        <button type="submit" class="btn btn-sm btn-primary pull-left" ><i class="fa fa-plus push-5-r"></i>Add To Cart</button>
                                    </form>
                                    <?php } ?>
                                    <a type="submit" class="btn btn-sm btn-primary pull-right" href="checkout.php"><i class="fa fa-plus push-5-r"></i>Checkout</a>
                                    <hr>
                                    <p><?php echo $product->getDescription()?>
                                    <!-- END Vital Info -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Product -->
                </div>
            </div>
        </section>
        <!-- END Side Content and Product -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
        <div class="pull-right">
            Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
        </div>
        <div class="pull-left">
            <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">A.M.A.R.A Shop</a> &copy; <span class="js-year-copy">2018</span>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Apps Modal -->
<!-- Opens from the button in the header -->
<div class="modal fade" id="apps-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-sm modal-dialog modal-dialog-top">
        <div class="modal-content">
            <!-- Apps Block -->
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Apps</h3>
                </div>
                <div class="block-content">
                    <div class="row text-center">
                        <div class="col-xs-6">
                            <a class="block block-rounded" href="base_pages_dashboard.html">
                                <div class="block-content text-white bg-default">
                                    <i class="si si-speedometer fa-2x"></i>
                                    <div class="font-w600 push-15-t push-15">Backend</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <a class="block block-rounded" href="bd_dashboard.html">
                                <div class="block-content text-white bg-modern">
                                    <i class="si si-rocket fa-2x"></i>
                                    <div class="font-w600 push-15-t push-15">Boxed</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Apps Block -->
        </div>
    </div>
</div>
<!-- END Apps Modal -->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/core/jquery.slimscroll.min.js"></script>
<script src="assets/js/core/jquery.scrollLock.min.js"></script>
<script src="assets/js/core/jquery.appear.min.js"></script>
<script src="assets/js/core/jquery.countTo.min.js"></script>
<script src="assets/js/core/jquery.placeholder.min.js"></script>
<script src="assets/js/core/js.cookie.min.js"></script>
<script src="assets/js/app.js"></script>

<!-- Page JS Plugins -->
<script src="assets/js/plugins/magnific-popup/magnific-popup.min.js"></script>

<!-- Page JS Code -->
<script>
    jQuery(function () {
        // Init page helpers (Appear + Magnific Popup plugins)
        App.initHelpers(['appear', 'magnific-popup']);
    });
</script>
</body>
</html>