<!DOCTYPE html>
<?php include_once "functions/functions.php";
checkUser();
if (isset($_POST['table'])){
    $_SESSION['table']=$_POST['table'];
    $table = $_SESSION['table'];
    if (!isset($_SESSION['sort'][$table])) {
        $_SESSION['sort'][$table] = [];}}
$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if ($page == "" || $page == "1") {
    $page1 = 0;
} else {
    $page1 = ($page * 5) - 5;
}
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ADMIN PANEL</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        .active, .accordion:hover {
            background-color: #ccc;
        }

        .accordion:after {
            content: '\002B';
            color: #777;
            font-weight: bold;
            float: right;
            margin-left: 5px;
        }

        .active:after {
            content: "\2212";
        }

        .panel {
            padding: 0 18px;
            background-color: white;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div id="content" class="row">
<?php include "new_components/admin_sidebar.php"; ?>
<?php include "new_components/admin_content.php"; ?>
    </div>
</div>
</body>