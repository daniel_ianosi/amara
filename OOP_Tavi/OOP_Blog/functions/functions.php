<?php
include "dbconnect.php";
include "classes/include_classes.php";
session_start();

if (!isset($_SESSION['currentDate'])) {
    $currentDate = date('Y-m-d');
    $_SESSION['currentDate'] = $currentDate;
} else {
    $currentDate = $_SESSION['currentDate'];
}


/**
 * Extract the first worCount words from the beginning of the text
 * @param string $text
 * @param int $wordCount
 * @return string
 */
function extractFirstWords($text, $wordCount = 35)
{
    $words = explode(' ', $text, $wordCount + 1);
    $words = array_slice($words, 0, $wordCount);

    return implode(' ', $words);
}

function getOneUniversal($table, $filters = [])
{
    global $mysql;
    $filter = '1=1';
    foreach ($filters as $column => $value) {
        $filter .= ' AND ' . "$column='$value'";
    }
    //die("SELECT * FROM $table WHERE $filter LIMIT 1;");
    $result = $mysql->query("SELECT id FROM $table WHERE $filter LIMIT 1;");
    $row = $result->fetch_assoc();
    $class=ucfirst($table);
    $object = new $class($row['id']);
    return $object;

}
/** @return array-object */
function getUniversal($table, $filters = [], $groupBy = [], $filterLike = [], $filterOrder = [], $offset = NULL, $limit = NULL, $filtersColumn = [],$condition='AND'){
    global $mysql;
    $list = [];
    $condition0='AND';
    $filter = '1=1';
    $like = 'AND 1=1';
    $order = '';


    foreach ($filters as $column => $value) {
        $filter .= ' AND ' . "$column='$value'";
    }
    foreach ($filtersColumn as $column => $value) {
        $filter .= ' AND ' . "$column=$value";
    }
    if (!empty($groupBy)) {
        $groupByCondition = ' GROUP BY ' . implode(',', $groupBy);
    } else {
        $groupByCondition = '';
    }
    foreach ($filterLike as $col => $val) {
        if ($col == 'table') {
        } else {
            if (isset($val) && !is_int($val)) {
                $like .= ' '.$condition0.' ' . "$col" . " LIKE '%" . "$val" . "%'";
                $condition0=$condition;
            } else {
                $like .= ' AND ' . "$col='$val'";
            }
        }
    }
    if (!empty($filterOrder)) {

        $orderList = [];
        foreach ($filterOrder as $key => $value) {
            $orderList[] = " $key $value ";
        }
        $order = ' ORDER BY ' . implode(',', $orderList);
    }
    if (!is_null($offset) || !is_null($limit)) {
        $pages = ' LIMIT ' . "$offset" . "," . "$limit";
    } else {
        $pages = "";
    }
    //die("SELECT id FROM $table WHERE $filter $like $groupByCondition  $order $pages;");
    $result = $mysql->query("SELECT id FROM $table WHERE $filter $like $groupByCondition  $order $pages;");
    //var_dump("SELECT id FROM $table WHERE $filter $like $groupByCondition  $order $pages");
    while ($row = $result->fetch_assoc()) {
        $class=ucfirst($table);
        $obj = new $class($row['id']);
        $list[] =$obj;
    }

    return $list;
}

function getComments($id)
{
    global $mysql;
    $list = [];
    $result = $mysql->query("SELECT * FROM comments WHERE id_post='$id';");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }

    return $list;

}

function writeComments($idPost, $commentName, $comment, $review)
{
    global $mysql;
    $mysql->query("INSERT INTO comments(`id_post`, `name`, `comment`, `review`) VALUES ('$idPost','$commentName','$comment','$review')");
}

function showUniversal($filter, $table)
{
    global $mysql;
    $list = [];
    $result = $mysql->query("SELECT DISTINCT $filter FROM $table ;");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }
    return $list;
}

function ShowTables()
{
    global $mysql;
    $list = [];
    $result = $mysql->query("SHOW TABLES ;");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }
    return $list;
}

function ShowColumns($table)
{
    global $mysql;
    $list = [];
    $result = $mysql->query("SHOW COLUMNS FROM $table;");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }
    return $list;
}


function updateCommand($table, $vals)
{
    global $mysql;

    $elements = [];
    foreach ($vals as $column => $value) {
        $elements[] = "$column='$value'";
    }
    $colUpdate = implode(',', $elements);
    $mysql->query("UPDATE $table SET $colUpdate WHERE id=" . $vals['id']);
    header("Location: dashboard.php?table=$table");
    die;
}


function acceptComment($id)
{
    global $mysql;
    $mysql->query("UPDATE comments SET review='Accepted' WHERE id='$id';");
}

function getColumnsName($table)
{

    global $mysql;
    $list = [];

    $result = $mysql->query("SELECT COLUMN_NAME
                                   FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'myblog' AND TABLE_NAME = '" . $table . "';");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }
    return $list;
}

function getColumns($table)
{

    global $mysql;
    $list = [];

    $result = $mysql->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'myblog' AND TABLE_NAME = '" . $table . "';");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }
    return $list;
}

function deleteRegistration($table, $id)
{
    global $mysql;

    $result = $mysql->query("DELETE FROM $table where id=$id");

}

function insertRegistration($table)
{
    global $mysql;
    $columnsTable = '(`';
    $values = "('";
    foreach (getColumnsName($table) as $columns) {
        if ($columns['COLUMN_NAME'] != 'id') {
            $columnsTable .= $columns['COLUMN_NAME'];
            $columnsTable .= "`,`";
            $values .= $_POST[$columns['COLUMN_NAME']];
            $values .= "','";
        }

    }
    $columnsTable = substr($columnsTable, 0, -2);
    $values = substr($values, 0, -2);
    $columnsTable .= ')';
    $values .= ')';

    $result = $mysql->query("INSERT INTO $table $columnsTable VALUES $values");
    header("Location: dashboard.php?table=$table");
    die;
}

function getButtons($table, $id)
{
    $default = [
        'Edit' => "edit.php?table=$table&id=$id",
        'Delete' => "delete.php?table=$table&id=$id",
    ];

    if ($table == 'comments') {
        $default = [
            'Accept' => "accept.php?table=$table&id=$id",
            'Delete' => "delete.php?table=$table&id=$id",
        ];
    }
    if ($table == 'product') {
        $default = [
            'Edit' => "edit.php?table=$table&id=$id",
            'View' => "product_view.php?table=$table&id=$id",
            'Delete' => "delete.php?table=$table&id=$id",
        ];
    }
    if ($table == 'product_images') {
        $default = [
            'Delete' => "delete.php?table=$table&id=$id",
            'Main Image' => "main_image.php?table=$table&id=$id",
        ];
    }
    if ($table == 'orders') {
    $default = [
        'Edit' => "edit.php?table=$table&id=$id",
        'View' => "order_view.php?table=$table&id=$id",
        'Delete' => "delete.php?table=$table&id=$id",
    ];}


    return $default;
}

function uploadButton($table, $id)
{
    $default = [
        'Upload' => "upload.php?table=product_image&id=$id",
    ];
    return $default;
}

function getForm($table, $id = NULL)
{
    $columns = getColumns($table);
    if (isset($id)) {
        $data = getOneUniversal($table, ['id' => $id]);
    } else {
        $data = [];
    }
    $response = '';
    foreach ($columns as $column) {
        if ($column['DATA_TYPE'] != 'text') {
            $response .= '<tr>
                 <th>' . $column['COLUMN_NAME'] . '</th>
                 <td><input type="text" name="' . $column['COLUMN_NAME'] . '" value="' . (isset($data[$column['COLUMN_NAME']]) ? $data[$column['COLUMN_NAME']] : '') . '" /></td>
             </tr>';
        } else {
            $response .= '<tr>
                 <th>' . $column['COLUMN_NAME'] . '</th>
                 <td><textarea name="' . $column['COLUMN_NAME'] . '" >' . (isset($data[$column['COLUMN_NAME']]) ? $data[$column['COLUMN_NAME']] : '') . '</textarea></td>
             </tr>';
        }
    }

    return $response;
}

function build_calendar($month, $year, $dateArray)
{
    // Create array containing abbreviations of days of week.
    $daysOfWeek = array('S', 'M', 'T', 'W', 'T', 'F', 'S');
    // What is the first day of the month in question?
    $firstDayOfMonth = mktime(0, 0, 0, $month, 1, $year);
    // How many days does this month contain?
    $numberDays = date('t', $firstDayOfMonth);
    // Retrieve some information about the first day of the
    // month in question.
    $dateComponents = getdate($firstDayOfMonth);
    // What is the name of the month in question?
    $monthName = $dateComponents['month'];
    // What is the index value (0-6) of the first day of the
    // month in question.
    $dayOfWeek = $dateComponents['wday'];
    // Create the table tag opener and day headers
    $calendar = "<table class='calendar'><a href='previous_month.php'><<= </a>";
    $calendar .= "<caption>$monthName $year</caption>";
    $calendar .= "<tr><a href='next_month.php'> =>></a>";
    // Create the calendar headers
    foreach ($daysOfWeek as $day) {
        $calendar .= "<th class='header'>$day</th>";
    }
    // Create the rest of the calendar
    // Initiate the day counter, starting with the 1st.
    $currentDay = 1;
    $calendar .= "</tr><tr>";
    // The variable $dayOfWeek is used to
    // ensure that the calendar
    // display consists of exactly 7 columns.
    if ($dayOfWeek > 0) {
        $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>";
    }
    $month = str_pad($month, 2, "0", STR_PAD_LEFT);
    while ($currentDay <= $numberDays) {
        // Seventh column (Saturday) reached. Start a new row.
        if ($dayOfWeek == 7) {
            $dayOfWeek = 0;
            $calendar .= "</tr><tr>";
        }
        $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
        $date = "$year-$month-$currentDayRel";
        $calendar .= "<td class='day' rel='$date'><a href='date.php?date=$date'>$currentDay</a></td>";
        // Increment counters
        $currentDay++;
        $dayOfWeek++;
    }
    // Complete the row of the last week in month, if necessary
    if ($dayOfWeek != 7) {
        $remainingDays = 7 - $dayOfWeek;
        $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>";
    }
    $calendar .= "</tr>";
    $calendar .= "</table>";
    return $calendar;
}

function productQuery($id)
{
    global $mysql;
    $list = [];
    $result = $mysql->query("SELECT product.id, product.name, product.price, product.stock, product_category.category as Category, product.description as Description, product_manufacturer.name as Manufacturer 
                                        FROM product, product_category, product_manufacturer 
                                        WHERE product.id_category=product_category.id AND product.id_manufacturer=product_manufacturer.id AND product.id=$id;");
    while ($row = $result->fetch_assoc()) {
        $list[] = $row;
    }
    return $list;
}


function getProductThumb($product)
{
    $mainImage = getOneUniversal('product_images',['id_produs'=>$product['id']]);
    $categoryProduct = getOneUniversal('product_category',['id'=>$product['id_category']]);
    $productSupplier = getOneUniversal('supplier',['id_product'=>$product['id']]);
    $stockSupplier=0;
    if (!is_null($productSupplier)){
    $stockSupplier=$productSupplier['stock'];
    }

    $imageURL = '../admin/uploads/' . $mainImage["file_name"];

    $result=
       '
        <div class="col-sm-6 col-lg-3">
            <div class="block">
                <div class="img-container">
                   
                    <img src="'. $imageURL.'" width="150" height="150" alt="">
                    <div class="img-options">
                        <div class="img-options-content">
                            <a class="btn btn-sm btn-default"
                                   href="shop_product.php?id='.$product['id'].'">View</a>
                            </div>
                            <div class="text-warning">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <span class="text-white">(690)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content" style="height: 150px">
                    <div class="push-10">
                        <div class="h4 font-w600 text-success pull-right push-10-l">'.$product["price"].'
                            Lei
                        </div>
                       
                        <span class="h4">
                        <span class="font-w600 text-success"><b>'.$product['stock'].'</b> IN STOCK</span>
                        </span>
                        <br>
                        <span class="h6">
                        <span class="font-w600 text-success" style="color: red"><b>'.$stockSupplier.'</b> Pending from supplier</span>
                        </span>
                        <br>
                        <a class="h4"
                           href="shop_product.php?id='.$product['id'].'">'.$product["name"].'</a>
                           <p class="text-muted">'.$categoryProduct['category'].'</p>
                    </div>
                    
                </div>
            </div>
      ';
   return $result;
}
function addUniversal($table, $vals){
    global $mysql;
    $columns='(`'.implode('`,`', array_keys($vals)).'`)';
    $values="('".implode("','", $vals)."')";


    $mysql->query("INSERT INTO $table $columns VALUES $values");


    return $mysql->insert_id;
}

function countRegistrations($table){
    global $mysql;
    $result= $mysql->query("SELECT COUNT(*) FROM $table");
    $row=$result->fetch_assoc();
    return $row['COUNT(*)'];
}
function checkUser(){
    if (!isset($_SESSION['user_name'])){
        header("Location: login.php?message=Nu esti autentificat.");
        die;
    }
}

?>