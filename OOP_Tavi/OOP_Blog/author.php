<!DOCTYPE html>
<html lang="en">
<?php include_once "functions/functions.php"; ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AUTHOR</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" >
</head>
<body>
<div class="container">
    <?php include "new_components/header.php"; ?>
<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>
<?php $author=new Authors($_GET['author']); //getOneUniversal('authors',['id'=>$_GET['author']]); ?>
<h1>MORE FROM AUTHOR <?php echo $author->getName(); ?></h1>
    <div id="content" class="row">
    <div class="col-lg-9 col-sm-12">
<?php foreach (getUniversal('blogpost',['id_author'=>$_GET['author']],[],[]) as $post): ?>
<div class="blog_post">
    <h1><a href="post.php?id=<?php echo $post->getId(); ?>" id="intro"><?php echo $post->getTitle(); ?></a></h1>
    <p>
        <?php echo extractFirstWords($post->getContent()); ?> ...
    </p>
    <div class="row">
        <div class="col-6" align="left">
        <b>Posted by <a href="author.php?author=<?php echo $post->getId_author(); ?>"><?php echo $post->getAuthor()->getName(); ?></a></b>
        </div>
        <div class="col-6" align="right">
        <a href="category.php?category=<?php echo $post->getId_category(); ?>">Category : <?php echo $post->getCategory()->getName(); ?></a>
            <br />
            <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $post->getDate(); ?>"><?php echo $post->getDate(); ?></a></b></p>
        </div>
    </div>
</div>
<?php endforeach;?>
        <form action="new_design.php">
            <td><button type="submit" >Back to Index</button></td>
        </form>
    </div>
        <?php include "new_components/sidebar.php"; ?>
    </div>

<?php include "new_components/footer.php" ;?>
