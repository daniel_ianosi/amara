<?php

// Include the database configuration file
include "functions/functions.php";

$statusMsg = '';
// File upload path
$targetDir = "uploads/";
$fileName = basename($_FILES["file"]["name"]);
$targetFilePath = $targetDir . $fileName;
$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

if(isset($_POST["submit"]) && !empty($_FILES["file"]["name"])){
    // Allow certain file formats
    $allowTypes = array('jpg','png','jpeg','gif');
    if(in_array($fileType, $allowTypes)){
        // Upload file to server
        if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
            // Insert image file name into database
            if (isset($_POST['main_image'])) {
                $oldImage=getOneUniversal('product_images',['id_produs' => $_SESSION['id'],'main_image'=>1]);
                $oldImage->setMain_image(0);
                $oldImage->save();
                $newimage=new Product_images();
                $newimage->setId_produs($_SESSION['id']);
                $newimage->setMain_image(1);
                $newimage->setFile_name($fileName);
                $newimage->save();
                $insert=TRUE;
                /* $replace = $mysql->query("UPDATE product_images SET main_image='0' WHERE main_image='1' AND id_produs='$_POST[id]'");
                $insert = $mysql->query("INSERT into product_images (id_produs,file_name,main_image) VALUES ('$_POST[id]','$fileName','$_POST[main_image]') "); */
            } else {
                /* $insert = $mysql->query("INSERT into product_images (id_produs,file_name,main_image) VALUES ('$_POST[id]','$fileName','0') "); */
                $newimage = new Product_images();
                $newimage->setId_produs($_SESSION['id']);
                $newimage->setMain_image(0);
                $newimage->setFile_name($fileName);
                $newimage->save();
                $insert = TRUE;
            }
            if($insert){
                $statusMsg = "The file ".$fileName. " has been uploaded successfully.";
            }else{
                $statusMsg = "File upload failed, please try again.";
            }
        }else{
            $statusMsg = "Sorry, there was an error uploading your file.";
        }
    }else{
        $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
    }
}else{
    $statusMsg = 'Please select a file to upload.';
}

header("Location: product_view.php?table=$_SESSION[table]&id=$_SESSION[id]&message=$statusMsg");

?>
