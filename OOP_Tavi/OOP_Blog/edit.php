<!DOCTYPE html>
<?php include_once "functions/functions.php";
checkUser();
if (isset($_POST['table'])){
    $_SESSION['table']=$_POST['table'];
    $table = $_SESSION['table'];
    if (!isset($_SESSION['sort'][$table])) {
        $_SESSION['sort'][$table] = [];}}
$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if ($page == "" || $page == "1") {
    $page1 = 0;
} else {
    $page1 = ($page * 5) - 5;
}
?>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ADMIN PANEL</title>
        <link rel="stylesheet" href="../../css/bootstrap.min.css" >
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .accordion {
                background-color: #eee;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
                transition: 0.4s;
            }

            .active, .accordion:hover {
                background-color: #ccc;
            }

            .accordion:after {
                content: '\002B';
                color: #777;
                font-weight: bold;
                float: right;
                margin-left: 5px;
            }

            .active:after {
                content: "\2212";
            }

            .panel {
                padding: 0 18px;
                background-color: white;
                max-height: 0;
                overflow: hidden;
                transition: max-height 0.2s ease-out;
            }
        </style>
    </head>
<?php

$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if ($page == "" || $page == "1") {
    $page1 = 0;
} else {
    $page1 = ($page * 5) - 5;
}
?>
    <body>
    <div class="container">
        <div id="content" class="row">
<?php include "new_components/admin_sidebar.php"; ?>

        <div class="col-9">
            <div class="row">
                <div class="col-4" style="border: solid">
                        NUMAR POSTURI: <?php echo countRegistrations('blogpost')//['COUNT(*)']; ?>
                </div>
                <div class="col-4" style="border: solid">
                        NUMAR PRODUSE: <?php echo countRegistrations('product')//['COUNT(*)']; ?>
                </div>
                <div class="col-4" style="border-bottom: double; border-bottom-color: blue; border-top: double;border-top-color: red; border-right: double; border-right-color: yellow;border-left:double;border-left-color: yellow">
                        NUMAR COMMENTURI: <?php echo countRegistrations('comments')//['COUNT(*)']; ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12">
                    <table border="2">
                    <form action="save.php" method="post">

                        <?php $class=ucfirst($_GET['table']);
                        if (isset($_GET['id'])) {
                            $object=new $class($_GET['id']);}
                        else { $object=new $class();}
                        $vars=get_object_vars($object);
                        foreach ($vars as $col=>$value): ?>
                            <?php if ($col<>'id'): ?>
                                <tr>
                                    <th> <?php echo $col; ?> </th>
                                    <td><textarea name="<?php echo $col; ?>"><?php echo $value; ?></textarea></td>
                                </tr>
                            <?php endif; ?>
                             <?php endforeach;?>
                            <tr>
                                <td><input type="hidden" name="id" value="<?php echo $object->getId(); ?>">
                                    <button type="submit">Update Registration</button></td>
                            </tr>
                    </form>
                    </table>
                </div>
            </div>


    </div>
    </div>
    </div>
    </body>
