<?php include "functions/functions.php";
checkUser();
$_SESSION['id']=$_GET['id'];
$class=ucfirst($_SESSION['table']);
$object = new $class($_SESSION['id']);?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ADMIN PANEL</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        .active, .accordion:hover {
            background-color: #ccc;
        }

        .accordion:after {
            content: '\002B';
            color: #777;
            font-weight: bold;
            float: right;
            margin-left: 5px;
        }

        .active:after {
            content: "\2212";
        }

        .panel {
            padding: 0 18px;
            background-color: white;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div id="content" class="row">
        <?php include "new_components/admin_sidebar.php"; ?>
        <div class="col-9">
            <div class="row">
                <div class="col-4" style="border: solid">
                    NUMAR POSTURI: <?php echo countRegistrations('blogpost')//['COUNT(*)']; ?>
                </div>
                <div class="col-4" style="border: solid">
                    NUMAR PRODUSE: <?php echo countRegistrations('product')//['COUNT(*)']; ?>
                </div>
                <div class="col-4" style="border-bottom: double; border-bottom-color: blue; border-top: double;border-top-color: red; border-right: double; border-right-color: yellow;border-left:double;border-left-color: yellow">
                    NUMAR COMMENTURI: <?php echo countRegistrations('comments')//['COUNT(*)']; ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12" align="center">
                    <?php if ($object->getMainImage()->getId()!=null){
                    $imageURL = 'uploads/'.$object->getMainImage()->getFile_name();?>
                    <img src="<?php echo $imageURL; ?>" width="180" height="180" />
                    <?php } else { ?>
                    <p>No image found...</p>
                    <?php } ?>
                    <p align="center"><?php echo $object->getName() ?></p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12">
                    <div align="left">
                        <form action="upload.php" method="post" enctype="multipart/form-data" align="left">
                            <!--  Select Image File to Upload:-->
                            <input type="file" name="file">
                            <input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
                            <!--hidden trece o valoare in post ($_POST['id'] - folosit in fisier uploads.php)-->
                            <input type="submit" name="submit" value="Upload">
                            <input type="checkbox" name="main_image" value="1">Main Image
                        </form>
                     </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-6">
                    <table border="1">
                        <tr>
                        <th><p align="center"> image </p></th>
                        <th><p align="center"> id </p></th>
                        <?php foreach (get_class_vars(Product_images::class) as $column=>$value): ?>
                        <?php if ($column<>'id'): ?>
                        <th> <p align="center"> <?php echo $column ?> </p></th>
                        <?php endif; endforeach; ?>
                        <th><p align="center"> Actions </p></th>
                        </tr>
                        <?php foreach (getUniversal('product_images',['id_produs' => $_SESSION['id']] , [], [], []) as $object2):?>
                        <tr>
                        <td><img src="uploads/<?php echo $object2->getFile_name(); ?> " width="80" height="80" /></td>
                        <td><?php echo $object2->getID(); ?></td>
                        <?php foreach (get_object_vars($object2) as $col=>$val): ?>
                        <?php if ($col <>'id'): ?>
                        <td> <?php echo $val; ?> </td>
                        <?php endif; endforeach; ?>
                        <td> <?php foreach (getButtons('product_images', $object2->getId()) as $action => $link): ?>
                        <a href="<?php echo $link ?>"><?php echo $action; ?></a>
                        <?php endforeach; ?>
                        </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                    <br>
                <div class="col-6">
                    <p>Name: <?php echo $object->getName(); ?></p>
                    <p>Category: <?php echo $object->getCategory()->getCategory(); ?></p>
                    <p>Price: <?php echo $object->getPrice(); ?> lei</p>
                    <p>Stock: <?php echo $object->getStock(); ?> buc</p>
                    <p>Description: <?php echo $object->getDescription(); ?></p>
                    <p>Manufacturer: <?php echo $object->getManufacturer()->getName(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
</body>
