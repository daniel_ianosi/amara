<?php include_once "functions/functions.php";?>
<div class="col-3 d-none d-sm-block">
    <h3>MORE ABOUT:</h3>
    <ul class="submenu">
        <?php foreach (getUniversal('blogpost',[],['id_category'],[]) as $post):?>
            <li> <a href="category.php?category=<?php echo $post->getId_category(); ?>"> <?php echo $post->getCategory()->getName(); ?></a></li>
        <?php endforeach;?>
    </ul>
    <h3>MORE FROM:</h3>
    <ul class="submenu">
        <?php foreach (getUniversal('blogpost',[],['id_author'],[]) as $post): ?>
            <li> <a href="author.php?author=<?php echo $post->getId_author(); ?>"> <?php echo $post->getAuthor()->getName(); ?></a></li>
        <?php endforeach;?>
    </ul>
    <?php echo build_calendar(date('m', strtotime($_SESSION['currentDate'])),date('Y', strtotime($_SESSION['currentDate'])), 1); ?>
    <?php if (!isset($_SESSION['user_name'])): ?>
        <h3>Login</h3>
        <form action="login_check_blog.php" method="post">
            <?php if (isset($_GET['message'])){?>
                <h1 style="color: red"><?php echo $_GET['message']; ?></h1>
            <?php }; ?>
            <table>
                <tr>
                    <th>User:</th>
                    <td><input type="text" name="user_name" size="15" /></td>
                </tr>
                <tr>
                    <th>Password:</th>
                    <td><input type="password" name="password" size="15" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><button type="submit" >Login</button></td>
                </tr>
            </table>
        </form>
    <?php endif; if (isset($_SESSION['user_name'])): ?>
    <h3><?php echo 'WELCOME '.strtoupper($_SESSION['user_name']); ?></h3>
    <ul class="submenu">
        <li><a href="logout_blog.php">LOG OUT</a> </li>
    </ul>
    <ul class="submenu">
        <?php if ($_SESSION['type']=='1'){ ?> <li><a href="admin.php?message=Welcome" ><?php echo 'ADMIN'; ?> </a></li> <?php }; ?>
        <?php endif; ?>
    </ul>

</div>