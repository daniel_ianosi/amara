<?php include_once "functions/functions.php"; ?>
<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>

<?php
$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if($page=="" || $page=="1")
{
    $page1 = 0;
}
else
{
    $page1 = ($page*5)-5;
}
?>
<div class="col-lg-9 col-sm-12">

    <div class="row">
        <?php foreach (getUniversal('blogpost',[],[],[],[],$page1,5) as $object):?>
        <div class="col-12">
            <h2><a href="post.php?id=<?php echo $object->getId(); ?>" id="intro"><?php echo $object->getTitle(); ?></a></h2>
            <hr />
            <p><?php echo extractFirstWords($object->getContent()); ?> ...</p>
            <hr />
            <div class="row">
                <div class="col-6" align="left">
                    <b>Posted by <a href="author.php?author=<?php echo $object->getId_author(); ?>" author="intro"><?php echo $object->getAuthor()->getName(); ?></a></b>
                </div>
                <div class="col-6" align="right">
                    <a href="category.php?category=<?php echo $object->getId_category(); ?>" category="intro">Category : <?php echo $object->getCategory()->getName(); ?></a>
                    <br />
                    <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $object->getDate(); ?>"><?php echo $object->getDate(); ?></a></b></p>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
    <div class="col-12" aling="center">
        <?php
        $nrPagini=ceil(count(getUniversal('blogpost'))/5);
        echo "<br>";
        echo ("Pagina : ");
        for ($i=1;$i<=$nrPagini;$i++)
        {    ?>
            <a href="new_design.php?page=<?php echo $i; ?>" style="text-decoration:none"><?php  echo $i."  ";  ?></a>
        <?php } ?>
    </div>
</div>


