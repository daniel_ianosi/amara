<?php include_once "functions/functions.php"; ?>
<div class="col-sm-5 col-md-5 col-l-3 col-xl-3 " background="darkgrey">
    <div class="p-3 mb-2 bg-secondary text-black">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .accordion {
                background-color: #eee;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
                transition: 0.4s;
            }

            .active, .accordion:hover {
                background-color: #ccc;
            }

            .accordion:after {
                content: '\002B';
                color: #777;
                font-weight: bold;
                float: right;
                margin-left: 5px;
            }

            .active:after {
                content: "\2212";
            }

            .panel {
                padding: 0 18px;
                background-color: white;
                max-height: 0;
                overflow: hidden;
                transition: max-height 0.2s ease-out;
            }
        </style>
        <h2>ADMIN PANEL</h2>
        <button class="accordion" style="color:darkred">BLOG ADMIN</button>
        <div class="panel">
            <form action="admin.php" method="post" >
            <button type="submit" name="table" value="authors">Authors</button></br>
            <button type="submit" name="table" value="blogpost">Blogpost</button></br>
            <button type="submit" name="table" value="category">Category</button></br>
            <button type="submit" name="table" value="comments">Comments</button>
            </form>
        </div>
        <button class="accordion" style="color:darkred">SHOP ADMIN</button>
        <div class="panel">
            <form action="admin.php" method="post" >
            <button type="submit" name="table" value="product">Product</button></br>
            <button type="submit" name="table" value="product_category">Category</button></br>
            <button type="submit" name="table" value="product_manufacturer">Manufacturer</button></br>
            <button type="submit" name="table" value="supplier">Supplier</button>
            </form>
        </div>
        <button class="accordion" style="color:darkred">ORDERS ADMIN</button>
        <div class="panel">
            <form action="admin.php" method="post">
            <button type="submit" name="table" value="orders">Orders</button>
            </form>
        </div>

        <script>
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.maxHeight){
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
            }
        </script>
        <br>
        <form action="logout.php">
            <button type="submit">LOG OUT</button>
        </form>
        </div>

</div>


