<?php include_once "functions/functions.php"; ?>
<div class="col-sm-7 col-md-7 col-l-9 col-xl-9 ">
    <div class="row">
    <div class="col-4" style="border: solid">
       NUMAR POSTURI: <?php echo countRegistrations('blogpost')//['COUNT(*)']; ?>
    </div>
    <div class="col-4" style="border: solid">
            NUMAR PRODUSE: <?php echo countRegistrations('product')//['COUNT(*)']; ?>
    </div>
    <div class="col-4" style="border-bottom: double; border-bottom-color: blue; border-top: double;border-top-color: red; border-right: double; border-right-color: yellow;border-left:double;border-left-color: yellow">
            NUMAR COMENTURI: <?php echo countRegistrations('comments')//['COUNT(*)']; ?>
    </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <?php if (!empty($_SESSION['table'])) { ?>
                <div>
                    <a href="edit.php?table=<?php echo $_SESSION['table']; ?>">
                        <button style="font-size:16px;color:darkred"> Adauga post nou <i style="color:red" class="fa fa-plus"></i> </button>
                    </a>
                </div>
            <?php } else { } ?>
        </div>
    </div>
    <br>
        <div class="row">
        <div class="col-12">
            <?php if (isset($_SESSION['table'])): ?>
                <form action="filt.php" method="post">
                    <table border="2">
                        <div>
                            <tr>
                                <th style="color:darkred">id</th>
                                <td><input type="text" name="id"></td>
                            </tr>
                            <?php foreach (get_class_vars(ucfirst($_SESSION['table'])) as $column=>$value): ?>
                            <tr>
                                <?php if ($column<>'id') :?>
                                <th style="color:darkred"><?php echo $column; ?> </th>
                                <td><input type="text" name="<?php echo $column; ?>"></td>
                                <?php endif ?>
                            </tr>
                                <?php endforeach;?>
                        </div>
                        <tr>
                            <td>
                                <button type="submit" style="font-size:16px;color:darkred">SEARCH</button>
                            </td>
                        </tr>
                    </table>
                    <td><input type="hidden" name="table" value="<?php echo $_SESSION['table']; ?>"/></td>
                </form>
                <br>
                <?php if (isset($_SESSION['condition'])):
                if ($_SESSION['condition']==TRUE): ?>
                <form action="nofilt.php" method="post">
                    <button type="submit" style="font-size:16px;color:red">RESET FILTER</button>
                    <input type="hidden" name="table" value="<?php echo $_SESSION['table']; ?>"/>
                </form>
                    <?php endif; endif; ?>
            <?php endif; ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <?php if (isset($_SESSION['table'])): ?>
            <table border="1">
                        <tr>
                    <?php if ($_SESSION['table']=='product'){ ?>
                        <th valign="top">image</th>
                    <?php } else {} ?>
                    <th valign="top">
                        <p align="center"> id </p>
                        <p align="center">
                        <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=id&sorttype=ASC" ?>"
                        >&#9650</a>
                        <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=id&sorttype=DESC" ?>"
                        >&#9660</a>
                        <?php if (isset($_SESSION['sort'][$_SESSION['table']]['id'])): ?>
                        <a href="sort.php?table=<?php echo $_SESSION['table'] . "&clearFilter=id" ?>"
                           style="color:red;">X</a>
                        </p>
                        <?php endif; ?>
                    </th>
                    <?php foreach (get_class_vars(ucfirst($_SESSION['table'])) as $column=>$value): ?>
                <?php if ($column<>'id'): ?>
                        <th valign="top">
                            <p align="center"><?php echo $column ?></p>
                            <p align="center">
                                <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=" . $column . "&sorttype=ASC" ?>"
                                >&#9650</a>
                                <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=" . $column . "&sorttype=DESC" ?>"
                                >&#9660</a>
                                <?php if (isset($_SESSION['sort'][$_SESSION['table']][$column])): ?>
                                    <a href="sort.php?table=<?php echo $_SESSION['table'] . "&clearFilter=" . $column ?>"
                                       style="color:red;">X</a>
                                <?php endif; ?>
                            </p>
                </th>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <th valign="top">Actions</th>
                </tr>
                <?php if (!isset($_SESSION['filter']['table'])){ $_SESSION['filter']['table']=[1=>1];}
                    foreach (getUniversal($_SESSION['table'], [], [], $_SESSION['filter']['table'],$_SESSION['sort'][$_SESSION['table']],$page1,5) as $object): ?>
                    <tr>
                    <?php if ($_SESSION['table']=='product'){ ?>
                    <td>
                        <img src="uploads/<?php echo $object->getMainImage()->getFile_name(); ?> " width="80" height="80" />
                    </td>
                    <?php } else {} ?>
                    <td>
                        <?php echo $object->getId(); ?>
                    </td>
                    <?php foreach (get_class_vars(ucfirst($_SESSION['table'])) as $column=>$value):
                       if ($column<>'id'):
                           $getter='get'.ucfirst($column)?>
                    <td>
                        <?php echo extractFirstWords($object->$getter());?>
                    </td>
                    <?php endif;
                    endforeach;?>
                    <td>
                        <?php foreach (getButtons($_SESSION['table'], $object->getId(),$page1/5+1) as $action => $link): ?>
                            <a href="<?php echo $link ?>"><?php echo $action; ?></a>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
            <?php endif; ?>
        </div>
        <?php if (isset($_SESSION['table'])):
            $nrPagini = ceil(count(getUniversal($_SESSION['table'], [], [], $_SESSION['filter']['table'])) / 5);
            echo("Pagina : ");
            for ($i = 1; $i <= $nrPagini; $i++) {?>
                <a href="admin.php?table=<?php echo $_SESSION['table'] ?>&page=<?php echo $i; ?>"
                     style=" text-decoration:none"><?php echo $i . "  "; ?></a> <?php
            }
        endif;
        ?>
    </div>
</div>
