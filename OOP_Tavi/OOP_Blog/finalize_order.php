<?php
include ("functions/functions.php");

$neworder=new Orders();
$vals=get_object_vars($neworder);
foreach ($vals as $atr=>$val){
    if ($atr<>'id'){
    $setter='set'. ucfirst($atr);
    $neworder->$setter($_POST[$atr]);
}}
$neworder->save();
//$orderId = addUniversal('orders',$_POST);


foreach ($_SESSION['cart'] as $id=>$qty){
    $product=getOneUniversal('product',['id'=>$id]);
    $orderItem=new Order_items();
    $orderItem->setId_order($neworder->getId());
    $orderItem->setId_product($id);
    $orderItem->setQuantity($qty);
    $orderItem->setProduct_name($product->getName());
    $orderItem->setPrice($product->getPrice());
    $orderItem->save();
    $product->setStock($product->getStock()-$qty);
    $product->save();
}

unset($_SESSION['cart']);

header("Location: shop_home.php");

die;
?>


