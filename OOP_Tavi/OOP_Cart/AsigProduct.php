<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/11/2018
 * Time: 11:44 AM
 */
class AsigProduct extends BaseProduct
{
    /**
     * @var int
     */
    public $period;

    public function __construct($id, $name, $description, $period)
    {
        $price=90*$period;
        parent::__construct($id, $name, $price, $description, TRUE);
    }

    /**
     * @return int
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param int $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }


}