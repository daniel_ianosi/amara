<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/10/2018
 * Time: 8:34 PM
 */
class ExtraW extends BaseProduct
{
    /**
     * @var int
     */
    public $percent;
    /**
     * @var BaseProduct
     */
    public $parentProduct;

    /**
     * ExtraW constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param bool $inStock
     * @param float $percent
     * @param BaseProduct $parentProduct
     */
    public function __construct($id, $name, $description, $percent,$parentProduct)
    {

        $this->percent = $percent;
        $this->parentProduct = $parentProduct;
        $price=$parentProduct->price*$percent/100;
        parent::__construct($id, $name, $price, $description, TRUE);
    }

    /**
     * @return int
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @return BaseProduct
     */
    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    /**
     * @param BaseProduct $parentProduct
     */
    public function setParentProduct($parentProduct)
    {
        $this->parentProduct = $parentProduct;
    }

    /**
     * @param int $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->getParentProduct()->price*$this->getPercent()/100;
    }


}