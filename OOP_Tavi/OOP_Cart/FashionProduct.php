<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 8:03 PM
 */
class FashionProduct extends BaseProduct
{
    /** @var array  */
    public $sizes=[];

    public $currentSize;

   public function __construct($id, $name, $price, $description, $sizes, $currentSize)
    {
        parent::__construct($id, $name, $price, $description, TRUE);
        $this->sizes = $sizes;
        $this->currentSize = $currentSize;

    }

    public function getPrice()
    {
        switch ($this->currentSize){
            case 'S':  return parent::getPrice();
            case 'L':  return parent::getPrice()*1.1;
            case 'XL':  return parent::getPrice()*1.2;
            case 'XXL':  return parent::getPrice()*1.3;
        }
    }


}