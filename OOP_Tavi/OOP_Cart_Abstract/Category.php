<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/19/2018
 * Time: 8:23 PM
 */
class Category extends BaseTable
{
    /** @var  string */
    public $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    protected function getTableName()
    {
        return 'category';
    }
}