<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 7:05 PM
 */
abstract class BaseProduct extends BaseTable
{
    /** @var  string */
    public $name;
    /** @var  float */
    public $price;
    /** @var  string */
    public $description;
    /** @var  boolean */
    public $inStock;

    /** @var  integer */
    public $categoryId;

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }



    /**
     * @return boolean
     */
    abstract public function isStockable();

    /**
     * @return int
     */


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isInStock()
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     */
    public function setInStock($inStock)
    {
        if ($this->isStockable()) {
            $this->inStock = $inStock;
        } else {
            $this->inStock = true;
        }
    }
}