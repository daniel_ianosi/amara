<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 8:03 PM
 */
class FashionProduct extends BaseProduct
{
    /** @var array  */
    public $sizes=[];

    public $curentSize;

    public function getPrice()
    {
        switch ($this->curentSize){
            case 'S':  return parent::getPrice();
            case 'L':  return parent::getPrice()*1.1;
            case 'XL':  return parent::getPrice()*1.2;
            case 'XXL':  return parent::getPrice()*1.3;
        }
    }


    /**
     * @return boolean
     */
    public function isStockable()
    {
        return true;
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return 'fashion_product';
    }

    /**
     * @return array
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * @param array $sizes
     */
    public function setSizes($sizes)
    {
        if (is_array($sizes)){
            $this->sizes=$sizes;
        }
        else {
            $this->sizes = (explode(',', $sizes));
        }
    }

    /**
     * @return mixed
     */
    public function getCurentSize()
    {
        return $this->curentSize;
    }

    /**
     * @param mixed $currentSize
     */
    public function setCurentSize($curentSize)
    {
        $this->curentSize = $curentSize;
    }


}