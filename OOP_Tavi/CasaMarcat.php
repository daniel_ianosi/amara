<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/5/2018
 * Time: 7:09 PM
 */
class CasaMarcat
{

    public $sold;

    /**
     * CasaMarcat constructor.
     * @param $id
     * @param $sold
     */
    public function __construct( $sold)
    {
        $this->sold = $sold;
    }

    public function in($amount){
        $this->sold+=$amount;
    }
}