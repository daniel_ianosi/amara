<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/5/2018
 * Time: 7:13 PM
 */
class POS
{
    public $cont;


    /**
     * POS constructor.
     * @param $cont
     */
    public function __construct(Cont $cont)
    {
        $this->cont = $cont;
    }

    public function in($amount){
        $this->cont->in($amount);
    }

}