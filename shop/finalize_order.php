<?php
include ("../functions.php");
$orderId = addUniversal('orders',$_POST);

foreach ($_SESSION['cart'] as $id=>$qty){
    $product=getOneUniversal('product',['id'=>$id]);
    addUniversal('order_items',['id_order'=>$orderId, 'id_product'=>$id, 'quantity'=>$qty,'product_name'=>$product['name'],'price'=>$product['price']]);
}
unset($_SESSION['cart']);

header("Location: shop_home.php");

die;
?>


