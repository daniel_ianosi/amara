<?php
include "../functions/functions.php";
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>OneUI - Admin Dashboard Template &amp; UI Framework</title>

    <meta name="description"
          content="OneUI - Admin Dashboard Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/img/favicons/favicon.png">

    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-192x192.png" sizes="192x192">

    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" id="css-main" href="assets/css/oneui.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>
<body>
<!-- Page Container -->
<!--
    Available Classes:

    'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

    'sidebar-l'                  Left Sidebar and right Side Overlay
    'sidebar-r'                  Right Sidebar and left Side Overlay
    'sidebar-mini'               Mini hoverable Sidebar (> 991px)
    'sidebar-o'                  Visible Sidebar by default (> 991px)
    'sidebar-o-xs'               Visible Sidebar by default (< 992px)

    'side-overlay-hover'         Hoverable Side Overlay (> 991px)
    'side-overlay-o'             Visible Side Overlay by default (> 991px)

    'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

    'header-navbar-fixed'        Enables fixed header
-->
<div id="page-container" class="side-scroll">

    <!-- Main Container -->
    <main id="main-container">
        <!-- Hero Content -->
        <div class="bg-image" style="background-image: url('assets/img/photos/photo3@2x.jpg');">
            <div class="bg-primary-dark-op">
                <section class="content content-full content-boxed overflow-hidden">
                    <!-- Section Content -->
                    <div class="push-30-t push-30 text-center">
                        <h1 class="h2 text-white push-10 visibility-hidden" data-toggle="appear"
                            data-class="animated fadeInDown">Welcome to our Digital Store!</h1>
                        <h2 class="h5 text-white-op visibility-hidden" data-toggle="appear"
                            data-class="animated fadeInDown">Feel free to explore.</h2>
                    </div>
                    <!-- END Section Content -->
                </section>
            </div>
        </div>
        <!-- END Hero Content -->
        <section class="content content-boxed">
            <div class="row">
                <form action="shop_products.php" method="get">
                    <input type="text" name="search" placeholder="search">
                    <button type="submit">SEARCH</button>
                </form>
            </div>
        </section>

        <!-- Products -->
        <section class="content content-boxed">

            <!-- New arrivals Marius -->

            <h3 class="font-w400 text-black push-20">New arrivals</h3>
            <div class="row">
                <?php
                foreach (getUniversal('product', ['newArrivals' => 1], [], [], ['id' => 'ASC'], 0, 4, []) as $newArrivalObject):
                $stockSupplier = 0;
                if (!is_null($newArrivalObject->getSupplier()->getId()))
                {
                    $stockSupplier = $newArrivalObject->getSupplier()->getStock();
                }
                $imageURL = '../admin/uploads/' . $newArrivalObject->getMainImage()->getFileName();
                ?>

                <div class="col-sm-6 col-lg-3">
                    <div class="block">
                        <div class="img-container">

                            <img src="<?php echo $imageURL; ?>" width="150" height="150" alt="">
                            <div class="img-options">
                                <div class="img-options-content">
                                    <a class="btn btn-sm btn-default"
                                       href="shop_product.php?id=<?php echo $newArrivalObject->getId();?>">View</a>
                                </div>
                                <div class="text-warning">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <span class="text-white">(690)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content" style="height: 150px">
                        <div class="push-10">
                            <div class="h4 font-w600 text-success pull-right push-10-l"><?php echo $newArrivalObject->getPrice(); ?>
                                Lei
                            </div>

                            <span class="h4">
                        <span class="font-w600 text-success"><b><?php echo $newArrivalObject->getStock(); ?></b> IN STOCK</span>
                        </span>
                            <br>
                            <span class="h6">
                        <span class="font-w600 text-success" style="color: red"><b><?php echo $stockSupplier; ?></b> Pending from supplier</span>
                        </span>
                            <br>
                            <a class="h4"
                               href="shop_product.php?id=<?php echo $newArrivalObject->getId();?>"><?php echo $newArrivalObject->getName(); ?></a>
                            <p class="text-muted"><?php echo $newArrivalObject->getCategory()->getName(); ?></p>
                        </div>

                    </div>
                </div>
                <?php endforeach; ?>
            </div>


            <!-- New best sellers Marius -->
            <h3 class="font-w400 text-black push-20">Best Sellers</h3>
            <div class="row">
                <?php
                foreach (getUniversal('product', ['bestSellers' => 1], [], [], ['id' => 'ASC'], 0, 4, []) as $bestSellerObject):
                    $stockSupplier = 0;
                    if (!is_null($bestSellerObject->getSupplier()->getId()))
                    {
                        $stockSupplier = $bestSellerObject->getSupplier()->getStock();
                    }
                    $imageURL = '../admin/uploads/' . $bestSellerObject->getMainImage()->getFileName();
                    ?>

                    <div class="col-sm-6 col-lg-3">
                        <div class="block">
                            <div class="img-container">

                                <img src="<?php echo $imageURL; ?>" width="150" height="150" alt="">
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <a class="btn btn-sm btn-default"
                                           href="shop_product.php?id=<?php echo $bestSellerObject->getId();?>">View</a>
                                    </div>
                                    <div class="text-warning">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span class="text-white">(690)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-content" style="height: 150px">
                            <div class="push-10">
                                <div class="h4 font-w600 text-success pull-right push-10-l"><?php echo $bestSellerObject->getPrice(); ?>
                                    Lei
                                </div>

                                <span class="h4">
                        <span class="font-w600 text-success"><b><?php echo $bestSellerObject->getStock(); ?></b> IN STOCK</span>
                        </span>
                                <br>
                                <span class="h6">
                        <span class="font-w600 text-success" style="color: red"><b><?php echo $stockSupplier; ?></b> Pending from supplier</span>
                        </span>
                                <br>
                                <a class="h4"
                                   href="shop_product.php?id=<?php echo $bestSellerObject->getId();?>"><?php echo $bestSellerObject->getName(); ?></a>
                                <p class="text-muted"><?php echo $bestSellerObject->getCategory()->getName(); ?></p>
                            </div>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>


            <div class="col-xs-12 push">
                <ul class="pager">
                    <li class="next">
                        <a href="shop_products.php">All Products <i class="fa fa-arrow-right"></i></a>
                    </li>
                </ul>
            </div>

        </section>
        <!-- END Products -->

        <!-- Explore Store -->
        <div class="bg-gray-light border-t border-b">
            <section class="content content-full content-boxed">
                <!-- Section Content -->
                <div class="push-20-t push-20 text-center">
                    <h3 class="h4 push-20 visibility-hidden" data-toggle="appear">Over <strong>50.000</strong> digital
                        products!</h3>
                    <a class="btn btn-rounded btn-noborder btn-lg btn-success visibility-hidden" data-toggle="appear"
                       data-class="animated bounceIn" href="base_pages_ecom_store_products.html">Explore Store <i
                                class="fa fa-arrow-right"></i></a>
                </div>
                <!-- END Section Content -->
            </section>
        </div>
        <!-- END Explore Store -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
        <div class="pull-right">
            Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://goo.gl/vNS3I"
                                                                     target="_blank">pixelcave</a>
        </div>
        <div class="pull-left">
            <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">OneUI 3.4</a> &copy; <span
                    class="js-year-copy">2015</span>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Apps Modal -->
<!-- Opens from the button in the header -->
<div class="modal fade" id="apps-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-sm modal-dialog modal-dialog-top">
        <div class="modal-content">
            <!-- Apps Block -->
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Apps</h3>
                </div>
                <div class="block-content">
                    <div class="row text-center">
                        <div class="col-xs-6">
                            <a class="block block-rounded" href="base_pages_dashboard.html">
                                <div class="block-content text-white bg-default">
                                    <i class="si si-speedometer fa-2x"></i>
                                    <div class="font-w600 push-15-t push-15">Backend</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <a class="block block-rounded" href="bd_dashboard.html">
                                <div class="block-content text-white bg-modern">
                                    <i class="si si-rocket fa-2x"></i>
                                    <div class="font-w600 push-15-t push-15">Boxed</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Apps Block -->
        </div>
    </div>
</div>
<!-- END Apps Modal -->


</body>
</html>