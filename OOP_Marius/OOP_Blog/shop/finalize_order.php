<?php
include ("../functions/functions.php");

$order = new Orders();
foreach (get_object_vars($order) as $atribute=>$value)
{
    if ($atribute!='id')
    {
        $setter = 'set' . ucfirst($atribute);
        $order->$setter($_POST[$atribute]);
    }
}
$order->save();


foreach ($_SESSION['cart'] as $id=>$qty)
{
    $product=getOneUniversal('product',['id'=>$id]);
    $orderItem = new OrderItems();
    $orderItem->setIdOrder($order->getId());
    $orderItem->setIdProduct($product->getId());
    $orderItem->setQuantity($qty);
    $orderItem->setProductName($product->getName());
    $orderItem->setPrice($product->getPrice());
    $orderItem->save();
    $product->setStock($product->getCurrentStock());
    $product->save();
    $stock = new Stock();
    $stock->setIdOrder($order->getId());
    $stock->setIdProduct($product->getId());
    $stock->setIdSupplier($product->getSupplier()->getId());
    $stock->setQuantity(-$qty);
    $stock->save();

}
unset($_SESSION['cart']);
header("Location: shop_home.php");

die;
?>


