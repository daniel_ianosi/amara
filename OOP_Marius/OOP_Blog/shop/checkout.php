<!DOCTYPE html>
<?php include "../functions/functions.php";
$totalprice = 0;
?>
<!--[if IE 9]>
<html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>OneUI - Admin Dashboard Template &amp; UI Framework</title>

    <meta name="description"
          content="OneUI - Admin Dashboard Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/img/favicons/favicon.png">

    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="assets/img/favicons/favicon-192x192.png" sizes="192x192">

    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" id="css-main" href="assets/css/oneui.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>
<body>
<!-- Page Container -->
<!--
    Available Classes:

    'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

    'sidebar-l'                  Left Sidebar and right Side Overlay
    'sidebar-r'                  Right Sidebar and left Side Overlay
    'sidebar-mini'               Mini hoverable Sidebar (> 991px)
    'sidebar-o'                  Visible Sidebar by default (> 991px)
    'sidebar-o-xs'               Visible Sidebar by default (< 992px)

    'side-overlay-hover'         Hoverable Side Overlay (> 991px)
    'side-overlay-o'             Visible Side Overlay by default (> 991px)

    'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

    'header-navbar-fixed'        Enables fixed header
-->
<div id="page-container" class="side-scroll ">

    <!-- Main Container -->
    <main id="main-container">
        <!-- Hero Content -->
        <div class="bg-image" style="background-image: url('assets/img/photos/photo3@2x.jpg');">
            <div class="bg-primary-dark-op">
                <section class="content content-full content-boxed overflow-hidden">
                    <!-- Section Content -->
                    <div class="push-30-t push-30 text-center">
                        <h1 class="h2 text-white push-10 visibility-hidden" data-toggle="appear"
                            data-class="animated fadeInDown"><i class="fa fa-shopping-cart text-white-op"></i> Checkout.
                        </h1>
                        <h2 class="h5 text-white-op visibility-hidden" data-toggle="appear"
                            data-class="animated fadeInDown">We are almost there!</h2>
                    </div>
                    <!-- END Section Content -->
                </section>
            </div>
        </div>
        <!-- END Hero Content -->
        <section class="content content-boxed">
            <div class="row">
                <form action="shop_products.php" method="get">
                    <input type="text" name="search" placeholder="search">
                    <button type="submit">SEARCH</button>
                </form>
            </div>
        </section>
        <!-- Checkout Content -->
        <section class="content content-boxed">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <!-- Card plugin (.js-card-form and .js-card-container are initialized at the bottom of the page) -->
                    <!-- For more info and examples you can check out https://github.com/jessepollak/card -->
                    <form class="js-card-form form-horizontal" action="base_pages_ecom_store_checkout.html"
                          method="post" onsubmit="return false;">
                        <!-- Products -->
                        <div class="block">
                            <div class="block-header">
                                <h3 class="block-title">1. Products Ordered: </h3>
                            </div>
                            <div class="block-content">
                                <form action="updatecart.php" method="get"></form>
                                <table class="table table-borderless table-vcenter">
                                    <tbody>
                                    <?php foreach ($_SESSION['cart'] as $product_id => $quantity) {
                                        $stock = 0;
                                        $stockSupplier = 0;
                                        $product = getOneUniversal('product', ['id' => $product_id]);
                                        if (!is_null($product->getStock())) {
                                            $stock = $product->getStock();
                                        }

                                        if (!is_null($product->getSupplier()->getStock())) {
                                            $stockSupplier = $product->getSupplier()->getStock();

                                        } ?>

                                        <tr>
                                            <td style="max-width: 55px;" class="text-center">
                                                <a class="text-danger"
                                                   href="removefromcart.php?id=<?php echo $product_id; ?>"><i
                                                            class="fa fa-times"></i></a>
                                            </td>
                                            <td style="width: 55px;">

                                                <?php $imageURL = '../admin/uploads/' . $product->getMainImage()->getFileName(); ?>
                                                <img src="<?php echo $imageURL; ?>" width="80" height="80"/>
                                            </td>
                                            <td>
                                                <p style="line-height: 5px"><a class="h5"
                                                                               href="shop_product.php?id=<?php echo $product_id; ?>"><?php echo $product->getName(); ?> </a>
                                                </p>
                                                <p style="line-height: 5px ; color: green"><?php echo 'In stock: ' . $stock . ' pcs'; ?></p>
                                                <?php if ($stockSupplier == 0) { ?>
                                                    <p style="line-height: 5px ;color: red">No pending from supplier</p>
                                                <?php } else {
                                                    ?>
                                                    <p style="line-height: 5px ;color: red"><?php echo $stockSupplier . ' pcs pending ' . $product->getSupplier()->getDelivery() . ' days'; ?></p>
                                                <?php } ?>
                                            </td>

                                            <td class="text-right">
                                                <form action="updatecart.php" method="get">


                                                    <?php if ($stock >= $quantity) { ?>
                                                        <input type="number" name="qty" min="1" max="1000"
                                                               value="<?php echo $quantity; ?>"/>
                                                        <input type="hidden" name="id"
                                                               value="<?php echo $product_id; ?>"/>
                                                        <input type="submit" value="Update Cart"/>

                                                    <?php } elseif (($stock < $quantity) && (($stock + $stockSupplier) >= $quantity)) {
                                                        $quantityPending = $quantity - $stock;
                                                        $totalQuantity = $stock + $quantityPending;
                                                        ?>

                                                        <input type="number" name="qty" min="1" max="1000"
                                                               value="<?php echo $totalQuantity; ?>"/>
                                                        <input type="hidden" name="id"
                                                               value="<?php echo $product_id; ?>"/>
                                                        <br><label for="in-stock" style="color: green">In
                                                            stock: <?php echo $stock; ?></label>
                                                        <br><label for="supplier-stock" style="color: red">Pending from
                                                            supplier: <?php echo $quantityPending . ' pcs in ' . $product->getSupplier()->getDelivery() . ' days'; ?></label>

                                                        <br>
                                                        <input type="submit" value="Update Cart"/>
                                                    <?php } else {
                                                        $totalQuantity = $stock + $stockSupplier; ?>
                                                        <label for="supplier-stock" style="color: red">Stock
                                                            limit: <?php echo $totalQuantity; ?></label>
                                                        <input type="hidden" name="id"
                                                               value="<?php echo $product_id; ?>"/>
                                                        <input type="number" name="qty" min="1" max="1000"
                                                               value="<?php echo $totalQuantity; ?>"/>

                                                        <br>
                                                        <input type="submit" value="Update Cart"/>
                                                    <?php } ?>
                                                </form>
                                            </td>

                                            <td class="text-right">
                                                <div class="font-w600 text-succes"><?php echo $quantity * $product->getPrice() ?>
                                                    lei
                                                </div>
                                                <?php $totalprice += ($quantity * $product->getPrice()); ?>
                                            </td>

                                        </tr>
                                    <?php } ?>
                                    <tr class="success">
                                        <td class="text-right" colspan="3">
                                            <span class="h4 font-w600">Total</span>
                                        </td>
                                        <td class="text-right">
                                            <div class="h4 font-w600 text-success"><?php echo $totalprice . " lei"; ?></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Products -->

            <!-- Create Account -->
            <?php if ($totalprice > 0){ ?>
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">2. SUBMIT ORDER </h3>
                </div>
                <form action="finalize_order.php" method="post">
                    <div class="block-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="Firstname" name="firstName" type="text">
                                            <label for="Firstname">Firstname</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="Email" name="email" type="email">
                                            <label for="Email">Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="Address1" name="address1" type="text">
                                            <label for="Address1">Address1</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="Address2" name="address2" type="text">
                                            <label for="Address2">Address2</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="Lastname" name="lastName" type="text">
                                            <label for="Lastname">Lastname</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="City" name="city" type="text">
                                            <label for="City">City</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="Telephone" name="phone" type="number"
                                                   maxlength="12">
                                            <label for="Telephone">Phone no:</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material floating">
                                            <input class="form-control" id="ZipCode" name="zipp" type="number"
                                                   maxlength="6">
                                            <label for="ZipCode">Zip Code</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check push-5-r"></i> Complete
                                Order
                            </button>
                </form>
            </div>
</div>
</div>

<?php }
else {
    ?>
    <a href="shop_products.php?table=product">Back to shop</a>

<?php } ?>
<!-- END Create Account -->

<!-- Credit Card -->

<!-- END Credit Card -->

</div>
</section>
<!-- END Checkout Content -->
</main>
<!-- END Main Container -->

<!-- Footer -->
<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="pull-right">
        Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://goo.gl/vNS3I"
                                                                 target="_blank">pixelcave</a>
    </div>
    <div class="pull-left">
        <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">OneUI 3.4</a> &copy; <span
                class="js-year-copy">2015</span>
    </div>
</footer>
<!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Apps Modal -->
<!-- Opens from the button in the header -->
<div class="modal fade" id="apps-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-sm modal-dialog modal-dialog-top">
        <div class="modal-content">
            <!-- Apps Block -->
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Apps</h3>
                </div>
                <div class="block-content">
                    <div class="row text-center">
                        <div class="col-xs-6">
                            <a class="block block-rounded" href="base_pages_dashboard.html">
                                <div class="block-content text-white bg-default">
                                    <i class="si si-speedometer fa-2x"></i>
                                    <div class="font-w600 push-15-t push-15">Backend</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <a class="block block-rounded" href="bd_dashboard.html">
                                <div class="block-content text-white bg-modern">
                                    <i class="si si-rocket fa-2x"></i>
                                    <div class="font-w600 push-15-t push-15">Boxed</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Apps Block -->
        </div>
    </div>
</div>
<!-- END Apps Modal -->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/core/jquery.slimscroll.min.js"></script>
<script src="assets/js/core/jquery.scrollLock.min.js"></script>
<script src="assets/js/core/jquery.appear.min.js"></script>
<script src="assets/js/core/jquery.countTo.min.js"></script>
<script src="assets/js/core/jquery.placeholder.min.js"></script>
<script src="assets/js/core/js.cookie.min.js"></script>
<script src="assets/js/app.js"></script>

<!-- Page JS Plugins -->
<script src="assets/js/plugins/card/jquery.card.min.js"></script>

<!-- Page JS Code -->
<script>
    jQuery(function () {
        // Init page helpers (Appear plugin)
        App.initHelpers('appear');

        // Init Card, for more info and examples you can check out https://github.com/jessepollak/card
        jQuery('.js-card-form').card({
            container: '.js-card-container',
            formSelectors: {
                numberInput: '#checkout-cc-number',
                expiryInput: '#checkout-cc-expiry',
                cvcInput: '#checkout-cc-cvc',
                nameInput: '#checkout-cc-name'
            }
        });
    });
</script>
</body>
</html>