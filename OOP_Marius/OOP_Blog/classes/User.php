<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 15-01-2019
 * Time: 10:08 AM
 */
class User extends BaseTable
{
 public $userName;
 public $type;
 public $password;

 protected function getTableName()
 {
     return 'user';
 }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}