<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 14-01-2019
 * Time: 8:25 PM
 */
include "BaseTable.php";
include "Authors.php";
include "BaseProduct.php";
include "Category.php";
include "Comments.php";
include "Blogpost.php";
include "User.php";
include "Product.php";
include "ProductCategory.php";
include "ProductManufacturers.php";
include "ProductImages.php";
include "Supplier.php";
include "Orders.php";
include "OrderItems.php";
include "Stock.php";