<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 15-01-2019
 * Time: 11:23 AM
 */
class ProductCategory extends BaseTable
{
public $name;

protected function getTableName()
{
    return 'productCategory';
}

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}