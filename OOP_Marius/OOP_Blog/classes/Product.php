<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 15-01-2019
 * Time: 11:17 AM
 */
class Product extends BaseTable
{
 public $name;
 public $price;
 public $stock;
 public $idCategory;
 public $description;
 public $idManufacturer;
 public $bestSellers;
 public $newArrivals;

 protected function getTableName()
 {
     return 'product';
 }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * @param mixed $idCategory
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getIdManufacturer()
    {
        return $this->idManufacturer;
    }

    /**
     * @param mixed $idManufacturer
     */
    public function setIdManufacturer($idManufacturer)
    {
        $this->idManufacturer = $idManufacturer;
    }

    /**
     * @return mixed
     */
    public function getBestSellers()
    {
        return $this->bestSellers;
    }

    /**
     * @param mixed $bestSellers
     */
    public function setBestSellers($bestSellers)
    {
        $this->bestSellers = $bestSellers;
    }

    /**
     * @return mixed
     */
    public function getNewArrivals()
    {
        return $this->newArrivals;
    }

    /**
     * @param mixed $newArrivals
     */
    public function setNewArrivals($newArrivals)
    {
        $this->newArrivals = $newArrivals;
    }

    public function getCategory()
    {
        return new ProductCategory($this->getIdCategory());
    }

    public function getManufacturers()
    {
        return new ProductManufacturers($this->getIdManufacturer());
    }
    public function getMainImage()
    {
        return getOneUniversal('productImages',['idProdus'=>$this->getId(),'mainImage'=>1]);
    }

    public function getImages()
    {
        return getUniversal('productImages',['idProdus'=>$this->getId()]);
    }

    public function getSupplier()
    {
        return getOneUniversal('supplier',['idProduct'=>$this->getId()]);
    }

    public function getCurrentStock()
    {
        $currentStock=0;
       foreach (getUniversal('stock',['idProduct'=>$this->getId()]) as $object)
       {
           $currentStock+=$object->getQuantity();
       }
       return $currentStock;
    }

}