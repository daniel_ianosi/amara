<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 14-01-2019
 * Time: 7:51 PM
 */
class Authors extends BaseTable
{
    public $name;

    /**
     * @return string
     */
    protected function getTableName()
    {
        return 'authors';
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}