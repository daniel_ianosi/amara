<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 16-01-2019
 * Time: 10:44 AM
 */
class ProductManufacturers extends BaseTable
{
    public $name;

    protected function getTableName()
    {
        return 'productManufacturers';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}