<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 14-01-2019
 * Time: 8:57 PM
 */
class Category extends BaseTable
{
public $name;

    protected function getTableName()
    {
        return 'category';
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}