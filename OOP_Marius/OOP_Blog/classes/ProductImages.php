<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 16-01-2019
 * Time: 12:54 PM
 */
class ProductImages extends BaseTable
{
    public $idProdus;
    public $fileName;
    public $mainImage;
protected function getTableName()
{
    return 'productImages';
}

    /**
     * @return mixed
     */
    public function getIdProdus()
    {
        return $this->idProdus;
    }

    /**
     * @param mixed $idProdus
     */
    public function setIdProdus($idProdus)
    {
        $this->idProdus = $idProdus;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param mixed $mainImage
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;
    }

}