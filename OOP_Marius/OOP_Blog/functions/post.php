<?php include "functions.php"; ?>
<?php global $idPost; ?>
<?php include "../componets/header.php";?>
<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>
<?php $post= getOneUniversal('blogpost',['id'=>$_GET['id']]); ?>
<div class="blog_post">
    <h1><a href="#intro" id="intro"><?php echo $post->getTitle(); ?></a></h1>
    <p>
        <?php echo $post->getContent(); ?>
    </p>
    <div class="article_menu">
        <?php $aut=getOneUniversal('authors',['id'=>$post->getIdAuthor()]) ?>
        <b>Posted by <a href="author.php?author=<?php echo $post->getIdAuthor(); ?>"><?php echo $aut->getName(); ?></a></b>
        <?php $cat=getOneUniversal('category',['id'=>$post->getIdCategory()]) ?>
        <a href="category.php?category=<?php echo $post->getIdCategory(); ?>"><?php echo $cat->getName(); ?></a>
        <br />
        <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $post->getDate(); ?>"><?php echo $post->getDate(); ?></a></b></p>
    </div>
</div>
    <form action="index.php">
        <td><button type="submit" >Back to Index</button></td>
    </form>
<h2> COMMENTS: </h2>
<?php foreach (getUniversal('comments',['idPost'=>$post->getId()],[],[],[]) as $comm): ?>
<div   class="comments">
    <?php if ($comm->getReview()=='Accepted') { ?>
       <hr>
    <p>
        <?php echo $comm->getComment();
        echo ' | By: '.$comm->getName().'</br>';?>
    </p>
    <?php } ?>
</div>


<?php endforeach;?>
<?php if (isset($_SESSION['user_name'])): ?>
<form action="addComment.php" method="post">
<table>
    <tr>
        <th>Comment :</th>
        <td><input type="text" name="Comment"/></td>
    </tr>
    <tr>
        <td><input type="hidden" name="CommentName" value="<?php echo $_SESSION['user_name']; ?>" /></td>
    </tr>
    <tr>
        <td><button type="submit" >SUBMIT</button></td>
    </tr>
    <td><input type="hidden" name="id" value="<?php echo $post->getId(); ?>"/> </td>
    <?php if ($_SESSION['type']==1){ ?>
        <td><input type="hidden" name="review" value="Accepted"/></td>
    <?php } else { ?>
    <td><input type="hidden" name="review" value=" "/></td>
    <?php }; ?>
</table>
</form>
<?php endif;?>
<?php include "../componets/footer.php" ;?>
