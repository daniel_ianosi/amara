<?php include "functions.php";?>
<?php include "componets/header.php";?>

<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>
<?php $data=getOneUniversal('blogpost',['id'=>$_GET['date']]); ?>
<h1>Here you can see all posts from this date: <?php echo $data->getDate(); ?></h1>
<?php foreach (getUniversal('blogpost',['date'=>$_GET['date']],[],[],[]) as $object): ?>
<div class="blog_post">
    <h1><a href="post.php?id=<?php echo $object->getId(); ?>" id="intro"><?php echo $object->getTitle(); ?></a></h1>
    <p>
        <?php echo extractFirstWords($object->getContent()); ?> ...
    </p>
    <div class="article_menu">
        <?php $aut=getOneUniversal('authors',['id'=>$object->getIdAuthor()]) ?>
        <b>Posted by <a href="author.php?author=<?php echo $object->getIdAuthor(); ?>"><?php echo $aut->getName(); ?></a></b>
        <?php $cat=getOneUniversal('category',['id'=>$object->getIdCategory()]) ?>
        <a href="category.php?category=<?php echo $object->getIdCategory(); ?>"><?php echo $cat->getName(); ?></a>
    </div>
    <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $object->getDate(); ?>"><?php echo $object->getDate(); ?></a></b></p>


</div>
<?php endforeach;?>

