<?php include "functions.php";?>
<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>

<?php
$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if($page=="" || $page=="1")
{
    $page1 = 0;
}
else
{
    $page1 = ($page*5)-5;
}
?>
<div class="col-lg-9 col-sm-12">

    <div class="row">
        <?php foreach (getUniversal('blogpost',[],[],[],[],$page1,5) as $post): ?>
        <div class="col-12">
            <h2><a href="post.php?id=<?php echo $post['id']; ?>" id="intro"><?php echo $post['title']; ?></a></h2>
            <hr />
            <p><?php echo extractFirstWords($post['content']); ?> ...</p>
            <hr />
            <div class="row">
                <div class="col-6" align="left">
                    <?php $aut=getOneUniversal('authors',['id'=>$post['id_author']]) ?>
                    <b>Posted by <a href="author.php?author=<?php echo $post['id_author']; ?>" author="intro"><?php echo $aut['name']; ?></a></b>
                </div>
                <div class="col-6" align="right">
                    <?php $cat=getOneUniversal('category',['id'=>$post['id_category']]) ?>
                    <a href="category.php?category=<?php echo $post['id_category']; ?>" category="intro"><?php echo $cat['name']; ?></a>
                    <br />
                    <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $post['date']; ?>"><?php echo $post['date']; ?></a></b></p>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
    <div class="col-12" aling="center">
        <?php
        $nrPagini=ceil(count(getUniversal('blogpost'))/5);
        echo "<br>";
        echo ("Pagina : ");
        for ($i=1;$i<=$nrPagini;$i++)
        {    ?>
            <a href="new_design.php?page=<?php echo $i; ?>" style="text-decoration:none"><?php  echo $i."  ";  ?></a>
        <?php } ?>
    </div>
</div>


