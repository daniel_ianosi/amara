<?php
include "functions.php";

checkUser();
$condition = boolval(0);
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>
<?php require 'inc/views/template_head_end.php'; ?>
<?php require 'inc/views/base_head.php'; ?>

<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 7px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 10px;
        margin: 3px 3px;
        cursor: pointer;
    }

    .button2 {
        background-color: #008CBA;
    }

    /* Blue */
    .button3 {
        background-color: #f44336;
    }

    /* Red */
    .button4 {
        background-color: #e1e1e1;
    }

    /* Blue */
</style>

<!-- Page Header -->
<div class="bg-image overflow-hidden"
     style="background-image: url('<?php echo $one->assets_folder; ?>/img/photos/photo31@2x.jpg');">
    <div class="bg-black-op">
        <div class="content content-narrow">
            <div class="block block-transparent">
                <div class="block-content block-content-full">
                    <h1 class="h1 font-w300 text-white animated fadeInDown push-50-t push-5">Dashboard A.M.A.R.A </h1>
                    <h2 class="h4 font-w300 text-white-op animated fadeInUp">Welcome Administrator</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content content-narrow">
    <!-- Stats -->
    <div class="row text-uppercase">
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Vizitatori unici</div>
                    <div><h3>402929 </h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Autori activi</div>
                    <div><h3>4562</h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Articole blog</div>
                    <div><h3>72929 </h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Total comentarii</div>
                    <div><h3>112929 </h3></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats -->

    <!-- Page Content -->
    <div class="content">


        <!-- Main Dashboard Chart -->
        <div class="block">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <button type="button" data-toggle="block-option" data-action="refresh_toggle"
                                data-action-mode="demo"><i class="si si-refresh"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Administrare tabele</h3>
            </div>
            <div class="block-content block-content-full bg-gray-lighter text-center">


                <!-- START CODE -->

                <?php
                $class = ucfirst($_SESSION['table']);
                if (isset($_GET['id'])) {
                    $object = new $class($_GET['id']);
                } else {
                    $object = new $class();
                } ?>
                <form action="updateCommand.php" method="post">
                    <table border="1">
                        <div>
                            <tr>
                                <th>id</th>
                                <td>
                                    <?php
                                    if (isset($_GET['id'])) {
                                        echo $object->getId();
                                    } else {
                                        echo 'Id va fi generat automat';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php if ($_SESSION['table'] != 'stock') { ?>
                                <?php foreach (get_object_vars($object) as $column => $value): ?>
                                    <tr>
                                        <?php if ($column != 'id') : ?>
                                            <th><?php echo $column; ?> </th>
                                            <td>
                                                <textarea name="<?php echo $column; ?>"><?php echo $value; ?></textarea>
                                            </td>
                                        <?php endif; ?>

                                    </tr>
                                <?php endforeach; ?>

                                <tr>
                                    <td>
                                        <button type="submit">Update registration</button>
                                    </td>
                                </tr>
                                <?php
                            } else { ?>
                                <tr>
                                    <th>idProduct</th>
                                    <td><input type="number" name="idProduct"></td>
                                </tr>
                                <tr>
                                    <th>idSupplier</th>
                                    <td><input type="number" name="idSupplier"></td>
                                </tr>
                                <tr>
                                    <th>quantity</th>
                                    <td><input type="number" name="quantity"></td>
                                </tr>

                                <tr>
                                    <td>
                                        <button type="submit">Add in Stock</button>
                                    </td>
                                </tr>
                            <?php }
                            ?>
                        </div>
                    </table>
                    <input type="hidden" name="id" value="<?php echo $object->getId(); ?>">
                </form>
                <?php die; ?>
                <!-- END CODE -->


                <?php require 'inc/views/base_footer.php'; ?>
                <?php require 'inc/views/template_footer_start.php'; ?>

                <!-- Page Plugins -->
                <script src="<?php echo $one->assets_folder; ?>/js/plugins/chartjs/Chart.min.js"></script>

                <!-- Page JS Code -->
                <script src="<?php echo $one->assets_folder; ?>/js/pages/base_pages_dashboard_v2.js"></script>
                <script>
                    jQuery(function () {
                        // Init page helpers (CountTo plugin)
                        App.initHelpers('appear-countTo');
                    });
                </script>

                <?php require 'inc/views/template_footer_end.php'; ?>
