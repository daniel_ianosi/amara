<?php
include "functions.php";

$class = ucfirst($_SESSION['table']);
if (isset($_POST['id']))
{
    $updateObject = new $class($_POST['id']);
}
else
{
    $updateObject = new $class();
}
foreach (get_object_vars($updateObject) as $atribut=>$value)
{
    $setter = 'set'.ucfirst($atribut);
    $updateObject->$setter($_POST[$atribut]);
}

$updateObject->save();
header("Location: dashboard.php?table=".$_SESSION['table']);
die;
?>
