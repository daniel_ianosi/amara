<?php
include "functions.php";
checkUser();
$condition = boolval(0);
if (isset($_GET['table'])){
    $_SESSION['table']=$_GET['table'];
    $table = $_SESSION['table'];
    if (!isset($_SESSION['sort'][$table])) {
        $_SESSION['sort'][$table] = [];}}
$page=null;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}
if ($page == "" || $page == "1") {
    $page1 = 0;
} else {
    $page1 = ($page * 5) - 5;
}
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>
<?php require 'inc/views/template_head_end.php'; ?>
<?php require 'inc/views/base_head.php'; ?>

<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 7px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 10px;
        margin: 3px 3px;
        cursor: pointer;
    }

    .button2 {
        background-color: #008CBA;
    }

    /* Blue */
    .button3 {
        background-color: #f44336;
    }

    /* Red */
    .button4 {
        background-color: #e1e1e1;
    }

    /* Blue */
</style>

<!-- Page Header -->
<div class="bg-image overflow-hidden"
     style="background-image: url('<?php echo $one->assets_folder; ?>/img/photos/photo31@2x.jpg');">
    <div class="bg-black-op">
        <div class="content content-narrow">
            <div class="block block-transparent">
                <div class="block-content block-content-full">
                    <h1 class="h1 font-w300 text-white animated fadeInDown push-50-t push-5">Dashboard A.M.A.R.A </h1>
                    <h2 class="h4 font-w300 text-white-op animated fadeInUp">Welcome Administrator</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content content-narrow">
    <!-- Stats -->
    <div class="row text-uppercase">
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Vizitatori unici</div>
                    <div><h3>402929 </h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Autori activi</div>
                    <div><h3>4562</h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Articole blog</div>
                    <div><h3>72929 </h3></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <div class="text-muted">
                        <small><i class="si si-calendar"></i> Today</small>
                    </div>
                    <div class="font-s12 font-w700">Total comentarii</div>
                    <div><h3>112929 </h3></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats -->

    <!-- Page Content -->
    <div class="content">


        <!-- Main Dashboard Chart -->
        <div class="block">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <button type="button" data-toggle="block-option" data-action="refresh_toggle"
                                data-action-mode="demo"><i class="si si-refresh"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Administrare tabele</h3>
            </div>
            <div class="block-content block-content-full bg-gray-lighter text-center">

                <!-- START My code -->
                <table class="main-table">
                    <tr>
                        <td class="content">
                            <?php if (!empty($_SESSION['table'])) { ?>
                                <div>
                                    <img src="assets/img/various/new_post.png" width="30" height="30">
                                    <a href="insert_registration.php?table=<?php echo $_SESSION['table']; ?>">
                                        <button>+ Adauga post nou</button>
                                    </a>
                                    </br>
                                    </br>
                                </div>
                            <?php } else {
                            } ?>
                            <?php if (isset($_SESSION['table'])): ?>
                            <form action="filter.php" method="post">
                                <table border="1">
                                    <div>
                                        <tr>
                                            <th>id</th>
                                            <td><input type="text" name="id"></td>
                                        </tr>
                                        <?php foreach (get_class_vars(ucfirst($_SESSION['table'])) as $column=>$value): ?>
                                        <tr>
                                            <?php if($column!='id') :?>
                                                <th><?php echo $column ?> </th>
                                                <td><input type="text" name="<?php echo $column; ?>"></td>
                                                <?php if ($column == 'review') {
                                                    $condition = 1;
                                                } else {
                                                } ?>
                                            <?php endif; endforeach; ?>
                                        </tr>
                                    </div>
                                    <tr>
                                        <td>
                                            <button type="submit">SEARCH</button>
                                        </td>
                                    </tr>
                                </table>
                        <td><input type="hidden" name="table" value="<?php echo $_SESSION['table']; ?>"/></td>
                        </form>
                        <?php endif; ?>
                        <table border="1" class="table-content">
                            </br>
                            </br>
                            <?php if (!empty($_SESSION['table'])){ ?>
                            <tr>

                                <?php if ($_SESSION['table']=='product'){ ?>
                                    <th valign="top">IMAGE</th>
                                <?php } ?>

                                <th valign="top">
                                    <p align="center">id</p>
                                    <p align="center">
                                        <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=id&sorttype=ASC" ?>"
                                        >&#9650</a>
                                        <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=id&sorttype=DESC" ?>"
                                        >&#9660</a>
                                        <?php if (isset($_SESSION['sort']['table']['id'])): ?>
                                            <a href="sort.php?table=<?php echo $_SESSION['table'];?> &clearFilter=id"
                                               style="color: red">X</a>
                                        <?php endif; ?>
                                    </p>
                                </th>
                                <?php foreach (get_class_vars(ucfirst($_SESSION['table'])) as $column=>$value): ?>
                                    <?php if($column!='id') :?>

                                        <th valign="top">
                                            <p align="center"><?php echo $column ?></p>
                                            <p align="center">
                                                <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=" . $column . "&sorttype=ASC" ?>"
                                                >&#9650</a>
                                                <a href="sort.php?table=<?php echo $_SESSION['table'] . "&column=" . $column . "&sorttype=DESC" ?>"
                                                >&#9660</a>
                                                <?php if (isset($_SESSION['sort']['table'][$column])): ?>
                                                    <a href="sort.php?table=<?php echo $_SESSION['table'] . "&clearFilter=" . $column ?>"
                                                       style="color: red">X</a>
                                                <?php endif; ?>
                                            </p>

                                        </th>

                                        <?php if ($column == 'review') {
                                            $condition = 1;
                                        } else {
                                        } ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <th valign="top">Actions</th>
                            </tr>
                            <tr>
                                <?php foreach (getUniversal($_SESSION['table'], [], [], [],$_SESSION['sort']['table'],$page1,5) as $object): ?>
                                <?php if ($_SESSION['table']=='product'){ ?>
                                    <td>
                                        <?php

                                        // Get images from the database
                                        $query = $mysql->query("SELECT DISTINCT file_name FROM product_images WHERE id_produs='".$object->getId()."' AND main_image='1';");
                                        if($query->num_rows > 0){
                                            while($row = $query->fetch_assoc()){
                                                $imageURL = 'uploads/'.$row["file_name"]; ?>
                                                <img src="<?php echo $imageURL; ?>" width="80" height="80" />
                                            <?php  } } else { ?>
                                            <p>No image found...</p>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                                <td>
                                    <?php echo $object->getId();?>
                                </td>

                                <?php foreach (get_object_vars($object) as $column=>$value): ?>
                                    <?php if ($column!='id'):?>

                                        <td> <?php echo extractFirstWords($value); ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td>

                                    <?php foreach (getButtons($_SESSION['table'], $object->getId()) as $action => $link): ?>
                                        <a href="<?php echo $link ?>"><?php echo $action; ?></a>

                                    <?php endforeach; ?>

                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </table>
                        </td>
                    </tr>
                    <?php } else {
                    } ?>
                    <?php

                    if (isset($_SESSION['table'])):
                        $nrPagini = ceil(count(getUniversal($_SESSION['table'])) / 5);
                        echo "<br>";
                        echo "<br>";
                        echo("Pagina : ");
                        for ($i = 1; $i <= $nrPagini; $i++) {
                            ?><a href="dashboard.php?table=<?php echo $_SESSION['table'] ?>&page=<?php echo $i; ?>"
                                 style="text-decoration:none"><?php echo $i . "  "; ?></a> <?php
                        }

                    endif;
                    ?>
                    <!-- END My code-->

            </div>
        </div>
        <!-- END Main Dashboard Chart -->


        <!-- END Latest Sales Widget -->

    </div>
    <!-- END Page Content -->
    <!-- END Page Content -->

    <?php require 'inc/views/base_footer.php'; ?>
    <?php require 'inc/views/template_footer_start.php'; ?>

    <!-- Page Plugins -->
    <script src="<?php echo $one->assets_folder; ?>/js/plugins/chartjs/Chart.min.js"></script>

    <!-- Page JS Code -->
    <script src="<?php echo $one->assets_folder; ?>/js/pages/base_pages_dashboard_v2.js"></script>
    <script>
        jQuery(function () {
            // Init page helpers (CountTo plugin)
            App.initHelpers('appear-countTo');
        });
    </script>

    <?php require 'inc/views/template_footer_end.php'; ?>
