<?php global $message; ?>
<!DOCTYPE html PUBLIC "www.scoalainformala.ro">
<html xmlns="www.scoalainformala.ro" xml:lang="en">
<head>
    <title>Mini Blog</title>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../css/1.css" type="text/css" media="screen,projection" />
</head>
<body>
<div id="container">
    <div id="header">
        <h1><a href="#">MEGA<strong>BLOG</strong> AMARA </a></h1>
        <h2>For all new programmers</h2>
        <ul id="nav">
            <li><a href="#" accesskey="h"><em>H</em>ome</a></li>
            <li><a href="#" title="CSS and XHTML (a:c)" accesskey="c"><em>c</em>ss &amp; xhtml</a></li>
            <li><a href="#" title="All about me (a:a)" accesskey="a"><em>A</em>bout the author</a></li>
            <li><a href="#" title="Contact" >C<em>o</em>ntact</a></li>
        </ul>
    </div>
    <div id="sidebar">
        <h1>MORE ABOUT:</h1>
        <ul class="submenu">
            <?php foreach (getUniversal('blogpost',[],['idCategory'],[]) as $object):?>
                <?php $cat=getOneUniversal('category',['id'=>$object->getIdCategory()]) ?>
            <li> <a href="category.php?category=<?php echo $object->getIdCategory(); ?>"> <?php echo $cat->getName(); ?></a></li>
            <?php endforeach;?>
        </ul>

        <?php echo build_calendar(date('m', strtotime($_SESSION['currentDate'])),date('Y', strtotime($_SESSION['currentDate'])), 1); ?>

        <h1>MORE FROM:</h1>
        <ul class="submenu">
            <?php foreach (getUniversal('blogpost',[],['idAuthor'],[]) as $object): ?>
                <?php $aut=getOneUniversal('authors',['id'=>$object->getIdAuthor()]) ?>
                <li> <a href="author.php?author=<?php echo $object->getIdAuthor(); ?>"> <?php echo $aut->getName(); ?></a></li>
            <?php endforeach;?>
        </ul>
        <?php if (!isset($_SESSION['user_name'])): ?>
        <h1>Login</h1>
        <form action="login_check_blog.php" method="post">
            <?php if (isset($_GET['message'])){?>
                <h1 style="color: red"><?php echo $_GET['message']; ?></h1>
            <?php }; ?>
            <table>
                <tr>
                    <th>User:</th>
                    <td><input type="text" name="user_name" size="15" /></td>
                </tr>
                <tr>
                    <th>Password:</th>
                    <td><input type="password" name="password" size="15" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><button type="submit" >Login</button></td>
                </tr>
            </table>
        </form>
        <?php endif; if (isset($_SESSION['user_name'])): ?>
        <h1><?php echo 'WELCOME '.strtoupper($_SESSION['user_name']); ?></h1>
        <ul class="submenu">
            <li><a href="logout_blog.php">LOG OUT</a> </li>
        </ul>
        <ul class="submenu">
            <?php if ($_SESSION['type']=='1'){ ?> <li><a href="../admin/dashboard.php?message=Welcome" ><?php echo 'ADMIN'; ?> </a></li> <?php }; ?>
            <?php endif; ?>
        </ul>
    </div>
    <div id="content">