<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 19-12-2018
 * Time: 8:31 PM
 */
class Category extends BaseTable
{
 public $name;

    /**
     * @return string
     */
    protected function getTableName()
    {
        return 'category';
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}