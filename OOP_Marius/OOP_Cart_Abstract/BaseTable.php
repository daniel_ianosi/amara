<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 19-12-2018
 * Time: 8:17 PM
 */
abstract class BaseTable
{
 public $id;

    /**
     * BaseTable constructor.
     * @param $id
     */
    public function __construct($id = null)
    {
        if (isset($id)) {
            $data = $this->getFromDb($this->getTableName(),['id'=>$id]);
            foreach (get_object_vars($this) as $columnName=>$value) {
                $setter = 'set' . ucfirst($columnName);
                $this->$setter($data[$columnName]);
            }
        }
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    public function getFromDb($table, $filters = [])
    {
        global $mysql;
        $list = [];
        $filter = '1=1';
        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }

        $result = $mysql->query("SELECT * FROM $table WHERE $filter LIMIT 1;");
        return $result->fetch_assoc();
    }

    public function delete(){
        global $mysql;
        $result = $mysql->query("DELETE FROM ".$this->getTableName()." where id=".$this->getId());
    }

    private function update(){
        var_dump($this->getId());
        global $mysql;
        $querry=null;
        $querry = " UPDATE ".$this->getTableName(). " SET ";

        foreach (get_object_vars($this) as $columnName=>$value){
            if (is_array($value)) {
                $value = implode(',', $value);
                $querry .= $columnName . "='" . $value ."' , ";
            }
            elseif ($columnName != 'id') {
                $querry .= $columnName . "= '" . $value . "' , ";
            }
        }
        $querry=substr($querry,0,-2);
        $querry.= " WHERE id=".$this->getId();

        $result = $mysql->query($querry);
    }

    private function insertIntoDb()
    {
        global $mysql;
        $columns = '(';
        $values = '(';
        foreach (get_object_vars($this) as $columnName => $value) {
            if ($columnName != 'id') {
                if (is_array($value)) {
                    $columns .= "`$columnName`,";
                    if (empty($value)){
                        $values.="'',";
                    }
                    else {
                        $values.="'";
                        $values .= implode(',', $value);
                        $values.="',";
                    }

                } else {
                    $columns .= "`$columnName`,";
                    $values .= "'$value',";
                }
            }
        }
        $columns=substr($columns,0,-2);
        $values=substr($values,0,-2);
        $columns.='`)';
        $values.='\')';
        //die("INSERT INTO " . $this->getTableName() . " $columns VALUES $values");
        $mysql->query("INSERT INTO " . $this->getTableName() . " $columns VALUES $values");
        $this->setId($mysql->insert_id);
    }

    public function save(){
        if ($this->getId()!=null){
            $this->update();
        }
        else{
            $this->insertIntoDb();
        }
    }


    /**
     * @return string
     */
    abstract protected function getTableName();

}