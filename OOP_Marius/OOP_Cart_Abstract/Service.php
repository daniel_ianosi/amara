<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/17/2018
 * Time: 7:28 PM
 */
class Service extends BaseProduct
{

    /**
     * @return boolean
     */
    public function isStockable()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'service';
    }

    public function setExtraAttrs($data)
    {
    }
}