<?php

class Person
{
    public $nume;
    public $prenume;
    public $portofel = null;

    /**
     * Person constructor.
     * @param $nume
     * @param $prenume
     * @param null $portofel
     */
    public function __construct($nume, $prenume, Portofel $portofel)
    {
        $this->nume = $nume;
        $this->prenume = $prenume;
        $this->portofel = $portofel;
    }
    public function Give($amount, Person $reciver){
        if ( $this->portofel->Out($amount)) {
            $reciver->Take($amount);
        }
    }

    public function Take($amount){
        $this->portofel->In($amount);
    }

    public function deposit($amount, $pinIntrodus, $cardIntrodus, Bancomat $bancomat)
    {
        if ($bancomat->verifyPin($cardIntrodus, $pinIntrodus)) {
            if ($this->portofel->bani >= $amount) {
                $this->portofel->Out($amount);
                $cardIntrodus->deposit($amount);
                $bancomat->setCash($bancomat->cash + $amount);
            }
        }
    }

    public function withdraw($amount, $pinIntrodus, Card $cardIntrodus, Bancomat $bancomat)
    {
        if ($bancomat->verifyPin($cardIntrodus, $pinIntrodus)) {
            if ($cardIntrodus->sold>=$amount)
            {
                if ($bancomat->getCash($amount))
                {
                    $this->portofel->In($amount);
                    $cardIntrodus->withdrawl($amount);
                    $bancomat->setCash($bancomat->cash - $amount);
                }
                else echo "Ne pare rau, fonduri insuficiente in bancomat <br/><br/>";
            }
            else echo "Ne pare rau, fonduri insuficiente pe card<br/><br/>";
        }
    }

    public function pay(Casier $casier, $type, $amount,CARD $card){
        if ($type=='cash'){
            $casier->tranzactieCash($amount);
            $this->portofel->Out($amount);
        }
        if ($type=='POS'){
            $casier->tranzactiePOS($amount,$card);
        }
    }
}