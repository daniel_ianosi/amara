<?php
include "Person.php";
include "Portofel.php";
include "Casier.php";
include "casaMarcat.php";
include "POS.php";
include "Bancomat.php";
include "Card.php";
include "Product.php";
include "CosCumparaturi.php";
include "Stock.php";

$bancomat = new Bancomat(6000);

$card1Marius = new Card('ING', 5000, 1365);
$card2Marius = new Card('BCR', 2000, 1245);
$card1Tavi = new Card('ALFA', 600, 1445);
$card1Daniel = new Card('REIFEISEN', 450, 1234);

$portofelMarius = new Portofel(250, ['ING' => $card1Marius, 'BCR' => $card2Marius]);
$portofelTavi = new Portofel(50, ['ALFA' => $card1Tavi]);
$portofelDaniel = new Portofel(2500, ['REIFEISEN' => $card1Daniel]);


$Marius = new Person("Constantin", "Marius", $portofelMarius);
$Tavi = new Person("Dita", "Tavi", $portofelTavi);
$Daniel = new Person("Ianosi", "Daniel", $portofelDaniel);

//Introduce cardul ING
$pinIntrodusMarius = 1365;
$cardIntrodus = 'ING';


echo "<h1>Tranzactia 1:  </h1>";
echo "<h3>Datele dumneavoastra sunt:  </h3>";

var_dump($Marius);

echo "<br/>";

$sumaRetrasa = 700;
$Marius->withdraw($sumaRetrasa, $pinIntrodusMarius, $card1Marius, $bancomat);

echo "<h3>Ati retras $sumaRetrasa de pe cardul card1Marius iar datele dumneavoastra sunt </h3>";

var_dump($Marius);
echo "<br/>";

echo "<h3>Datele bancomatului sunt: </h3>";
var_dump($bancomat);
echo "<br/>";


$sumaDepusa = 150;
echo "<h3>Ati depus $sumaDepusa pe cardul card1Marius iar datele dumneavoastra sunt </h3>";
$Marius->deposit($sumaDepusa, $pinIntrodusMarius, $card1Marius, $bancomat);

var_dump($Marius);
echo "<br/>";

echo "<h3>Datele bancomatului sunt: </h3>";
var_dump($bancomat);
echo "<br/>";
echo "<br/>";


//Introduce cardul BCR
$pinIntrodusMarius = 1245;
$cardIntrodus = 'BCR';

echo "<h1>Tranzactia 2:  </h1>";
echo "<h3>Datele dumneavoastra sunt:  </h3>";

var_dump($Marius);

echo "<br/>";

$sumaRetrasa = 2000;
$Marius->withdraw($sumaRetrasa, $pinIntrodusMarius, $card2Marius, $bancomat);

echo "<h3>Ati retras $sumaRetrasa de pe cardul card2Marius iar datele dumneavoastra sunt </h3>";

var_dump($Marius);
echo "<br/>";

echo "<h3>Datele bancomatului sunt: </h3>";
var_dump($bancomat);
echo "<br/>";


$sumaDepusa = 300;
echo "<h3>Ati depus $sumaDepusa pe cardul card2Marius iar datele dumneavoastra sunt </h3>";
$Marius->deposit($sumaDepusa, $pinIntrodusMarius, $card2Marius, $bancomat);

var_dump($Marius);
echo "<br/>";

echo "<h3>Datele bancomatului sunt: </h3>";
var_dump($bancomat);
echo "<br/>";
echo "<br/>";

$casaDeMarcat = new casaMarcat(1, 2500);
$POS = new POS(2300);

//Plata cash
$casier = new Casier('Popescu', 'Maria', $casaDeMarcat, $POS);
$amount = 50;
$type = 'cash';

$Marius->pay($casier, $type, $amount, $card1Marius);

echo "<h1>Casierie</h1>";
echo "<br/>";
echo "<br/>";
echo "<h3>Casierie</h3>";
var_dump($casier);
echo "<br/>";
echo "<br/>";
echo "<h3>Marius</h3>";
var_dump($Marius);
echo "<br/>";
echo "<br/>";

//plata prin POS
$amount = 700;
$type = 'POS';
$Marius->pay($casier, $type, $amount, $card1Marius);
echo "<h1>Casierie</h1>";
echo "<br/>";
echo "<br/>";
echo "<h3>Casierie</h3>";
var_dump($casier);
echo "<br/>";
echo "<br/>";
echo "<h3>Marius</h3>";
var_dump($Marius);
echo "<br/>";
echo "<br/>";

//Crearea produselor

$produs1 = new Product(23, 'Televizor', 'Samsung HD 101cm', 1200);
$produs2 = new Product(17, 'Monitor', 'Philips diagonala 21 inch', 400);
$produs3 = new Product(3, 'Pantaloni', 'Pantaloni ski dama', 200);
$produs4 = new Product(21, 'Ceas', 'Ceas barbatesc Fossil', 100);
$produs5 = new Product(14, 'Cuptor', 'LG', 300);


//Crearea cosului de cumparaturi
$cosCumparaturi1 = new CosCumparaturi([
    $produs1->id => ['quantity' => 2, 'produs' => $produs1],
    $produs2->id => ['quantity' => 3, 'produs' => $produs2],
    $produs3->id => ['quantity' => 2, 'produs' => $produs3],
]);
//Crearea stocului

$stock = new Stock([
    $produs1->id => ['produs' => $produs1, 'stock' => [
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 1],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 3],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -2],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 6],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 12],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -3]
                                                      ]
                    ],
    $produs2->id => ['produs' => $produs2, 'stock' => [
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 7],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 3],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -6],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 2],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 11],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -3]
                                                      ]
                    ],
    $produs3->id => ['produs' => $produs3, 'stock' => [
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 3],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 3],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -5],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 11],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -1],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -3]
                                                    ]
                    ],
    $produs4->id => ['produs' => $produs4, 'stock' => [
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 2],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 8],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -3],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 11],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -4],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -3]
                                                    ]
                    ],
    $produs5->id => ['produs' => $produs5, 'stock' => [
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 17],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 8],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -13],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => 11],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -5],
                                                        ['date' => date("Y-M-D/h:i:s"), 'quantity' => -3]
                                                    ]
                    ],
    ]);


$stock->listStock($stock);
$stock->listStockStepByStep($stock);

$stock->addInStock($produs1, 4);
$stock->listStockStepByStep($stock);

$stock->removeFromStock($produs1, 2);
$stock->listStockStepByStep($stock);

// echo "Numarul de bucati pentru produsul 5 este".$stock->piecesNumber($produs5)."</br>";

echo str_repeat('*', 30);
echo "<h2 style='color: #0000cc'>Cos de cumparaturi contine </h2>";
echo "<br/>";
//Listare cos de cumparaturi
$cosCumparaturi1->listCosCumparaturi($cosCumparaturi1);

//Adaugare produs nou in cosul de cumparaturi
$produsDeAdaugat = $produs4;

echo "<h3 style='color: red'>Ati dorit sa introduceti un produs in cos: </h3>";
echo str_repeat('*', 30);
echo "</br>";
echo "$produsDeAdaugat->name -----$produsDeAdaugat->price RON </br>";
echo str_repeat('*', 30);
$cosCumparaturi1->addInCosCumparaturi(5, $produsDeAdaugat, $stock);
echo "</br>";

//Stergere produs existent din cosul de cumparaturi
echo "<h3 style='color: red'>Ati dorit sa  stergeti urmatorul produs din cos: </h3>";
$produsDeSters = $produs3;
echo str_repeat('*', 30);
echo "</br>";
echo "$produsDeSters->name -----$produsDeSters->price RON </br>";
echo str_repeat('*', 30);
echo "</br>";
$cosCumparaturi1->removeFromCosCumparaturi($produsDeSters);


//Stergere produs existent din cosul de cumparaturi
echo "<h3 style='color: red'>Ati dorit sa  stergeti urmatorul produs din cos: </h3>";
$produsDeSters = $produs5;
echo str_repeat('*', 30);
echo "</br>";
echo "$produsDeSters->name -----$produsDeSters->price RON </br>";
echo str_repeat('*', 30);
echo "</br>";
$cosCumparaturi1->removeFromCosCumparaturi($produsDeSters);

//Adaugare produs nou in cosul de cumparaturi
$produsDeAdaugat = $produs3;

echo "<h3 style='color: red'>Ati dorit sa introduceti un produs in cos: </h3>";
echo str_repeat('*', 30);
echo "</br>";
echo "$produsDeAdaugat->name -----$produsDeAdaugat->price RON </br>";
echo str_repeat('*', 30);
$cosCumparaturi1->addInCosCumparaturi(5, $produsDeAdaugat, $stock);
echo "</br>";


//Adaugare produs nou in cosul de cumparaturi
$produsDeAdaugat = $produs3;

echo "<h3 style='color: red'>Ati dorit sa introduceti un produs in cos: </h3>";
echo str_repeat('*', 30);
echo "</br>";
echo "$produsDeAdaugat->name -----$produsDeAdaugat->price RON </br>";
echo str_repeat('*', 30);
$cosCumparaturi1->addInCosCumparaturi(5, $produsDeAdaugat, $stock);
echo "</br>";


//Adaugare produs nou in cosul de cumparaturi
$produsDeAdaugat = $produs5;

echo "<h3 style='color: red'>Ati dorit sa introduceti un produs in cos: </h3>";
echo str_repeat('*', 30);
echo "</br>";
echo "$produsDeAdaugat->name -----$produsDeAdaugat->price RON </br>";
echo str_repeat('*', 30);
$cosCumparaturi1->addInCosCumparaturi(2, $produsDeAdaugat, $stock);
echo "</br>";








