<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 09-12-2018
 * Time: 3:23 PM
 */
class CosCumparaturi
{
    public $products=[];

    /**
     * CosCumparaturi constructor.
     * @param array $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function addInCosCumparaturi ($quantity, Product $product, Stock $stock){
        if (isset($this->products[$product->id]))
        {
            echo "</br><h3 style='color: #8fdf82'> Produsul exista deja</h3>";
        }
        else {
            if ($quantity<=$stock->piecesNumber($product)){
                $this->products[$product->id]=['quantity'=>$quantity,'produs'=>$product];
                $this->listCosCumparaturi($this);
            }
           else {
               echo "</br><h3 style='color: #8fdf82'> Nu exista $quantity produse dorite pe stoc</h3>";
           }
        }
    }

    public function removeFromCosCumparaturi (Product $product){
        if(!isset($this->products[$product->id])) {
            echo "</br><h3 style='color: #8fdf82'> Produsul pe care doriti sa il stergeti nu exista in cos</h3>";
        }
        else {
            unset($this->products[$product->id]);
        }
        $this->listCosCumparaturi($this);
    }

    public function listCosCumparaturi(cosCumparaturi $cos){
        $totalPrice=0;
        $nrCrt=0;
        echo "</br>";
        foreach ($cos->products as $key =>$productsList) {
                $nrCrt++;
                $totalProductPrice=0;
                $totalProductPrice=$productsList['produs']->price * $productsList['quantity'];
                $totalPrice+=$totalProductPrice;
                echo "$nrCrt.".$productsList['produs']->name."------".$productsList['produs']->price." RON x  ".$productsList['quantity']." - buc = $totalProductPrice RON</br>";
            }
        echo str_repeat('-',60)."</br> TOTAL de plata: $totalPrice RON";
    }
}