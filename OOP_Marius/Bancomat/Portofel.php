<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 03-12-2018
 * Time: 8:41 PM
 */
class Portofel
{
    public $bani;
    public $cards=array();

    /**
     * portofel constructor.
     * @param $bani
     */
    public function __construct($bani, $cards)
    {
        $this->bani = $bani;
        $this->cards = $cards;
    }

    /**
     * @return mixed
     */
    public function getBani()
    {
        return $this->bani;
    }

    public function In($amount){
        $this->bani+=$amount;
    }
    public function Out($amount){
        if ($this->bani>=$amount){
            $this->bani-=$amount;
        }
        else echo "Imi pare rau dar nu am atatia bani in portofel";
    }
}