<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 05-12-2018
 * Time: 7:10 PM
 */
class casaMarcat
{
    public $serie;
    public $bani;

    /**
     * casaMarcat constructor.
     * @param $serie
     * @param $bani
     */
    public function __construct($serie, $bani)
    {
        $this->serie = $serie;
        $this->bani = $bani;
    }

    /**
     * @param mixed $bani
     */
    public function setBani($bani)
    {
        $this->bani = $bani;
    }

    public function incasare($bani)
    {
        $this->bani+=$bani;
    }
    }