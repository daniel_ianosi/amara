<?php


/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 05-12-2018
 * Time: 11:05 AM
 */
class Bancomat
{
    public $cash;

    /**
     * Bancomat constructor.
     * @param $cash
     */
    public function __construct($cash)
    {
        $this->cash = $cash;
    }

    /**
     * @return mixed
     */
    public function getCash($amount)
    {
        if ($this->cash>=$amount){
            return true;
        }
    }

    /**
     * @param mixed $cash
     */
    public function setCash($cash)
    {
        $this->cash = $cash;
    }

    public function verifyPin(Card $cardIntrodus, $pinIntrodus)
    {
        if ($cardIntrodus->getPin() == $pinIntrodus) {
            return true;
        } else echo "PIN incorect! <br/><br/>";
    }

}