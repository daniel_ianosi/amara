<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 05-12-2018
 * Time: 10:48 AM
 */
class Card
{
    public $bank;
    public $sold;
    public $pin;

    /**
     * Card constructor.
     * @param $sold
     * @param $pin
     */
    public function __construct($bank, $sold, $pin)
    {
        $this->bank=$bank;
        $this->sold = $sold;
        $this->pin = $pin;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }
    /**
     * @return mixed
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * @return mixed
     */
    public function getSold()
    {
        return $this->sold;
    }

    public function withdrawl($amount){
        if ($amount<=$this->sold){
            $this->sold-=$amount;
        }
        else {
            echo "Ne pare rau, fonduri insuficiente <br/><br/>";
        }
    }

    public function deposit($amount){
       $this->sold+=$amount;
           }
}