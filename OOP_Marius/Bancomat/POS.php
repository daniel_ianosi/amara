<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 05-12-2018
 * Time: 7:15 PM
 */
class POS
{
    public $sold;

    public function __construct($sold)
    {
        $this->sold=$sold;
    }

    public function verifyPin(Card $cardIntrodus, $pinIntrodus)
    {
        if ($cardIntrodus->getPin() == $pinIntrodus) {
            return true;
        } else echo "PIN incorect! <br/><br/>";
    }

    public function payment($amount, Card $card){
        $this->sold+=$amount;
        $card->withdrawl($amount);
    }
}