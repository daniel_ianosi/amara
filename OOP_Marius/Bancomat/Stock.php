<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 10-12-2018
 * Time: 12:12 PM
 */
class Stock
{

    public $products=[];

    /**
     * Stock constructor.
     * @param array $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function addInStock($product, $quantity){
        $this->products[$product->id]['stock'][]=['quantity'=>$quantity,'date'=>date("Y-M-D/h:i:s")];
    }

    public function removeFromStock($product, $quantity){
        $this->products[$product->id]['stock'][]=['quantity'=>-$quantity,'date'=>date("Y-M-D/h:i:s")];
    }

    public function piecesNumber($product){
        $pieces=0;
        for ($i=0;$i<count($this->products[$product->id]['stock']);$i++)
        {
            $pieces+=$this->products[$product->id]['stock'][$i]['quantity'];
        }
        return $pieces;
    }

    /**
     * @param Stock $stock
     */
    public function listStock(Stock $stock){

        echo "</br><h2 style='color: #ffc107'>Produsele aflate pe stock si cantitatea aferenta:</h2> ";
        $nrCrt=0;
        foreach ($stock->products as $productsStock){
            $nrCrt++;
            echo "</br> $nrCrt. ".$productsStock['produs']->name." ------- ".$stock->piecesNumber($productsStock['produs'])." bucati; ";
        }
        echo "</br>";
    }

    public function listStockStepByStep(Stock $stock)
    {
        echo "</br><h2 style='color: #ffc107'>Produsele aflate pe stock si cantitatea aferenta:</h2> ";
        $nrCrt = 0;
        foreach ($stock->products as $productsStock) {
           for ($i = 0; $i < count($productsStock['stock']); $i++) {
                $nrCrt++;
                echo "</br> $nrCrt. Data: " . $productsStock['stock'][$i]['date'] . " ||=> " . $productsStock['produs']->name . " ------- " . $productsStock['stock'][$i]['quantity'] . " bucati;";
            }
            echo "</br>";
        }
    }
}