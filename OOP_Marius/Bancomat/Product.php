<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 09-12-2018
 * Time: 3:15 PM
 */
class Product
{
    public $id;
    public $name;
    public $description;
    public $price;

    /**
     * Product constructor.
     * @param $name
     * @param $description
     * @param $price
     */
    public function __construct($id, $name, $description, $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }


}

