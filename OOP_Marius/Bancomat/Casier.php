<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 05-12-2018
 * Time: 7:20 PM
 */
class Casier
{
    public $nume;
    public $prenume;
    public $casaDeMarcat=null;
    public $POS=null;

    /**
     * Casier constructor.
     * @param $nume
     * @param $prenume
     * @param $casaDeMarcat
     */
    public function __construct($nume, $prenume, casaMarcat $casaDeMarcat, POS $POS)
    {
        $this->nume = $nume;
        $this->prenume = $prenume;
        $this->casaDeMarcat = $casaDeMarcat;
        $this->POS = $POS;
    }

    public function buy($payBy,$amount){
        if ($payBy=='cash'){
            $this->tranzactieCash($amount);

        }
        if ($payBy=='POS'){
            $this->tranzactiePOS($amount);
        }
    }

    public function tranzactieCash($bani){
        $this->casaDeMarcat->incasare($bani);
    }

    public function tranzactiePOS($amount, $card){
        $this->POS->payment($amount,$card);
    }

}