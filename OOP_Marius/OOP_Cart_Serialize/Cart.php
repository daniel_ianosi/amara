<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 7:00 PM
 */
class Cart
{
    /** @var CartItem[] */
    public $cartItems = [];

    /**
     * Cart constructor.
     * @param CartItem[] $cartItems
     */
    public function __construct(array $cartItems=[])
    {
        $this->cartItems = $cartItems;
    }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function add($product, $quantity=1){
        if (!$product->isInStock()){
            return;
        }

            if (!isset($this->getCartItems()[$product->getId()])) {
                $this->cartItems[$product->getId()] = new CartItem($product, $quantity);
            } else {
                $this->getCartItems()[$product->getId()]->addQuantity($quantity);
            }
                          }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function update(BaseProduct $product, $quantity=1){
        if (!isset($this->getCartItems()[$product->getId()])){
            $this->cartItems[$product->getId()] = new CartItem($product, $quantity);
        } else {
            $this->getCartItems()[$product->getId()]->setQuantity($quantity);
        }
    }

    /**
     * @param Product $product
     */
    public function delete($product){

            echo "<h3> S-a dorit stergerea produsului " . $product->getName() . "</h3>";
            unset($this->cartItems[$product->getId()]);

    }

    public function getTotal(){
        $total = 0;
        foreach ($this->getCartItems() as $cartItem){
            $total += $cartItem->getTotalPrice();


        }

        return $total;
    }

    /**
     * @return CartItem[]
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }

    /**
     * @param CartItem[] $cartItems
     * @return Cart
     */
    public function setCartItems($cartItems)
    {
        $this->cartItems = $cartItems;
        return $this;
    }

    public function listCart(){
        foreach ($this->cartItems as $productId => $cartItem){
          echo "</br> $productId. " . $cartItem->getProduct()->getName() . " ----- " . $cartItem->getQuantity() . " buc x " . $cartItem->getProduct()->getPrice() . " lei = " . $cartItem->getTotalPrice() . " lei;";

        }
        echo "</br>";
    }
}