<?php
include "include.php";

?>

<html>
<header><h2 style=" color: red">Show Cart </h2> </header>
<br>
<div>
<table style="border: solid" border="1">
    <tr>
        <th> Nume </th>
        <th width="100px"> Cantitate</th>
        <th> Pret</th>
        <th> Total (Lei)</th>
        <th> Action</th>
    </tr>
    <?php foreach ($cart->getCartItems() as $cartItem):?>
        <tr>
            <td><?php echo $cartItem->getProduct()->getName() ?></td>
            <td style="text-align: center"><b><?php echo $cartItem->getQuantity(); ?></b>
                <form action="updateQuantity.php?id=<?php echo $cartItem->getProduct()->getId()?>" method="post">
                    <select name="updateQuantity">
                        <?php for ($i=1;$i<=20;$i++):?>
                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                        <?php endfor;?>
                    </select>
                    <button type="submit"> Update Quantity </button>
                </form>
            </td>
            <td style="text-align: right"><?php echo $cartItem->getProduct()->getPrice(); ?></td>
            <td style="text-align: right"><?php echo $cartItem->getTotalPrice(); ?></td>
            <td><a href="deleteCartItem.php?<?php echo "id=".$cartItem->getProduct()->getId()?>">Delete</a></td>
        </tr>
    <?php endforeach;?>
<tr border="2px" bgcolor="#a9a9a9">
    <td colspan="3"><b>TOTAL DE PLATA</b></td>
    <td style="text-align: right"><b><?php echo $cart->getTotal(); ?></b></td>
    <td><a href="finishOrder.php">Finish order</a></td>
</tr>
</table>

</div>
</br>
<div>
    <form action="addCartItem.php" method="post">
    <table>
        <tr>
            <td>
                Products
            </td>
            <td>
                Quantity
            </td>
        </tr>
        <tr>
            <td>
                <select name="id">
                    <?php foreach ($products as $product):?>
                         <option value="<?php echo $product->getId() ?>"><?php echo $product->getName() ?></option>
                   <?php endforeach;?>
                </select>
            </td>
            <td>
               <input type="text" name="quantity" />
            </td>
            <td>
                <button type="submit"> Add </button>
            </td>
        </tr>
    </table>
    </form>

</div>
</body>
</html>
