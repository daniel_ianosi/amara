<?php
include "Cart.php";
include "CartItem.php";
include "BaseProduct.php";
include "FashionProduct.php";
include "ExtraWarranty.php";

$product1 = new BaseProduct(1, 'Aspirator', 23.25, 'sdasdasd', true);
$product2 = new FashionProduct(7, 'Camasa', 13.99, 'sdasdasd', true,['S','L'],'S');
$product3 = new BaseProduct(20, 'Cuptor', 20.00, 'dadada', true);
$product4 = new BaseProduct(30, 'Geaca ski', 200.00, 'dadada', true);

$extraW1 = new ExtraWarranty(89,'Extra 1 an','daa',5, $product1);
$extraW2 = new ExtraWarranty(26,'Extra 1 an','daa',10, $product3);
$extraW3 = new ExtraWarranty(256,'Extra 1 an','daa',10, $product4);
$extraW4 = new ExtraWarranty(56,'Montaj','daa',20, $product1);

$cart = new Cart();
$cart->add($product1);
$product1->setInStock(true);

$cart->add($product1,8);
$cart->add($product2,10);
$cart->add($product3,4);

$cart->listCart();
echo "<h4> TOTAL--------->".$cart->getTotal()."</h4>";
echo "</br>";

$cart->add($extraW1,1);
$cart->add($extraW2,1);
$cart->add($extraW3,1);
$cart->add($extraW4,1);

$cart->listCart();
echo "<h4> TOTAL--------->".$cart->getTotal()."</h4>";

echo "</br>";
$cart->delete($extraW1);
$cart->listCart();
echo "</br>";

$cart->delete($product1);
$cart->listCart();
echo "<h4> TOTAL--------->".$cart->getTotal()."</h4>";
echo "</br>";

$cart->add($product1,8);
$cart->add($extraW1,1);
$cart->listCart();
echo "<h4> TOTAL--------->".$cart->getTotal()."</h4>";

$cart->delete($extraW1);
$cart->listCart();
echo "<h4> TOTAL--------->".$cart->getTotal()."</h4>";