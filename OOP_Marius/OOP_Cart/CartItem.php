<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 7:01 PM
 */
class CartItem
{
    /** @var  BaseProduct */
    public $product;
    /** @var int */
    public $quantity = 0;

    /**
     * @var CartItem
     */
    public $childrensProduct =[];

    /**
     * CartItem constructor.
     * @param BaseProduct $product
     * @param int $quantity
     */
    public function __construct(BaseProduct $product, $quantity=1)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return BaseProduct
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param BaseProduct $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @param int $quantity
     */
    public function addQuantity($quantity){
        $this->setQuantity($this->getQuantity()+$quantity);
    }


    /**
     * @return float
     */
    public function getTotalPrice(){
        if ($this->getQuantity()>=10){
            return ($this->getProduct()->getPrice()*$this->getQuantity())*0.8;
        }
        return $this->getProduct()->getPrice()*$this->getQuantity();
    }

    public function addChildren(ExtraWarranty $children){
        $this->childrensProduct[$children->getId()]=$children;
    }

    /**
     * @return CartItem
     */
    public function getChildrensProduct()
    {
        return $this->childrensProduct;
    }

    /**
     * @param CartItem $childrensProduct
     */
    public function setChildrensProduct($childrensProduct)
    {
        $this->childrensProduct = $childrensProduct;
    }


}