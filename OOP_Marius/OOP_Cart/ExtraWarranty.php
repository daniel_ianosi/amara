<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 10-12-2018
 * Time: 8:34 PM
 */
class ExtraWarranty extends BaseProduct
{
    /**
     * @var
     */

    public $percent;
    public $parentProduct;

    /**
     * ExtraWarranty constructor.
     * @param int $id
     * @param string $name
     * @param float $description
     * @param string $percent
     * @param BaseProduct $parentProduct
     */
    public function __construct($id, $name, $description, $percent, BaseProduct $parentProduct)
    {
        $this->percent=$percent;
        $this->parentProduct=$parentProduct;
        $price= $parentProduct->getPrice()*$percent/100;
        parent::__construct($id, $name, $price, $description, true);
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @return mixed
     */
    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    /**
     * @param mixed $parentProoduct
     */
    public function setParentProduct($parentProduct)
    {
        $this->parentProduct = $parentProduct;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}