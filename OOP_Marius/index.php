<?php
/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/3/2018
 * Time: 7:35 PM
 */

include "Brick.php";

$redBrick = new Brick();
$redBrick->cols= 20;
$redBrick->rows= 20;
$greyBrick = new Brick('grey');
//$greyBrick->colour='blue';
$blueSmallBrick = new Brick('blue',1,4);


//$redBrick->addBrick($greyBrick);

for ($i=0; $i<=100; $i++) {
    $redBrick->addBrick(new Brick('blue',rand(1,20),rand(1,20)));
}

//var_dump($redBrick);

//echo $redBrick->colour."\n";
//echo $blueSmallBrick->colour."\n";
//echo $greyBrick->colour."\n";

$redBrick->draw();