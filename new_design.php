<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Title</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
</head>
<body>
<div class="container">
    <?php include "new_components/header.php"; ?>
    <div id="content" class="row">
        <?php include "new_components/content.php"; ?>
        <?php include "new_components/sidebar.php"; ?>
    </div>
    <?php include "new_components/footer.php"; ?>
</div>
</body>
</html>