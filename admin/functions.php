<?php
include "../functions.php";

function checkUser(){
    if (!isset($_SESSION['user_name'])){
        header("Location: login_admin.php?message=Nu esti autentificat.");
        die;
    }
}
?>