-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2018 at 07:31 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `stock` int(10) NOT NULL,
  `id_category` int(10) NOT NULL,
  `description` text NOT NULL,
  `id_manufacturer` varchar(100) NOT NULL,
  `best_sellers` int(1) DEFAULT '0',
  `new_arrivals` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `id_category`, `description`, `id_manufacturer`, `best_sellers`, `new_arrivals`) VALUES
(1, 'Televizor 81cm, 4K-8K', 652, 21, 1, 'Televizorul bla bla bla', '1', 1, 0),
(14, 'Televizor 101cm LED TV', 1021, 5, 1, 'Televizor 101 bla bla bla', '1', 0, 1),
(15, 'Monitor 21 inch', 236, 12, 2, 'Monitorul de 21 inch este cu 2 inch mai mare decat cel de 19', '2', 1, 0),
(16, 'Monitor 19 inch', 156, 6, 2, 'Monitorul de 19 inch este cu 2 inch mai mic decat cel de 21', '3', 0, 0),
(17, 'Jaketa ski', 526, 8, 3, 'Nu poti sa mergi la ski fara jacheta ca altfel o sa faci turturi', '4', 0, 0),
(18, 'Pantaloni ski', 623, 9, 3, 'Mai sunt 3 saptamani si o sa merg la ski', '5', 0, 0),
(19, 'Masina de spalat', 896, 2, 4, 'Masina de spalat traieste mai mult cu callgon', '6', 0, 1),
(20, 'Aspirator', 325, 6, 4, 'Aspiratorul te scapa de matura', '1', 0, 0),
(21, 'Cuptor cu microunde', 198, 16, 4, 'Nu introduceti materiale de metal in cuptorul cu microunde', '2', 1, 0),
(22, 'Bormasina', 465, 3, 5, 'Bormasina este mercedesul patriarhilui Daniel', '7', 0, 1),
(23, 'Scaun', 26, 9, 6, 'Scaun la cap sa tot ai', '8', 0, 0),
(24, 'masa 6 persoane', 266, 3, 6, 'Masa de 6 persoane este masa cu 3 persoane pe o parte si 3 pe alta', '8', 0, 1),
(25, 'ceas barbatesc', 466, 15, 7, 'Ceasul poate sa fie cu cuc sau fara cuc', '9', 0, 0),
(26, 'ceas femeiesc', 366, 17, 7, 'Ceasul de dama este ceasul fara cuc', '10', 1, 0),
(27, 'Parfum 100ml', 266, 4, 8, 'Apa de parfum nu este apa de toaleta', '11', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
