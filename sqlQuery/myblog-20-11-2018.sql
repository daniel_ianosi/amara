-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2018 at 05:14 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`) VALUES
(1, 'Tavi'),
(2, 'Cristi'),
(3, 'Alex'),
(4, 'Marius'),
(5, 'Daniel');

-- --------------------------------------------------------

--
-- Table structure for table `blogpost`
--

CREATE TABLE `blogpost` (
  `id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `id_category` int(10) NOT NULL,
  `content` text NOT NULL,
  `id_author` int(10) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogpost`
--

INSERT INTO `blogpost` (`id`, `title`, `id_category`, `content`, `id_author`, `date`) VALUES
(1, 'Lorem_Ipsum1', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris egestas congue semper. In cursus, ex non tempor iaculis, elit felis pharetra arcu, vitae volutpat velit elit at nunc. Donec finibus pellentesque dui, tristique placerat est dignissim sed. Aenean mattis magna id pretium rhoncus. Donec ut orci id lorem fringilla consequat. Sed porta volutpat diam, at sollicitudin lectus scelerisque a. Pellentesque tristique et urna id volutpat. Vivamus fringilla rutrum tellus vel accumsan. In sodales sodales dolor sed pharetra. Cras ornare risus eu blandit laoreet. Donec id egestas mauris.\r\n\r\nVestibulum iaculis efficitur efficitur. Sed ornare augue eu eleifend dictum. Etiam posuere bibendum nisi, eu varius ex ultrices eu. Suspendisse non urna rhoncus erat dictum venenatis. Fusce at ipsum in ipsum sagittis laoreet. Morbi nec condimentum massa. Integer egestas, urna efficitur sodales facilisis, nibh quam semper massa, ut pulvinar tellus est vel purus. In gravida molestie sem at facilisis.\r\n\r\nInteger vestibulum venenatis sapien in euismod. In hac habitasse platea dictumst. Etiam efficitur, est eu maximus ultricies, nisl nunc auctor justo, in egestas leo sapien et nisl. Integer laoreet varius facilisis. Duis congue, felis vitae blandit tincidunt, libero elit dapibus orci, rhoncus sagittis mauris odio ac leo. Curabitur porttitor ipsum eget cursus consequat. Pellentesque eu justo semper, luctus mi id, tincidunt eros. Quisque tempor nulla a mi semper, nec volutpat massa hendrerit. Integer diam risus, volutpat a mollis sed, tristique eu massa. Pellentesque egestas, justo ac volutpat dictum, nibh leo commodo nisl, a placerat nibh ante eu ligula. Ut tincidunt massa ac augue posuere facilisis.\r\n\r\nNam eget est suscipit, porta augue ullamcorper, porta leo. Proin nunc ipsum, facilisis ac consequat eu, tempus in enim. In finibus leo vel velit sagittis, eu ornare tortor tempor. Aenean non lacus vestibulum, ullamcorper quam eu, pulvinar nunc. Vivamus eget feugiat magna. Cras consectetur sem libero. Ut sodales convallis est, a gravida risus viverra non. Pellentesque maximus pellentesque sem finibus tincidunt. Donec ultricies suscipit elit vitae elementum. Cras ullamcorper sapien vitae augue mattis condimentum. Proin scelerisque convallis urna malesuada maximus. Nullam id sem mauris. Aenean quis risus dignissim, pulvinar orci vitae, facilisis quam. Nam id est et dolor pulvinar vestibulum gravida vitae orci. Vivamus pretium pellentesque nulla in condimentum. Aenean placerat bibendum dolor eget malesuada.\r\n\r\nUt sed dui tincidunt, suscipit lacus ut, cursus orci. Praesent at eros at nisi faucibus cursus sit amet eget elit. Donec suscipit tristique posuere. Praesent pretium sapien massa, in interdum ante placerat vitae. Vivamus fringilla ante nec erat pellentesque, quis ullamcorper mi ultrices. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce a odio sit amet purus congue cursus eu id ipsum. Nam quis euismod mauris. Vivamus in arcu mattis lacus facilisis lacinia. Sed convallis iaculis purus, quis viverra lorem tempor ut. Etiam risus lacus, ultricies non ultricies non, bibendum eget libero. Mauris a ullamcorper mi, placerat dictum nunc.\r\n\r\nUt lacinia, nunc eget tempor feugiat, mauris velit cursus erat, ac malesuada massa sem at massa. Donec convallis sapien in fermentum porta. Etiam urna metus, condimentum a elit vitae, elementum ultricies neque. Suspendisse potenti. Fusce non enim quis enim accumsan molestie. Duis efficitur quam id euismod interdum. Aenean in mauris a felis gravida hendrerit vitae sit amet velit. Fusce rhoncus facilisis tellus. Sed elit diam, scelerisque sed nisl eu, varius finibus leo.\r\n\r\nInteger sodales mauris at mi scelerisque interdum. Nam ac diam at turpis porttitor convallis. Nunc vel euismod eros. Sed et est ut elit laoreet feugiat. Quisque faucibus diam in diam bibendum, eu commodo ante porta. Ut iaculis nunc quam, et commodo leo molestie sit amet. Donec ligula metus, molestie ac mi quis, fringilla vestibulum neque. Etiam dignissim turpis sit amet sollicitudin pellentesque. Sed dictum vitae magna sit amet porta. Donec a massa malesuada dolor finibus tincidunt nec vel augue. In egestas nec nisi ac gravida.\r\n\r\nDuis ullamcorper laoreet sapien, sed mattis eros. Aliquam imperdiet pulvinar nulla, vitae lacinia nulla. Mauris tempus diam ut mauris ultrices, non mollis lectus vulputate. Suspendisse lorem odio, vulputate id scelerisque ut, luctus a tortor. Integer tincidunt vulputate facilisis. Vivamus ut magna elit. Sed at tortor at augue rutrum eleifend at ac velit. Integer metus velit, mollis eget pretium quis, vehicula non purus. Quisque sed lorem varius, viverra quam quis, venenatis neque. Donec vulputate rhoncus elit, non eleifend sem sollicitudin a. Etiam tempus hendrerit mauris, non blandit quam. Cras sodales dolor ut sem consectetur ultricies. Suspendisse quis aliquet velit. Nullam vitae lacus quis magna molestie pellentesque. Aliquam erat volutpat. Nunc tempor malesuada orci, at maximus augue pellentesque a.\r\n\r\nUt eu cursus sapien, non vehicula mauris. Curabitur eget ligula leo. Maecenas non ultricies enim, vitae luctus mauris. Morbi ut accumsan est, et tincidunt felis. In aliquet aliquam felis, vel mollis dui condimentum malesuada. Mauris dignissim cursus nisl, eget malesuada lorem vulputate et. Quisque vel blandit erat, et maximus nulla.\r\n\r\nNullam quis odio faucibus, dapibus lacus vel, condimentum mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac augue augue. Mauris vulputate placerat tortor, ut suscipit odio pharetra eget. Nunc efficitur, turpis ut consequat vehicula, mauris nisi malesuada ligula, non sodales nulla diam at leo. Nam a lacus vestibulum erat scelerisque condimentum nec a nibh. Suspendisse rutrum convallis sagittis.', 1, '2018-10-01'),
(2, 'Lorem_Ipsum2', 2, 'Ut lacinia, nunc eget tempor feugiat, mauris velit cursus erat, ac malesuada massa sem at massa. Donec convallis sapien in fermentum porta. Etiam urna metus, condimentum a elit vitae, elementum ultricies neque. Suspendisse potenti. Fusce non enim quis enim accumsan molestie. Duis efficitur quam id euismod interdum. Aenean in mauris a felis gravida hendrerit vitae sit amet velit. Fusce rhoncus facilisis tellus. Sed elit diam, scelerisque sed nisl eu, varius finibus leo.\r\n\r\nInteger sodales mauris at mi scelerisque interdum. Nam ac diam at turpis porttitor convallis. Nunc vel euismod eros. Sed et est ut elit laoreet feugiat. Quisque faucibus diam in diam bibendum, eu commodo ante porta. Ut iaculis nunc quam, et commodo leo molestie sit amet. Donec ligula metus, molestie ac mi quis, fringilla vestibulum neque. Etiam dignissim turpis sit amet sollicitudin pellentesque. Sed dictum vitae magna sit amet porta. Donec a massa malesuada dolor finibus tincidunt nec vel augue. In egestas nec nisi ac gravida.\r\n\r\nDuis ullamcorper laoreet sapien, sed mattis eros. Aliquam imperdiet pulvinar nulla, vitae lacinia nulla. Mauris tempus diam ut mauris ultrices, non mollis lectus vulputate. Suspendisse lorem odio, vulputate id scelerisque ut, luctus a tortor. Integer tincidunt vulputate facilisis. Vivamus ut magna elit. Sed at tortor at augue rutrum eleifend at ac velit. Integer metus velit, mollis eget pretium quis, vehicula non purus. Quisque sed lorem varius, viverra quam quis, venenatis neque. Donec vulputate rhoncus elit, non eleifend sem sollicitudin a. Etiam tempus hendrerit mauris, non blandit quam. Cras sodales dolor ut sem consectetur ultricies. Suspendisse quis aliquet velit. Nullam vitae lacus quis magna molestie pellentesque. Aliquam erat volutpat. Nunc tempor malesuada orci, at maximus augue pellentesque a.\r\n\r\nUt eu cursus sapien, non vehicula mauris. Curabitur eget ligula leo. Maecenas non ultricies enim, vitae luctus mauris. Morbi ut accumsan est, et tincidunt felis. In aliquet aliquam felis, vel mollis dui condimentum malesuada. Mauris dignissim cursus nisl, eget malesuada lorem vulputate et. Quisque vel blandit erat, et maximus nulla.', 1, '2018-10-01'),
(3, 'Lolo', 1, 'Nullam quis odio faucibus, dapibus lacus vel, condimentum mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac augue augue. Mauris vulputate placerat tortor, ut suscipit odio pharetra eget. Nunc efficitur, turpis ut consequat vehicula, mauris nisi malesuada ligula, non sodales nulla diam at leo. Nam a lacus vestibulum erat scelerisque condimentum nec a nibh. Suspendisse rutrum convallis sagittis.', 2, '2018-10-05'),
(4, 'Ohoho', 4, 'Nullam quis odio faucibus, dapibus lacus vel, condimentum mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac augue augue. Mauris vulputate placerat tortor, ut suscipit odio pharetra eget. Nunc efficitur, turpis ut consequat vehicula, mauris nisi malesuada ligula, non sodales nulla diam at leo. Nam a lacus vestibulum erat scelerisque condimentum nec a nibh. Suspendisse rutrum convallis sagittis. blabla', 2, '2018-10-06'),
(5, 'Nohoho', 3, 'Nullam quis odio faucibus, dapibus lacus vel, condimentum mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac augue augue. Mauris vulputate placerat tortor, ut suscipit odio pharetra eget. Nunc efficitur, turpis ut consequat vehicula, mauris nisi malesuada ligula, non sodales nulla diam at leo. Nam a lacus vestibulum erat scelerisque condimentum nec a nibh. Suspendisse rutrum convallis sagittis. blabla', 3, '2018-10-07'),
(7, 'Testam ceva', 5, 'CRAIOVA CASTIGA CUPA CAMPIONILOR !!!!\r\nAstfel devine favorita in fata Barcelonei la castigarea trofeului \"cea mai buna echipa de fotbal din Galaxie\". Bravooooo Mitrita !!! Ronaldo si Mesi este in genunchi , plang ca niste copii. Mandrii ca suntem OLTENI !!!!', 2, '2018-10-09'),
(10, 'Testam ceva', 2, 'Andra a lu Maruta fara pu... este gravida pentru a 7-a oara. Oare cum de este posibil ??? ', 4, '2018-10-12'),
(11, 'proba', 3, 'de proba', 5, '2018-10-14'),
(12, 'sdefsd', 4, 'dsgfsdf', 3, '2018-10-17'),
(14, 'Testam ceva', 1, 'CRAIOVA CASTIGA CUPA CAMPIONILOR !!!!\r\nAstfel devine favorita in fata Barcelonei la castigarea trofeului \"cea mai buna echipa de fotbal din Galaxie\". Bravooooo Mitrita !!! Ronaldo si Mesi este in genunchi , plang ca niste copii. Mandrii ca suntem OLTENI !!!!', 2, '2018-10-21');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'IT'),
(2, 'SPORT'),
(3, 'MONEY'),
(4, 'INTERNATIONAL'),
(5, 'ECONOMIC'),
(6, 'NEWS');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `review` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `id_post`, `name`, `comment`, `review`) VALUES
(1, 1, 'Ion', 'From Jeff: This is a guest post by Jeremy Myers. Jeremy is a prison chaplain, author, dad, and husband. You can follow his blog Till He Comes or connect with him on Twitter.\r\nEvery blogger loves comments. We communicators relish the responses our messages cause, and blogging allows this to happen instantaneously. But not all messages are always worth equal response. So how do you know?', 'Accepted'),
(2, 1, 'Ghe', 'Jeremy is a prison chaplain, author, dad, and husband. You can follow his blog Till He Comes or connect with him on Twitter.\r\nEvery blogger loves comments. We communicators relish the responses our messages cause, and blogging allows this to happen instantaneously. But not all messages are always worth equal response. So how do you know?', 'Accepted'),
(3, 1, 'Me', 'ITSY BITSY SPIDER', 'Accepted'),
(4, 2, 'Gigel', 'HALLO !', 'Accepted'),
(5, 2, 'Alfredo', 'COOL ! ', 'Accepted'),
(7, 3, 'Daniel', 'WAHAAAA !', 'Accepted'),
(8, 2, 'Tavi', 'CEVA LIPSESTE ! ', 'Accepted'),
(9, 3, 'Alex', 'ALEX MANANCA BOMBOANE !', 'Accepted'),
(10, 4, 'Xiang', 'Yunh Nhu Ming Cho', 'Accepted'),
(11, 1, 'YUNG', 'XIAOMI', 'Accepted'),
(12, 1, 'Test', 'Another ordinary comment !', 'Accepted'),
(13, 1, 'Tester', 'Another ordinary comment !', 'Accepted'),
(14, 1, 'Anca', 'Am vazut si eu !', 'Accepted'),
(15, 6, 'Cristi', 'HUOOO', 'Accepted'),
(16, 6, 'Alexandru', 'dupa ce scriu', 'Accepted'),
(17, 6, 'Spam', 'Another test', 'Accepted'),
(18, 6, 'Me', 'This is annoying', ''),
(19, 6, 'Catalin', 'Craiova nr 1', 'Accepted'),
(20, 1, 'xxo', '', 'Accepted'),
(21, 1, 'Gaby', 'YUMMY', 'Accepted'),
(22, 1, 'BUNNY', 'I DID IT BITCHES !', ''),
(24, 1, '', 'ss', ''),
(28, 1, 'tavi', 's2', 'Accepted'),
(29, 1, 'tavi', 's3', 'Accepted'),
(30, 2, 'cristian', 'YUMMY', ' '),
(31, 1, 'tavi', 'test', 'Accepted'),
(32, 1, 'marius', 'add direct', ' '),
(33, 1, '<br />\r\n<b>Notice</b>:  Undefined index: user in <', 'I DID IT BITCHES !', 'Accepted'),
(34, 1, '<br />\r\n<b>Notice</b>:  Undefined index: user in <', 'YUMMY', 'Accepted'),
(35, 1, 'tavi', 'add direct', 'Accepted'),
(41, 1, 'tavi', 'xoxo', 'Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `stock` int(10) NOT NULL,
  `id_category` int(10) NOT NULL,
  `description` text NOT NULL,
  `id_manufacturer` varchar(100) NOT NULL,
  `best_sellers` int(1) DEFAULT '0',
  `new_arrivals` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `id_category`, `description`, `id_manufacturer`, `best_sellers`, `new_arrivals`) VALUES
(1, 'Televizor 81cm, 4K-8K', 652, 21, 1, 'Televizorul bla bla bla', '1', 1, 0),
(14, 'Televizor 101cm LED TV', 1021, 5, 1, 'Televizor 101 bla bla bla', '1', 0, 1),
(15, 'Monitor 21 inch', 236, 12, 2, 'Monitorul de 21 inch este cu 2 inch mai mare decat cel de 19', '2', 1, 0),
(16, 'Monitor 19 inch', 156, 6, 2, 'Monitorul de 19 inch este cu 2 inch mai mic decat cel de 21', '3', 0, 0),
(17, 'Jaketa ski', 526, 8, 3, 'Nu poti sa mergi la ski fara jacheta ca altfel o sa faci turturi', '4', 0, 0),
(18, 'Pantaloni ski', 623, 9, 3, 'Mai sunt 3 saptamani si o sa merg la ski', '5', 0, 0),
(19, 'Masina de spalat', 896, 2, 4, 'Masina de spalat traieste mai mult cu callgon', '6', 0, 1),
(20, 'Aspirator', 325, 6, 4, 'Aspiratorul te scapa de matura', '1', 0, 1),
(21, 'Cuptor cu microunde', 198, 16, 4, 'Nu introduceti materiale de metal in cuptorul cu microunde', '2', 1, 0),
(22, 'Bormasina', 465, 3, 5, 'Bormasina este mercedesul patriarhilui Daniel', '7', 0, 1),
(23, 'Scaun', 26, 9, 6, 'Scaun la cap sa tot ai', '8', 0, 0),
(24, 'masa 6 persoane', 266, 3, 6, 'Masa de 6 persoane este masa cu 3 persoane pe o parte si 3 pe alta', '8', 0, 1),
(25, 'ceas barbatesc', 466, 15, 7, 'Ceasul poate sa fie cu cuc sau fara cuc', '9', 1, 0),
(26, 'ceas femeiesc', 366, 17, 7, 'Ceasul de dama este ceasul fara cuc', '10', 0, 0),
(27, 'Parfum 100ml', 266, 4, 8, 'Apa de parfum nu este apa de toaleta', '11', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(10) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `category`) VALUES
(1, 'Televizoare'),
(2, 'Monitoare si Calculatoare'),
(3, 'Imbracaminte'),
(4, 'Electrocasnice'),
(5, 'Scule si unelte'),
(6, 'Mobilier'),
(7, 'Ceasuri'),
(8, 'Parfumuri');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `id_produs` int(10) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_image` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `id_produs`, `file_name`, `main_image`) VALUES
(1, 16, 'Note8.jpg', 0),
(3, 1, 'Note8.jpg', 0),
(4, 15, 'S9_view.jpg', 1),
(5, 1, 'J7+.jpg', 0),
(6, 1, 'J7.jpg', 0),
(7, 14, '101led.jpg', 1),
(8, 14, 'Note8.jpg', 0),
(10, 23, 'scaun2.jpg', 1),
(11, 23, 'scaun3.jpg', 0),
(12, 23, 'scaun1.jpg', 0),
(13, 24, 'masa1.jpg', 0),
(14, 24, 'masawow.jpg', 1),
(15, 24, 'masa.jpg', 0),
(16, 27, 'parfum1.jpg', 0),
(17, 27, 'parfum2.jpg', 0),
(18, 27, 'parfum3.jpg', 0),
(19, 27, '101led.jpg', 0),
(20, 27, 'S9_view.jpg', 0),
(21, 27, 'scaun2.jpg', 0),
(22, 27, 'parfum1.jpg', 1),
(23, 16, 'J7+.jpg', 1),
(24, 1, 'tv81.jpg', 1),
(29, 25, 'ceas.jpg', 1),
(30, 25, 'c.jpg', 0),
(31, 25, 'ceas1.jpg', 0),
(33, 1, 'Net.jpg', 0),
(34, 17, '1.jpg', 1),
(35, 17, '2.jpg', 0),
(36, 21, 'cuptor cu microunde.jpg', 1),
(37, 19, 'masina de spalat.jpg', 1),
(38, 22, 'bormasina.jpg', 1),
(39, 20, 'aspirator.jpg', 1),
(40, 18, 'panataloni ski.jpg', 1),
(41, 26, 'ceas dama.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_manufacturer`
--

CREATE TABLE `product_manufacturer` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_manufacturer`
--

INSERT INTO `product_manufacturer` (`id`, `name`) VALUES
(1, 'SAMSUNG'),
(2, 'PHILIPS'),
(3, 'LG'),
(4, 'SALOMON'),
(5, 'HEAD'),
(6, 'ARCTIC'),
(7, 'BOSCH'),
(8, 'ADMA DESIGN'),
(9, 'FOSSIL'),
(10, 'DANIEL KLEIN'),
(11, 'BVLGARI');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `type`, `password`) VALUES
(1, 'daniel', 1, 'daniel'),
(2, 'tavi', 1, 'tavi'),
(3, 'alex', 1, 'alex'),
(4, 'cristian', 0, 'cristian'),
(5, 'marius', 0, 'marius');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogpost`
--
ALTER TABLE `blogpost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_manufacturer`
--
ALTER TABLE `product_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_unique` (`user_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blogpost`
--
ALTER TABLE `blogpost`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `product_manufacturer`
--
ALTER TABLE `product_manufacturer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
