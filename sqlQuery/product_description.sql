-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2018 at 07:36 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_description`
--

CREATE TABLE `product_description` (
  `id` int(10) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_description`
--

INSERT INTO `product_description` (`id`, `description`) VALUES
(1, 'anskajshf asfa kafskjasf akahsfkjans fak asflsjfa'),
(2, 'jkshfkansfk a akahsfkjans fak asfjaklsjfa'),
(3, 'jkshfkansfk akajshf asfa kafskjjans faklsjfa'),
(4, 'jksfskjasf akahsfkjans fak asfjaklsjfa'),
(5, 'jkshfkansfk akajshf asfa kafskjasf akahjaklsjfa'),
(6, 'jkshfkansfkfskjasf akahsfkjans fak asfjaklsjfa'),
(7, 'jkshfkansfk akajshf asfa kans fak asfjaklsjfa'),
(8, 'jkshfkaa kafskjasf akahsfkjans fjaklsjfa'),
(9, 'jkssffk akajshf asfa kaf68 akahsfkjans fak asfjaklsjfa'),
(10, 'jkshfksafasfkajshf asfa kafsk68ahak asfjaklsjfa'),
(11, 'jkshetryf asfghksfa kafskjasf akahs68ak asfjaklsjfa'),
(12, 'jkshfkalyuikafskjasf akahsfkjans fak asfjaklsjfa'),
(13, 'jkshfsfasgffskjasf lans fak asfjaklsjfa'),
(14, 'jkshfkansfk akajshf asfa kafkjans fak asfjaklsjfa'),
(15, 'jkshfkansfk akasfaskjasf akahsfkjans fak asfjaklsjfa'),
(16, 'jkshfkansfk akasf akahsfkjans fak asfjaklsjfa'),
(17, 'jkshfkansfk akaasfasf akahsfkjsfsfjaklsjfa'),
(18, 'jkshfkansfk akajshf asfa kafafass fak asfjaklsjfa'),
(19, 'jkshfkansfk akaakahsfasf fak asfjaklsjfa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_description`
--
ALTER TABLE `product_description`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_description`
--
ALTER TABLE `product_description`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
