-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2018 at 12:34 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------


--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(10) NOT NULL,
  `id_product` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `stock` int(10) NOT NULL,
  `delivery` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `id_product`, `name`, `stock`, `delivery`) VALUES
(1, 1, 'Televizor 81cm, 4K-8K', 3, 5),
(2, 14, 'Televizor 101cm LED TV', 10, 7),
(3, 15, 'Monitor 21 inch', 6, 3),
(4, 16, 'Monitor 19 inch', 2, 2),
(5, 17, 'Jaketa ski', 3, 6),
(6, 18, 'Pantaloni ski', 2, 3),
(7, 19, 'Masina de spalat', 4, 1),
(8, 20, 'Aspirator', 5, 4),
(9, 21, 'Cuptor cu microunde', 2, 5);

-- --------------------------------------------------------

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
