-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2018 at 09:56 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `id_produs` int(10) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_image` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `id_produs`, `file_name`, `main_image`) VALUES
(1, 16, 'Note8.jpg', 0),
(3, 1, 'Note8.jpg', 0),
(4, 15, 'S9_view.jpg', 1),
(5, 1, 'J7+.jpg', 0),
(6, 1, 'J7.jpg', 0),
(7, 14, '101led.jpg', 1),
(8, 14, 'Note8.jpg', 0),
(10, 23, 'scaun2.jpg', 1),
(11, 23, 'scaun3.jpg', 0),
(12, 23, 'scaun1.jpg', 0),
(13, 24, 'masa1.jpg', 0),
(14, 24, 'masawow.jpg', 1),
(15, 24, 'masa.jpg', 0),
(16, 27, 'parfum1.jpg', 0),
(17, 27, 'parfum2.jpg', 0),
(18, 27, 'parfum3.jpg', 0),
(19, 27, '101led.jpg', 0),
(20, 27, 'S9_view.jpg', 0),
(21, 27, 'scaun2.jpg', 0),
(22, 27, 'parfum1.jpg', 1),
(23, 16, 'J7+.jpg', 1),
(24, 1, 'tv81.jpg', 1),
(29, 25, 'ceas.jpg', 1),
(30, 25, 'c.jpg', 0),
(31, 25, 'ceas1.jpg', 0),
(33, 1, 'Net.jpg', 0),
(34, 17, '1.jpg', 1),
(35, 17, '2.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
