<?php
session_start();
include "Cart.php";
include "CartItem.php";
include "BaseProduct.php";

$allProducts[1] = new BaseProduct(1, 'Aspirator', 23.25, 'Aspiratoarele trag tot.', true);
$allProducts[2] = new BaseProduct(2, 'Ventilator', 14.20, 'Ventilatoarele sunt perfecte pentru taiat degete.', true);
$allProducts[3] = new BaseProduct(3, 'Bormasina', 19.82, 'Bormasina face multa liniste.', true);
$allProducts[4] = new BaseProduct(4, 'Haine Vechi', 19.82, 'Hainele Vechi sunt cool.', true);
$allProducts[5] = new BaseProduct(5, 'Scuter Prapadit', 19.82, 'Scuterul paradit nu este neaparat un scuter nou.', true);

if (!isset ($_SESSION['cart'])) {
    $_SESSION['cart'] = serialize(new Cart());
}

$cart = unserialize($_SESSION['cart']);
?>