<?php
include "forIncluding.php";
?>

<head>
    <title>Alex Shopping Cart</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css" >
    <link rel="stylesheet" href="../css/cartstyle.css" >
</head>

<body>
<h2>Shopping Cart</h2>
<div class="container col-xl-9 col-lg-9 col-sm-6">
    <div id="content" class="row">
        <table>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total per product</th>
            </tr>
            <?php foreach ($cart->getCartItems() as $cartItem):?>
                <tr>
                    <td>
                        <?php echo $cartItem->getProduct()->name; ?>
                        <form action="delete-product.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $cartItem->getProduct()->id; ?>">
                            <button type="submit" >Remove from cart</button>
                        </form>
                    </td>
                    <td>
                        <form action="update-product.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $cartItem->getProduct()->id; ?>" />
                            <input type="number" name="quantity" min="1" max="100" value="<?php echo $cartItem->getQuantity(); ?>" />
                            <button type="submit" >Update</button>
                        </form>
                    </td>
                    <td><?php echo number_format($cartItem->getProduct()->getPrice(), 2, '.', ''); ?></td>
                    <td><?php echo number_format($cartItem->getTotalPrice(), 2, '.', '');; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="text-right">
        <p><b>TOTAL: <?php echo $cart->getTotal(); ?> Lei</b></p>
    </div>
    <div id="content" class="row">
        <form action="add-product.php" method="post">
            <select name="id">
                <?php foreach ($allProducts as $product):?>
                    <option value="<?php echo $product->getId(); ?>"><?php echo $product->getName(); ?></option>
                <?php endforeach; ?>
            </select>
            <input type="number" name="quantity" min="1" max="100" value="1"/>
            <button type="submit">Add product to cart</button>
        </form>
    </div>
</div>
</body>