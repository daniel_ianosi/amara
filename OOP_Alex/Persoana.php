<?php

/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/3/2018
 * Time: 8:39 PM
 */
class Persoana
{
    Public $Nume;

    Public $Prenume;

    Public $Portofel;

   // public $card;

    //public $casier;

   // public $pos;

    /**
     * Persoana constructor.
     * @param $Nume
     * @param $Prenume
     * @param $Portofel
     */
    public function __construct($Nume, $Prenume, Portofel $Portofel)
    {
        $this->Nume = $Nume;
        $this->Prenume = $Prenume;
        $this->Portofel = $Portofel;
        //$this->card = $card;
        //$this->casier = $casier;
    }

    public function Give(Persoana $receiver,$amount){
        if ($amount>0 && ($this->Portofel->out($amount))== TRUE){
        $receiver->Portofel->in($amount);
        }

    }
    public function Take($amount ){
        $this->Portofel->in($amount);
    }

    public function personRetrieveFromATM(ATM $ATM, Card $card, $pinNumber, $bani){
        if ($card->retrieveMoneyFromCard($bani, $pinNumber)){
            if($ATM->retrieveMoneyFromATM($bani)){
                echo "Money successfully retrieved!";
                $this->Portofel->bani += $bani;
            } else {
                echo "Operation was unsuccessful!";
            }
        }

    }

    public function personDepositInATM(ATM $ATM, Card $card, $pinNumber, $bani){
        if ($this->Portofel->out($bani)){
            if($card->depositInCard ($bani, $pinNumber)){
                echo "Deposit was successfully done!";
                $ATM->insertMoneyInATM($bani);
        } else {
            echo "Insufficient funds";
            }
        }
    }

    public function buyWithCash(Casier $casier, $amount){
        if ($this->Portofel->out($amount)){
            $casier->receiveCash($amount);
        } else {
            echo "Insufficient money";
        }
    }

    public function buyWithCard(Casier $casier, Card $card, $amount){
        if ($card->retrieveMoneyFromCard ($amount, $pinNumber)){
            $casier->receiveCard($amount);
        } else {
            echo "Transaction failed!";
        }
    }

}
