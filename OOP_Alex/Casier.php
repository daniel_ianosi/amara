<?php


class Casier
{
    public $name;
    /** @var Casa_Marcat  */
    public $casaDeMarcat;
    /** @var POS */
    public $pos;

    /**
     * Casier constructor.
     * @param $name
     * @param Casa_Marcat $casaDeMarcat
     * @param POS $pos
     */

    public function __construct($name, Casa_Marcat $casaDeMarcat, POS $pos)
    {
        $this->name = $name;
        $this->casaDeMarcat = $casaDeMarcat;
        $this->pos = $pos;
    }

    public function receiveCash($amount){
        $this->casaDeMarcat->insert($amount);
    }

    public function receiveCard($amount){
        $this->pos->inFromCard($amount);
    }


}