<?php

class POS
{
    public $card;
    public $amount;
    public $pinNumber;

    public function __construct(Card $card, $amount){
        $this->card = $card;
        $this->amount = $amount;

    }

    public function inFromCard($amount) {
        $this->card->retrieveMoneyFromCard($amount, $pinNumber);
    }
}
