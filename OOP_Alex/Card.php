<?php

/**
 * Created by PhpStorm.
 * User: Alexandru
 * Date: 05.12.2018
 * Time: 0:52
 */
class Card
{
    public $bani;
    public $pinNumber;

    /**
     * Card constructor.
     * @param $bani
     * @param $pinNumber
     */
    public function __construct ($bani, $pinNumber){
        $this->bani = $bani;
        $this->pinNumber = $pinNumber;
    }

    public function depositInCard ($bani, $pinNumber){
        if ($bani > 0){
            if($this->pinNumber == $pinNumber) {
                $this->$bani += $bani;
                $condition = true;
            } else {
                echo "Wrong PIN number!";
                $condition = false;
            }
        } else {
            echo "Please insert a positive amount!";
            $condition = false;
        }
        return $condition;
    }

    public function retrieveMoneyFromCard ($bani, $pinNumber){
        if ($bani > 0) {
            if (($this->pinNumber == $pinNumber) && ($this->$bani >= $bani)) {
                $this->bani -= $bani;
                $condition = true;
            } elseif ($this->$pinNumber != $pinNumber) {
                echo "Wrong PIN number!";
                $condition = false;
            } else {
                echo "Insufficient funds!";
                $condition = false;
            }
        } else {
            echo "Please retrieve a positive amount!";
            $condition = false;
        }
        return $condition;
    }
}