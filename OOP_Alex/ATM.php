<?php

/**
 * Created by PhpStorm.
 * User: Alexandru
 * Date: 05.12.2018
 * Time: 0:50
 */
class ATM
{
    public $bani;

    /**
     * ATM constructor.
     * @param $bani
     */
    public function __construct($bani){
        $this->bani = $bani;
    }

    public function insertMoneyInATM($bani){
        if ($bani > 0) {
            $this->bani += $bani;
        } else {
            echo "Please insert a positive amount!";
        }
    }

    public function retrieveMoneyFromATM($bani){
        if ($bani > 0){
            if($this->bani >= $bani){
                $this->bani -= $bani;
                return true;
            } else {
                echo "Insufficient funds!";
                return false;
            }
        } else {
            echo "Please retrieve a positive amount!";
            return false;
        }
    }
}