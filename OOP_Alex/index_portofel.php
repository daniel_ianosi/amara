<?php
/**
 * Created by PhpStorm.
 * User: ro_ta
 * Date: 12/3/2018
 * Time: 8:52 PM
 */
include "Persoana.php";
include "Portofel.php";
include "Casier.php";
include "ATM.php";
include "Card.php";
include "Casa_Marcat.php";
include "POS.php";

$PortofelBlack= new Portofel(999);
$PortofelWhite=new Portofel(666);
$blackPerson=new Persoana('Dita','Octavian',$PortofelBlack);
$blackWife=new Persoana('Dita','Anca',$PortofelBlack);
$whitePerson=new Persoana('Preafericitul','Daniel',$PortofelWhite);

echo $whitePerson->Nume." are ".$whitePerson->Portofel->bani." lei \n";

$whitePerson->give($blackPerson,555);

echo $blackPerson->Prenume." a primit de la ".$whitePerson->Nume." 555 lei si are ".$blackPerson->Portofel->bani;
echo "\n".$whitePerson->Nume." mai are ".$whitePerson->Portofel->bani;

$blackWife->give($whitePerson, 666);
echo "\n".$blackWife->Prenume." ia dat lui ".$whitePerson->Nume." 666 lei \n";
echo $blackPerson->Prenume." are acum ".$blackPerson->Portofel->bani;
echo "\n".$whitePerson->Nume." are acum ".$whitePerson->Portofel->bani;

$blackPerson->Take(999);

echo "\nKarma a facut dreptate si ".$blackPerson->Prenume." are ".$blackPerson->Portofel->bani;


$casier = new Casier();
$blackWife->buyWithCash($casier, 100);
$pos = new POS();
$card = new Card();
$blackPerson -> buyWithCard($casier, 250);

?>