<?php


class Casa_Marcat
{
    public $bani;

    public function __construct($bani){
        $this->bani = $bani;
    }

    public function insert($bani){
        $this->bani += $bani;
    }

    public function retrieve($bani){
        if ($this->bani >= $bani){
            $this->bani -= $bani;
            return true;
        }
        echo "Insufficient money!";
        return false;
    }
}