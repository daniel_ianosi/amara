<?php

include "Persoana.php";
include "Portofel.php";
include "Card.php";
include "ATM.php";

$ATM1 = new ATM(10000);

$card1 = new Card(1234,1000);
$card2 = new Card(0000, 400);
$card3 = new Card(0123, 350);
$Portofel1=new Portofel(600, [$card1, $card2, $card3]);
$Person1=new Persoana('Gigioc', 'Gigi', $Portofel1);

//Insert money into ATM
echo "On card1 there are ".$card1->bani." lei. <br/>";
echo "On card2 there are ".$card2->bani." lei. <br/>";
echo "There are ".$ATM1->bani." lei into ATM before insertion. <br/>";
$Person1->personDepositInATM($ATM1, $card1, 11,1234);
echo "<br/>";
echo $Person1->Nume." deposits 555 lei on card1. <br/>";
echo "On card1 there are now ".$card1->bani." lei. <br/>";
echo "In ATM1 there are now ".$ATM1->bani." lei. <br/>";
echo "<br/>";
echo "<br/>";
//Retieve money from ATM
echo $Person1->Nume." has ".$Person1->Portofel->bani." in his wallet! <br/>";
$Person1->personRetrieveFromATM($ATM1, $card2, 200, 0000);
echo $Person1->Nume." retrieves 300 lei from card 2. <br/>";
echo "Now, ".$Person1->Nume." has ".$Person1->Portofel->bani." lei in his wallet! <br/>";

