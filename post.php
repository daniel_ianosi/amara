<?php include "functions.php"; ?>
<?php global $idPost; ?>
<?php include "componets/header.php";?>
<?php if (!isset($_GET['message'])){ $_SESSION['url']=$_SERVER['REQUEST_URI'];} ?>
<?php $post= getOneUniversal('blogpost',['id'=>$_GET['id']]); ?>
<div class="blog_post">
    <h1><a href="#intro" id="intro"><?php echo $post['title']; ?></a></h1>
    <p>
        <?php echo $post['content']; ?>
    </p>
    <div class="article_menu">
        <?php $aut=getOneUniversal('authors',['id'=>$post['id_author']]) ?>
        <b>Posted by <a href="author.php?author=<?php echo $post['id_author']; ?>"><?php echo $aut['name']; ?></a></b>
        <?php $cat=getOneUniversal('category',['id'=>$post['id_category']]) ?>
        <a href="category.php?category=<?php echo $post['id_category']; ?>"><?php echo $cat['name']; ?></a>
        <br />
        <p><b>Date: <a style="color:red" href="date.php?date=<?php echo $post['date']; ?>"><?php echo $post['date']; ?></a></b></p>
    </div>
</div>
    <form action="index.php">
        <td><button type="submit" >Back to Index</button></td>
    </form>
<h2> COMMENTS: </h2>
<?php foreach (getUniversal('comments',['id_post'=>$post['id']],[],[],[]) as $comm): ?>
<div   class="comments">
    <?php if ($comm['review']=='Accepted') { ?>
       <hr>
    <p>
        <?php echo $comm['comment'];
        echo ' | By: '.$comm['name'].'</br>';?>
    </p>
    <?php } ?>
</div>


<?php endforeach;?>
<?php if (isset($_SESSION['user_name'])): ?>
<form action="addComment.php" method="post">
<table>
    <tr>
        <th>Comment :</th>
        <td><input type="text" name="Comment"/></td>
    </tr>
    <tr>
        <td><input type="hidden" name="CommentName" value="<?php echo $_SESSION['user_name']; ?>" /></td>
    </tr>
    <tr>
        <td><button type="submit" >SUBMIT</button></td>
    </tr>
    <td><input type="hidden" name="id" value="<?php echo $post['id']; ?>"/> </td>
    <?php if ($_SESSION['type']==1){ ?>
        <td><input type="hidden" name="review" value="Accepted"/></td>
    <?php } else { ?>
    <td><input type="hidden" name="review" value=" "/></td>
    <?php }; ?>
</table>
</form>
<?php endif;?>
<?php include "componets/footer.php" ;?>
