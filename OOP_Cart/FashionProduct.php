<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 8:03 PM
 */
class FashionProduct extends BaseProduct
{
    /** @var array  */
    public $sizes=[];

    public $currentSize;

    public function getPrice()
    {
        switch ($this->currentSize){
            case 'S':  return parent::getPrice();
            case 'L':  return parent::getPrice()*1.1;
            case 'XL':  return parent::getPrice()*1.2;
            case 'XXL':  return parent::getPrice()*1.3;
        }
    }


    /**
     * @return boolean
     */
    public function isStockable()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'fashion_product';
    }

    public function setExtraAttrs($data)
    {
        $this->setSizes(explode(',',$data['sizes']));
        $this->setCurrentSize($data['current_size']);
    }

    /**
     * @return array
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * @param array $sizes
     */
    public function setSizes($sizes)
    {
        $this->sizes = $sizes;
    }

    /**
     * @return mixed
     */
    public function getCurrentSize()
    {
        return $this->currentSize;
    }

    /**
     * @param mixed $currentSize
     */
    public function setCurrentSize($currentSize)
    {
        $this->currentSize = $currentSize;
    }


}