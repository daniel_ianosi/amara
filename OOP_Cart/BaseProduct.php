<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 12/10/2018
 * Time: 7:05 PM
 */
abstract class BaseProduct
{
    /** @var  int */
    public $id;
    /** @var  string */
    public $name;
    /** @var  float */
    public $price;
    /** @var  string */
    public $description;
    /** @var  boolean */
    public $inStock;

    /**
     * Product constructor.
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param bool $inStock
     */
    public function __construct($id = null)
    {
        if (isset($id)) {
            $this->id = $id;
            $data = $this->getFromDb($this->getTableName(),['id'=>$id]);
            $this->setName($data['name']);
            $this->setPrice($data['price']);
            $this->setDescription($data['description']);
            $this->setInStock($data['in_stock']);
            $this->setExtraAttrs($data);
        }

    }


    public function getFromDb($table, $filters = [])
    {
        global $mysql;
        $list = [];
        $filter = '1=1';
        foreach ($filters as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }

        $result = $mysql->query("SELECT * FROM $table WHERE $filter LIMIT 1;");
        return $result->fetch_assoc();
    }

    /**
     * @return string
     */
    abstract public function getTableName();


    abstract public function setExtraAttrs($data);

    /**
     * @return boolean
     */
    abstract public function isStockable();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isInStock()
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     */
    public function setInStock($inStock)
    {
        if ($this->isStockable()) {
            $this->inStock = $inStock;
        } else {
            $this->inStock = true;
        }
    }




}